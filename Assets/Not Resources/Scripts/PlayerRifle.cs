﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ModelAnimator_RifleState
{
    Idle,
    ShotFired,
    ShotFiredAfter
}
public class PlayerRifle : PlayerWeapon
{
    private SkinnedMeshRenderer rifleRend;
    private GameObject rifleProjectile_prefab;
    [SerializeField] float timeBetweenShots;
    public float TimeBetweenShots
    {
        get
        {
            return timeBetweenShots;
        }
    }
    private float timeBetweenShots_current;
    private GameObject rifle_firePoint;

    private GameObject sE_rifle_prefab;

    public ModelAnimator_RifleState rifleStateEnum { get; set; }




    void EnableDisableShared()
    {
        if(FindDependencies() && FindResources())
        {
            rifleStateEnum = ModelAnimator_RifleState.Idle;
            timeBetweenShots_current = 0;
        }

    }



    void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (inputScr.GetInputByName("Actions", "Rifle") > 0)
            {
                if (timeBetweenShots_current > 0)
                {
                    timeBetweenShots_current -= Time.deltaTime;
                }
                else
                {
                    rifleStateEnum = ModelAnimator_RifleState.ShotFired;
                    GameObject rifleProjectile_inst= Instantiate(rifleProjectile_prefab, rifle_firePoint.transform.position, Quaternion.Euler(Vector3.zero), null);

                    RifleProjectile rifleProjectile_inst_scr = rifleProjectile_inst.GetComponent<RifleProjectile>();

                    if (rifleProjectile_inst_scr == null)
                    {
                        Destroy(rifleProjectile_inst);
                    }
                    else
                    {
                        rifleProjectile_inst_scr.moveDir = gameObject.transform.forward;
                        Instantiate(sE_rifle_prefab, rifle_firePoint.transform.position, Quaternion.Euler(Vector3.zero), null);
                    }


                    timeBetweenShots_current = timeBetweenShots;
                }
            }
            else
            {
                EnableDisableShared();

            }


        }

    }


    void FindRifleModel()
    {
        if (FindDependencies() && FindResources())
        {
            if (rifleRend == null)
            {
                GameObject rifle = GameObject.FindGameObjectWithTag("playerModel_rifle");

                if (rifle != null)
                {
                    rifleRend = rifle.GetComponent<SkinnedMeshRenderer>();
                }
            }
        }


    }
    public override void DisablePrepare()
    {
        if (FindDependencies() && FindResources())
        {
            EnableDisableShared();
            FindRifleModel();
            if (rifleRend != null)
            {
                rifleRend.enabled = false;

            }
        }

    }

    public override void EnablePrepare()
    {
        if (FindDependencies() && FindResources())
        {
            EnableDisableShared();
            FindRifleModel();
            if (rifleRend != null)
            {
                rifleRend.enabled = true;

            }
        }

    }

    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();

        if (rifle_firePoint == null)
        {
            rifle_firePoint = GameObject.FindGameObjectWithTag("playerModel_rifle_firePoint");
        }

        if (rifle_firePoint!=null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (rifleProjectile_prefab == null)
        {
            rifleProjectile_prefab = Resources.Load<GameObject>("Prefabs/Entities/rifleProjectile");
        }

        if (sE_rifle_prefab == null)
        {
            sE_rifle_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_rifle");
        }
        if (rifleProjectile_prefab != null && sE_rifle_prefab!=null)
        {
            ret = true;
        }

        return ret;
    }


}
