﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour,IEditorFunctionality
{
    private PlayerGravity gravScr;
    [SerializeField] float jumpStrength;
    public float JumpStrength
    {
        get { return jumpStrength; }
        private set { jumpStrength = value; }
    }
    private InputManager inputScr;

    private bool jumpAxisUsed;

    public void EditorUpdate()
    {
        jumpStrength = Mathf.Abs(jumpStrength);
    }
    

    private GameObject sE_jump_prefab;

    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (gravScr.Grounded||gravScr.CoyoteTime_Current>0)
            {
                if (inputScr.GetInputByName("Actions", "Jump") > 0)
                {
                    if (jumpAxisUsed == false)
                    {




                        gravScr.UpwardForceQueued = true;
                        gravScr.UFT = UpwardForceType.Jump;
                        Instantiate(sE_jump_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        jumpAxisUsed = true;
                    }
                }
                else
                {
                    jumpAxisUsed = false;
                }

            }
            else
            {
                if(gravScr.UFT== UpwardForceType.Jump)
                {
                    if (inputScr.GetInputByName("Actions", "Jump") <= 0)
                    {
                        if (gravScr.gravityCurrent > 0.06f)
                        {
                            gravScr.gravityCurrent = 0.06f;
                        }
                    }
                }


            }

        }

    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_jump_prefab == null)
        {
            sE_jump_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_jump");
        }

        if (sE_jump_prefab != null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<PlayerGravity>();
        }

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (inputScr!=null && gravScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
