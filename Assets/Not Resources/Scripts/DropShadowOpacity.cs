﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropShadowOpacity : MonoBehaviour
{
    [SerializeField] GenericRay groundRay;
    private Projector proj;
    private float origFarClip;

    [SerializeField] float minFarClip;

    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies())
        {
            origFarClip = proj.farClipPlane;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (FindDependencies())
        {
            if (groundRay.ray_surface != null)
            {
                if (Vector3.Distance(groundRay.ray_hitPoint, gameObject.transform.position) * 2 <= minFarClip)
                {
                    proj.farClipPlane = minFarClip;

                }
                else
                {
                    proj.farClipPlane = Vector3.Distance(groundRay.ray_hitPoint, gameObject.transform.position) * 2;

                }
            }
            else
            {
                proj.farClipPlane = origFarClip;
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (proj == null)
        {
            proj = gameObject.GetComponent<Projector>();
        }

        if (proj != null)
        {
            ret = true;
        }

        return ret;
    }
}
