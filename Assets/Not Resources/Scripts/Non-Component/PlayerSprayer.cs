﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprayer : PlayerWeapon
{
    private SkinnedMeshRenderer sprayerRend;
    protected GameObject sprayParticle_inst;
    private ParticleSystem sprayParticle;
    protected GameObject sprayer_firePoint;

    protected bool spraying = false;

    protected string sprayerMdlTag = "playerModel_sprayer";



    private void LateUpdate()
    {
        if (base.FindDependencies())
        {
            if (sprayParticle_inst != null && sprayer_firePoint != null)
            {

                sprayParticle_inst.transform.position = sprayer_firePoint.transform.position;
                sprayParticle_inst.transform.forward = gameObject.transform.forward;
            }
        }

    }
    protected virtual void Update()
    {
        if (base.FindDependencies())
        {
            if (sprayParticle != null)
            {
                if (inputScr.GetInputByName("Actions", "Sprayer") > 0)
                {
                    spraying = true;
                    if (sprayParticle.isPlaying == false)
                    {
                        sprayParticle.Play();

                    }
                }
                else
                {
                    spraying = false;
                    if (sprayParticle.isPlaying == true)
                    {
                        sprayParticle.Stop();

                    }
                }

            }
            else
            {
                spraying = false;
            }


        }

    }

    protected void RefreshSprayParticle()
    {
        if (base.FindDependencies())
        {
            if (sprayParticle_inst != null)
            {
                sprayParticle = sprayParticle_inst.GetComponent<ParticleSystem>();
            }
        }

    }

    void FindSprayerModel()
    {
        if (base.FindDependencies())
        {
            if (sprayerRend == null)
            {

                GameObject sprayer = GameObject.FindGameObjectWithTag(sprayerMdlTag);

                if (sprayer != null)
                {
                    sprayerRend = sprayer.GetComponent<SkinnedMeshRenderer>();
                }
            }
        }


    }

    protected virtual void EnableDisableShared()
    {
        if (base.FindDependencies())
        {

            ExtendedFunctionality.SafeDestroy(sprayParticle_inst);

            FindSprayerModel();
            sprayer_firePoint = GameObject.FindGameObjectWithTag("playerModel_sprayer_firePoint");
        }

    }

    public override void DisablePrepare()
    {
        if (base.FindDependencies())
        {
            EnableDisableShared();
            if (sprayerRend != null)
            {
                sprayerRend.enabled = false;

            }
            spraying = false;
        }

    }

    public override void EnablePrepare()
    {
        if (base.FindDependencies())
        {
            EnableDisableShared();

            if (sprayerRend != null)
            {
                sprayerRend.enabled = true;

            }
        }


    }

}
