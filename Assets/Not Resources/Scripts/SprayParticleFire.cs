﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayParticleFire : SprayParticle
{
    [SerializeField] int damage;
    public override void OnParticleCollision(GameObject other)
    {

        Enemy enCheck = other.GetComponent<Enemy>();

        if (enCheck != null)
        {
            enCheck.ShiftHealth_FlameParticle(Mathf.Abs(damage)*-1);

        }

        Chest chestCheck = other.GetComponent<Chest>();

        if (chestCheck != null)
        {
            if(chestCheck.ChestTypeEnum==ChestType.SprayerFire|| chestCheck.ChestTypeEnum == ChestType.Any)
            {
                Destroy(chestCheck.gameObject);
            }
        }
    }
}
