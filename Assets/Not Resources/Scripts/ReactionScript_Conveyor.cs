﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionScript_Conveyor : ReactionScript
{
    [SerializeField] Conveyor[] convScr;
    protected override void ReactActive()
    {
        if (convScr != null && convScr.Length > 0)
        {
            foreach (Conveyor conv in convScr)
            {
                conv.Running = true;
            }
        }
    }

    protected override void ReactInactive()
    {
        if(convScr != null && convScr.Length > 0)
        {
            foreach(Conveyor conv in convScr)
            {
                conv.Running = false;

            }
        }
    }
}
