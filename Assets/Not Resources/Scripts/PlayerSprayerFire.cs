﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprayerFire : PlayerSprayer
{

    private GameObject pS_sprayerFire_prefab;
    private GameObject sE_sprayerFire_init_prefab;
    private GameObject sE_sprayerFire_loop_prefab;

    private GameObject fireLoop_inst;
    private GameObject fireInit_inst;

    private bool initSoundComplete=false;

    protected override void EnableDisableShared()
    {
        if(base.FindDependencies() && FindResources())
        {
            sprayerMdlTag = "playerModel_sprayer_fire";
            base.EnableDisableShared();
        }

    }

    public override void EnablePrepare()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.EnablePrepare();
            if (sprayer_firePoint != null)
            {
                sprayParticle_inst = Instantiate(pS_sprayerFire_prefab, sprayer_firePoint.transform.position, Quaternion.Euler(Vector3.zero), null);



                RefreshSprayParticle();
            }
        }

    }

    void EndSound()
    {
        initSoundComplete = false;
        ExtendedFunctionality.SafeDestroy(fireInit_inst);
        ExtendedFunctionality.SafeDestroy(fireLoop_inst);
    }

    public override void DisablePrepare()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.DisablePrepare();
            EndSound();
        }

    }

    protected override void Update()
    {
        if(base.FindDependencies() && FindResources())
        {
            base.Update();
            if (spraying == true)
            {
                if (initSoundComplete == false)
                {
                    if (fireInit_inst == null)
                    {
                        fireInit_inst = Instantiate(sE_sprayerFire_init_prefab, sprayParticle_inst.transform.position, Quaternion.Euler(Vector3.zero), null);

                    }
                    initSoundComplete = true;
                }


                if (fireLoop_inst == null)
                {
                    fireLoop_inst = Instantiate(sE_sprayerFire_loop_prefab, sprayParticle_inst.transform.position, Quaternion.Euler(Vector3.zero), sprayParticle_inst.transform);
                }

            }
            else
            {
                EndSound();

            }
        }
    }



    bool FindResources()
    {
        bool ret = false;

        if (pS_sprayerFire_prefab == null)
        {
            pS_sprayerFire_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_sprayerFire");
        }

        if (sE_sprayerFire_init_prefab == null)
        {
            sE_sprayerFire_init_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_sprayerFire_init");
        }

        if (sE_sprayerFire_loop_prefab == null)
        {
            sE_sprayerFire_loop_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_sprayerFire_loop");
        }

        if (pS_sprayerFire_prefab != null && sE_sprayerFire_loop_prefab!=null && sE_sprayerFire_init_prefab!=null)
        {
            ret = true;
        }

        return ret;
    }

}
