﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModelAnimator_Twister : EnemyModelAnimator
{


    [SerializeField] float attackExpandMultiple;

    private GameObject protonModel_prefab;
    private GameObject protonModel_inst;

    private PlayerStats statScr;

    private GameObject sE_twisterSpin_prefab;
    private GameObject sE_twisterSpin_inst;
    protected override void Awake()
    {

        if (FindResources() && FindDependencies())
        {
            SetMdlPath_OneTimeOnly("Prefabs/Enemies/twisterModel");

            base.Awake();
            

        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        ExtendedFunctionality.SafeDestroy(protonModel_inst);
        ExtendedFunctionality.SafeDestroy(sE_twisterSpin_inst);

    }

    protected override void Update()
    {
        if (FindResources() && FindDependencies())
        {
            base.Update();



            if (mdl_inst_anim != null)
            {



                if(enScr.ChaseMode == true)
                {
                    if (enScr.AlertTime_Current <=0)
                    {
                        if (!mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("attack") && !mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("attackLoop"))
                        {
                            mdl_inst_anim.Play("attack");

                        }
                        else
                        {

                            if (mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("attack") && mdl_inst_anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
                            {
                                mdl_inst_anim.Play("attackLoop");

                            }
                        }

                    }
                }


                if (mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("attackLoop") || mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("attack"))
                {

                    enScr.HurtRegionToggle(true);

                    mdlEffectScale = Vector3.Lerp(mdlEffectScale, new Vector3(attackExpandMultiple, 1, attackExpandMultiple), 3 * Time.deltaTime);


                    if (protonModel_inst == null)
                    {
                        protonModel_inst = Instantiate(protonModel_prefab, gameObject.transform.position+new Vector3(0,0.1f,0), Quaternion.Euler(Vector3.zero), null);

                        MaintainOffset offScr = protonModel_inst.GetComponent<MaintainOffset>();

                        if (offScr == null)
                        {
                            offScr = protonModel_inst.AddComponent<MaintainOffset>();
                        }

                        offScr.OffsetObj = gameObject;
                        offScr.Init();

                    }

                    if (protonModel_inst != null)
                    {

                        protonModel_inst.transform.forward = (statScr.transform.position - gameObject.transform.position).normalized;

                    }

                    if (enScr.FrozenTimer_Current <= 0)
                    {
                        if (sE_twisterSpin_inst == null)
                        {
                            sE_twisterSpin_inst = Instantiate(sE_twisterSpin_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        }
                        else
                        {
                            sE_twisterSpin_inst.transform.position = gameObject.transform.position;
                        }
                    }
                    else
                    {
                        ExtendedFunctionality.SafeDestroy(sE_twisterSpin_inst);

                    }
                }
                else
                {
                    enScr.HurtRegionToggle(false);

                    mdlEffectScale = Vector3.Lerp(mdlEffectScale, Vector3.one, 7 * Time.deltaTime);
                    ExtendedFunctionality.SafeDestroy(protonModel_inst);
                    ExtendedFunctionality.SafeDestroy(sE_twisterSpin_inst);
                }



            }
        }
    }


    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();


        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }


        if (statScr!=null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();
        if (protonModel_prefab == null)
        {
            protonModel_prefab = Resources.Load<GameObject>("Prefabs/Enemies/protonModel");
        }

        if (sE_twisterSpin_prefab == null)
        {
            sE_twisterSpin_prefab= Resources.Load<GameObject>("Prefabs/Sound Effects/sE_twisterSpin");
        }
        if(sE_twisterSpin_prefab!=null && protonModel_prefab != null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }

}
