﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum ColourCycleMode
{
    Loop,
    Random,
    RandomNoSuccession
}
public class ExplosionEffect : MonoBehaviour
{
    [SerializeField] List<Color> explosionColours = new List<Color>();
    [SerializeField] ColourCycleMode colCycle;
    [SerializeField] float cycleTime;
    [SerializeField] float expandRate;
    private float cycleTime_current;

    private Renderer rend;
    [SerializeField] int colCycleIndex;
    private int colCycleIndex_prev;
    private void Awake()
    {
        colCycleIndex_prev = colCycleIndex;


        rend = gameObject.GetComponent<Renderer>();

    }
    [SerializeField] float explosionAlpha;
    private void Update()
    {
        if (cycleTime_current > 0)
        {
            cycleTime_current -= Time.deltaTime;
        }
        else
        {
            if (rend != null)
            {
                switch (colCycle)
                {
                    case ColourCycleMode.Loop:
                        if (colCycleIndex + 1 >= explosionColours.Count)
                        {
                            colCycleIndex = 0;
                        }
                        else
                        {
                            colCycleIndex += 1;
                        }
                        break;

                    case ColourCycleMode.Random:
                        colCycleIndex = Random.Range(0, explosionColours.Count);
                        break;
                    case ColourCycleMode.RandomNoSuccession:
                        while (colCycleIndex == colCycleIndex_prev)
                        {
                            colCycleIndex = Random.Range(0, explosionColours.Count);

                        }
                        break;

                }
                foreach (Material mat in rend.materials)
                {
                    if (mat.HasProperty("_Color"))
                    {
                        mat.SetColor("_Color", new Color( explosionColours[colCycleIndex].r, explosionColours[colCycleIndex].g, explosionColours[colCycleIndex].b, explosionAlpha));
                    }

                    if (mat.HasProperty("_Emission"))
                    {
                        mat.SetColor("_Emission", new Color(explosionColours[colCycleIndex].r, explosionColours[colCycleIndex].g, explosionColours[colCycleIndex].b, explosionAlpha));


                    }
                }
                colCycleIndex_prev = colCycleIndex;
            }

            cycleTime_current = cycleTime;
        }

        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + (expandRate * Time.deltaTime), gameObject.transform.localScale.y + (expandRate * Time.deltaTime), gameObject.transform.localScale.z + (expandRate * Time.deltaTime));
    }
}
