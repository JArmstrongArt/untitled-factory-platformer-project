﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum SineType
{
    Sine,
    Cosine
}


[DefaultExecutionOrder(-100)]//this is so that, when used as a moving platform. it is calculated before other scripts that use movement
public class SineMove : MonoBehaviour
{
    [SerializeField] UpdateType sineUpdateEnum;
    [SerializeField] SineType sineEnum_X;
    [SerializeField] SineType sineEnum_Y;
    [SerializeField] SineType sineEnum_Z;
    [SerializeField] Vector3 sineStrength;
    public Vector3 SineStrength
    {
        get
        {
            return sineStrength;
        }
    }
    [SerializeField] Vector3 sineRate;
    [SerializeField] float timeOffset;
    [SerializeField] float timeOffset_absolute;
    private Vector3 origPos= Constants.NULLVEC3;

    public Vector3 OrigPos
    {
        set
        {
            origPos = value;
        }
    }

    private Vector3 prevPos;
    public Vector3 DiffPos { get; private set; }


    // Start is called before the first frame update
    void Awake()
    {
        Init();
    }

    private void OnEnable()//for when the sinemove is on a coin that got spawned by a chest and so it sinemoves later on
    {
        Init();

    }

    private void Init()
    {
        if (origPos == Constants.NULLVEC3)
        {
            origPos = gameObject.transform.position;

        }

    }


    void mainFunc(float timeUnit)
    {
        prevPos = gameObject.transform.position;

        float xVal = Mathf.Sin((timeUnit + timeOffset_absolute+(timeOffset * origPos.y)) * sineRate.x);
        float yVal = Mathf.Sin((timeUnit + timeOffset_absolute + (timeOffset * origPos.x)) * sineRate.y);
        float zVal = Mathf.Sin((timeOffset_absolute + timeUnit) * sineRate.z);

        if (sineEnum_X != SineType.Sine)
        {
            xVal = Mathf.Cos((timeUnit + (timeOffset * origPos.y)) * sineRate.x);

        }

        if (sineEnum_Y != SineType.Sine)
        {
            yVal = Mathf.Cos((timeUnit + (timeOffset * origPos.x)) * sineRate.y);

        }

        if (sineEnum_Z != SineType.Sine)
        {
            zVal = Mathf.Cos(timeUnit * sineRate.z);

        }

        gameObject.transform.position = origPos + new Vector3(xVal * sineStrength.x, yVal * sineStrength.y, zVal * sineStrength.z);

        DiffPos = gameObject.transform.position - prevPos;
    }

    // Update is called once per frame
    void Update()
    {
        if (sineUpdateEnum == UpdateType.Regular)
        {
            mainFunc(Time.time);
        }
    }

    void LateUpdate()
    {
        if (sineUpdateEnum == UpdateType.Late)
        {
            mainFunc(Time.time);
        }
    }

    void FixedUpdate()
    {
        if (sineUpdateEnum == UpdateType.Fixed)
        {
            mainFunc(Time.fixedTime);
        }
    }
}
