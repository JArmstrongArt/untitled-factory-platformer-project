﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShieldState
{
    Lowered,
    Raised,
    Melted
}
public class ChikChik : Enemy
{
    [SerializeField] int frameAdvanceThink;//think this many frames in advance when predicting a shot
    private PlayerStats statScr;
    private Vector3 prevPlayerPos;
    private GameObject chikChikDynamite_prefab;
    public ShieldState ShieldStateEnum { get; private set; }
    private PlayerDash dashScr;
    private PlayerHurt hurtScr;
    public EnemyModelAnimator_ChikChik ChikAnimScr { get; private set; }


    [SerializeField] Vector2 dynamiteFireTimeRange;
    private float dynamiteFireTime_current;
    [SerializeField] Vector2 dynamitePowerRange;
    [SerializeField] Vector2 dynamiteAngleRange;

    [SerializeField] float shieldHealth;
    private Transform dynSpawnPos;
    private bool lateAwake = false;
    private bool alertSndPlayed;
    public float ShieldHealth
    {
        get
        {
            return shieldHealth;
        }
    }

    private GameObject sE_chikChikAlert_prefab;
    private void LateUpdate()
    {
        if (FindResources() && FindDependencies())
        {
            if (lateAwake == false)
            {
                GameObject[] dynSpawnPosObj_all = GameObject.FindGameObjectsWithTag("chikChik_dynaSpawnPos");
                if (dynSpawnPosObj_all != null)
                {
                    for (int i = 0; i < dynSpawnPosObj_all.Length; i++)
                    {
                        if (dynSpawnPosObj_all[i] != null)
                        {
                            if (dynSpawnPosObj_all[i].transform.IsChildOf(gameObject.transform))
                            {
                                dynSpawnPos = dynSpawnPosObj_all[i].transform;
                                break;
                            }
                        }
                    }

                }
                if (dynSpawnPos != null)
                {
                    lateAwake = true;

                }
            }
            else
            {
                Vector3 playerMoveVec = statScr.transform.position - prevPlayerPos;


                Vector3 aimPos = statScr.transform.position + (playerMoveVec * frameAdvanceThink);
                if (ChaseMode == true && AlertTime_Current > 0)
                {
                    if (alertSndPlayed == false)
                    {
                        Instantiate(sE_chikChikAlert_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        alertSndPlayed = true;
                    }
                }
                else
                {
                    alertSndPlayed = false;

                }

                Vector3 meToPlayer = playerScr.transform.position - gameObject.transform.position;
                if (ChaseMode == true && AlertTime_Current <= 0)
                {
                    if (ShieldStateEnum != ShieldState.Melted)
                    {
                        ShieldStateEnum = ShieldState.Raised;

                    }

                    if (FrozenTimer_Current <= 0)
                    {
                        if (dynamiteFireTime_current > 0)
                        {

                            dynamiteFireTime_current -= Time.deltaTime;
                        }
                        else
                        {
                            GameObject dynInst = Instantiate(chikChikDynamite_prefab, dynSpawnPos.position, Quaternion.Euler(Vector3.zero), null);

                            ChikChikDynamite dynScr = dynInst.GetComponent<ChikChikDynamite>();
                            if (dynScr != null)
                            {
                                dynScr.InitArc(Random.Range(dynamitePowerRange.x, dynamitePowerRange.y), gameObject.transform.position, (new Vector3(aimPos.x,gameObject.transform.position.y, aimPos.z)-gameObject.transform.position).normalized, Random.Range(dynamiteAngleRange.x, dynamiteAngleRange.y));
                            }
                            else
                            {
                                Destroy(gameObject);
                            }
                            ChikAnimScr.FireAnimDue = true;
                            dynamiteFireTime_current = Random.Range(dynamiteFireTimeRange.x, dynamiteFireTimeRange.y);
                        }
                    }

                }
                else
                {
                    ShieldStateEnum = ShieldState.Lowered;
                }

                if (ShieldStateEnum == ShieldState.Raised)
                {

                    if (meToPlayer.magnitude <= 2.5f)
                    {
                        if (dashScr.DashDuration_Current > 0)
                        {
                            if (Vector3.Dot(new Vector3(playerScr.transform.forward.x, 0, playerScr.transform.forward.z).normalized, new Vector3(gameObject.transform.forward.x, 0, gameObject.transform.forward.z).normalized) <= -0.5f)
                            {

                                ChikAnimScr.BlockAnimDue = true;
                                hurtScr.GetHurt(0, true);

                            }
                        }
                    }


                }

                prevPlayerPos = statScr.transform.position;
            }



        }
    }
    public override int ShiftHealth(int shiftAmount, EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {
        int retVal = -1;
        if (FindDependencies() && FindResources())
        {
            if (ShieldStateEnum == ShieldState.Raised)
            {
                if (cOD == EnemyCauseOfDamage.Lob)
                {
                    retVal = base.ShiftHealth(shiftAmount);

                }
                else if (cOD == EnemyCauseOfDamage.SprayerFire)
                {
                    if (shieldHealth > 0)
                    {
                        shieldHealth -= Mathf.Abs(shiftAmount);
                        ChikAnimScr.BurnAnimDue = true;

                    }
                    else
                    {
                        ShieldStateEnum = ShieldState.Melted;
                    }


                }
                else
                {


                    ChikAnimScr.BlockAnimDue = true;
                }



            }
            
            if(ShieldStateEnum != ShieldState.Raised)
            {
                retVal = base.ShiftHealth(shiftAmount);
            }






        }

        return retVal;
    }

    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet= base.FindDependencies();

        if (dashScr == null)
        {
            dashScr = GameObject.FindObjectOfType<PlayerDash>();
        }

        if (hurtScr == null)
        {
            hurtScr = GameObject.FindObjectOfType<PlayerHurt>();
        }

        if (ChikAnimScr == null)
        {
            ChikAnimScr = gameObject.GetComponent<EnemyModelAnimator_ChikChik>();
        }

        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }

        if (statScr!=null && ChikAnimScr != null && hurtScr != null && dashScr != null && parRet)
        {
            ret = true;
        }
        return ret;
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (chikChikDynamite_prefab == null)
        {
            chikChikDynamite_prefab = Resources.Load<GameObject>("Prefabs/Enemies/ChikChikDynamite");
        }
        if (sE_chikChikAlert_prefab == null)
        {
            sE_chikChikAlert_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_chikChikAlert");
        }

        if (sE_chikChikAlert_prefab!=null && chikChikDynamite_prefab != null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
