﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [HideInInspector]
    public List<GameObject> myChildren = new List<GameObject>();
    void Awake()
    {
        while (gameObject.transform.childCount > 0)
        {
            myChildren.Add(gameObject.transform.GetChild(0).gameObject);
            gameObject.transform.GetChild(0).parent = null;
        }

    }

}
