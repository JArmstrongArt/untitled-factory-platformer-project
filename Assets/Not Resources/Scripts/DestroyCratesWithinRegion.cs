﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCratesWithinRegion : MonoBehaviour,IEditorFunctionality
{
    [SerializeField] float destroyRadius;

    public void EditorUpdate()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(gameObject.transform.position, destroyRadius);
    }

    private void Awake()
    {
        Chest[] allChests = GameObject.FindObjectsOfType<Chest>();

        foreach(Chest che in allChests)
        {
            if (che != null)
            {
                if (Vector3.Distance(che.transform.position, gameObject.transform.position) <= destroyRadius)
                {
                    if(che.ChestTypeEnum==ChestType.Lob|| che.ChestTypeEnum == ChestType.Any)
                    {
                        Destroy(che.gameObject);

                    }
                }
            }
        }

    }

}
