﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtRegion : MonoBehaviour
{
    private bool triggerEnabled=true;

    private int damage = 1;
    public int Damage
    {
        set
        {
            damage = value;
        }

        private get
        {
            return damage;
        }
    }
    public bool TriggerEnabled
    {
        private get
        {
            return triggerEnabled;
        }

        set
        {
            triggerEnabled = value;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (TriggerEnabled)
        {
            PlayerHurt hurtScr = other.GetComponent<PlayerHurt>();

            if (hurtScr != null)
            {
                hurtScr.GetHurt(Damage);
            }
        }

    }
}
