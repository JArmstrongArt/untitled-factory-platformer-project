﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobUnit : ArcExplosive
{





    private GameObject sE_lobTick_prefab;

    [SerializeField] float tickTime;
    private float tickTime_current;




    private GameObject lobUnit_Model_prefab;

    private GameObject lobUnit_Model_inst;





    private PlayerStats statScr;
    protected override void OnDestroy()
    {
        base.OnDestroy();
        ExtendedFunctionality.SafeDestroy(lobUnit_Model_inst);

    }





    private void Update()
    {
        if (FindResources() && FindDependencies())
        {
            if (lobUnit_Model_inst == null)
            {
                lobUnit_Model_inst = Instantiate(lobUnit_Model_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

                MaintainOffset offScr = lobUnit_Model_inst.GetComponent<MaintainOffset>();
                if (offScr == null)
                {
                    offScr = lobUnit_Model_inst.AddComponent<MaintainOffset>();
                }

                offScr.OffsetUpdate = UpdateType.Late;


                offScr.OffsetObj = gameObject;
                offScr.Init();
            }

            if (lobUnit_Model_inst != null)
            {
                Rotate rotScr = lobUnit_Model_inst.GetComponent<Rotate>();
                if (rotScr != null)
                {
                    if (totalArcTime_remaining > 0)
                    {

                        rotScr.enabled = true;
                    }
                    else
                    {
                        lobUnit_Model_inst.transform.right = statScr.gameObject.transform.forward;//i really dont know why lobUnit_model is aligned to the right but it doesnt matter

                        rotScr.enabled = false;

                    }

                }

            }

            

        }
    }


    protected override void FixedUpdate()
    {
        if (FindResources() && FindDependencies())
        {
            base.FixedUpdate();
            if (totalArcTime_remaining > 0)
            {



                


                if (tickTime_current > 0)
                {
                    tickTime_current -= Time.deltaTime;
                }
                else
                {
                    Instantiate(sE_lobTick_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    if (totalArcTime_remaining <= 0.8f)
                    {
                        tickTime_current = tickTime * 0.05f;
                    }
                    else
                    {
                        tickTime_current = tickTime;
                    }
                }

            }
            else
            {


            }

        }

    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (sE_lobTick_prefab == null)
        {
            sE_lobTick_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_lobTick");
        }


        if (lobUnit_Model_prefab == null)
        {
            lobUnit_Model_prefab = Resources.Load<GameObject>("Prefabs/Entities/lobUnit_Model");
        }


        if (lobUnit_Model_prefab != null && sE_lobTick_prefab != null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }

        if (statScr != null)
        {
            ret = true;
        }

        return ret;
    }




}
