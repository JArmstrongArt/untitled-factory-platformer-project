﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPLAT : Enemy
{
    private GameObject sE_SPLATAlert_prefab;
    private bool alertSndPlayed=false;

    private void Update()
    {
        if (FindResources())
        {
            if (ChaseMode && AlertTime_Current > 0)
            {
                if (alertSndPlayed == false)
                {
                    Instantiate(sE_SPLATAlert_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    alertSndPlayed = true;
                }
            }
            else
            {
                alertSndPlayed = false;
            }
        }


    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();


        if (sE_SPLATAlert_prefab == null)
        {
            sE_SPLATAlert_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_SPLATAlert");
        }
        if (sE_SPLATAlert_prefab!=null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
