﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcCoin : ArcObject
{

    private Collectable colScr;

    protected override void Awake()
    {
        if (FindDependencies())
        {
            base.Awake();

            colScr.SecretlyDisabled = true;
        }

    }
    protected override void EndArcReact()
    {
        if (FindDependencies())
        {

            colScr.SecretlyDisabled = false;


        }


    }

    bool FindDependencies()
    {
        bool ret = false;



        if (colScr == null)
        {
            colScr = gameObject.GetComponent<Collectable>();
        }

        if (colScr!=null )
        {
            ret = true;
        }

        return ret;
    }
}
