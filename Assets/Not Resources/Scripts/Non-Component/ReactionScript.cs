﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ReactionScript : MonoBehaviour,IEditorFunctionality
{
    private GameObject sE_reactionComplete_prefab;
    [SerializeField] MonoBehaviour[] involvedReactionaries;

    protected abstract void ReactActive();
    protected abstract void ReactInactive();

    private bool reacted = false;
    void Update()
    {
        if (FindResources())
        {
            bool readyToReact = true;
            if (involvedReactionaries != null && involvedReactionaries.Length > 0)
            {
                for (int i = 0; i < involvedReactionaries.Length; i++)
                {
                    if (involvedReactionaries[i] != null)
                    {
                        IReactionaryElement react = involvedReactionaries[i] as IReactionaryElement;
                        if (react != null)
                        {
                            if (react.ConditionFulfilled == false)
                            {
                                readyToReact = false;
                                break;
                            }
                        }
                    }
                }
            }

            if (readyToReact)
            {
                if (reacted == false)
                {

                    ReactActive();
                    Instantiate(sE_reactionComplete_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    reacted = true;
                }
            }
            else
            {
                reacted = false;
                ReactInactive();
            }
        }

    }

    public void EditorUpdate()
    {
        if(involvedReactionaries!=null && involvedReactionaries.Length > 0)
        {
            for (int i = 0; i < involvedReactionaries.Length; i++)
            {
                if (involvedReactionaries[i] != null)
                {
                    IReactionaryElement react = involvedReactionaries[i] as IReactionaryElement;
                    if (react == null)
                    {
                        involvedReactionaries[i] = null;
                        UnityEditor.EditorUtility.DisplayDialog("!ALERT!", "I can't make custom inspectors yet, so for now please only place IReactionaryElement interface scripts in this serialized MonoBehaviour array. If your object HAS an IReactionaryElement interface script on it and you still got this message, then move that script upwards in the inspector above all other MonoDevelop scripts.", "Got it.");
                    }
                }

            }
        }

    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_reactionComplete_prefab == null)
        {
            sE_reactionComplete_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_reactionComplete");
        }

        if (sE_reactionComplete_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
