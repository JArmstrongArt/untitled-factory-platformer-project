﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Collectable
{
    [SerializeField] Color[] coinCols;
    [SerializeField] Renderer rend;

    private GameObject sE_coin_prefab;
    private GameObject sE_dashCoin_prefab;
    private Vector3 flingDir;
    private float flingPower;

    private bool collected = false;


    private float gravPower=0.6f;
    private float gravCurrent=0.0f;
    private SineMove sinMovScr;

    private Rotate rotScr;
    [SerializeField] float postCollectionLifetime;
    // Start is called before the first frame update
    protected override void Awake()
    {
        if (FindResources() && FindDependencies())
        {
            base.Awake();


            flingDir = new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(0.9f, 1f), Random.Range(-0.3f, 0.3f)).normalized;
            flingPower = Random.Range(46, 40);

            ResetColour();
        }

    }

    private void Update()
    {
        if (FindResources() && FindDependencies())
        {
            if (!secretlyDisabled)
            {
                if (collected)
                {
                    sinMovScr.enabled = false;


                    gameObject.transform.position += ((flingDir * flingPower) * Time.deltaTime) + ((Vector3.down * gravCurrent) * Time.deltaTime);
                    gravCurrent += gravPower;
                }
                else
                {
                    sinMovScr.enabled = true;

                }
            }
            else
            {
                sinMovScr.enabled = false;
                sinMovScr.OrigPos = Constants.NULLVEC3;
            }


        }
    }

    void ResetColour()
    {
        if (FindResources() && FindDependencies())
        {

            if (rend != null)
            {
                Color coinCol = coinCols[Mathf.RoundToInt(Random.Range(0, coinCols.Length - 1))];
                coinCol = new Color(coinCol.r, coinCol.g, coinCol.b, 1);
                foreach (Material mat in rend.materials)
                {
                    float coinCol_h, coinCol_s, coinCol_v;
                    Color.RGBToHSV(coinCol, out coinCol_h, out coinCol_s, out coinCol_v);
                    if (mat.HasProperty("_Color"))
                    {
                        mat.SetColor("_Color", coinCol);
                    }

                    if (mat.HasProperty("_SpecColor"))
                    {


                        mat.SetColor("_SpecColor", Color.HSVToRGB(coinCol_h, 10, 100));

                    }

                    if (mat.HasProperty("_EmissionColor"))
                    {
                        mat.SetColor("_EmissionColor", Color.HSVToRGB(coinCol_h, coinCol_s, 0.12f));
                    }

                    if (mat.HasProperty("_Emission"))
                    {
                        mat.SetColor("_Emission", Color.HSVToRGB(coinCol_h, coinCol_s, 0.12f));
                    }
                }
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;



        if (sinMovScr == null)
        {
            sinMovScr = gameObject.GetComponent<SineMove>();
        }

        if (rotScr == null)
        {
            rotScr = gameObject.GetComponent < Rotate>();
        }
        if ( sinMovScr!=null && rotScr!=null)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();

        if (sE_coin_prefab == null)
        {
            sE_coin_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_coin");
        }

        if (sE_dashCoin_prefab == null)
        {
            sE_dashCoin_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_dashCoin");
        }

        if(sE_coin_prefab!=null && sE_dashCoin_prefab!=null&& parRet == true)
        {
            ret = true;
        }

        return ret;
    }

    void RegularCoinSndReact(PlayerDash dashScr = null)
    {
        GameObject sE_coin_inst = Instantiate(sE_coin_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

        if (dashScr != null)
        {
            GeneralSoundEffect sE_coin = sE_coin_inst.GetComponent<GeneralSoundEffect>();

            if (sE_coin != null)
            {
                if (dashScr.DashDuration_Current > 0)
                {
                    sE_coin.UpdatePitchActual(new Vector2(dashScr.AscendingCoinPitchPercent, dashScr.AscendingCoinPitchPercent));

                    dashScr.AscendingCoinPitchPercent = Mathf.Clamp(dashScr.AscendingCoinPitchPercent + 10, dashScr.AscendingCoinPitchPercent_Orig, dashScr.AscendingCoinPitchPercent_Orig * 2.5f);
                }
            }
        }
    }

    protected override void CollectionReact(PlayerDash dashScr=null)
    {
        if (FindResources() && FindDependencies())
        {
            if (collected == false)
            {
                if (dashScr.NearestCollectable != null)
                {
                    RegularCoinSndReact(dashScr);
                }
                else
                {
                    if(dashScr.AscendingCoinPitchPercent>= dashScr.AscendingCoinPitchPercent_Orig * 2.5f)
                    {
                        Instantiate(sE_dashCoin_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    }
                    else
                    {
                        RegularCoinSndReact(dashScr);

                    }
                }




                if (!statScr.collectedCoinIDs.Contains(collectableID))
                {
                    statScr.collectedCoinIDs.Add(collectableID);
                }

                rotScr.SpinVals = new Vector3(rotScr.SpinVals.x * 6, rotScr.SpinVals.y * 6, rotScr.SpinVals.z * 6);

                LifeTimer lT = gameObject.AddComponent<LifeTimer>();
                lT.LifeTime = postCollectionLifetime;
                collected = true;
            }

        }
    }


}

