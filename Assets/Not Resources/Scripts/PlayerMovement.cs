﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private CharacterController playerController;


    private InputManager inputScr;
    [SerializeField] float movementSpeed;
    public float MovementSpeed { get { return movementSpeed; }  }
    public float MovementSpeed_Orig { get; private set; }

    private PlayerCameraAndMovementTrajectoryCalculation trajScr;

    public Vector3 MoveCalc { get; private set; }

    private PlayerCamera camScr;

    void Awake()
    {
        if (FindDependencies())
        {
            MovementSpeed_Orig = movementSpeed;
            MoveCalc = Vector3.zero;
        }
    }






    private void FixedUpdate()
    {
        if(FindDependencies())
        {
            if (trajScr.PlayerCam_Scr != null)
            {

                float forwardMovement_input = 0;
                switch (Constants.CONTROLLER)
                {
                    case ControlType.Controller:
                        forwardMovement_input = inputScr.GetInputByName("Movement", "ForBack_Controller",true);

                        break;
                    case ControlType.Keyboard:
                        forwardMovement_input = inputScr.GetInputByName("Movement", "ForBack");
                        break;

                }




                float sideMovement_input = 0;
                switch (Constants.CONTROLLER)
                {
                    case ControlType.Controller:
                        sideMovement_input = inputScr.GetInputByName("Movement", "LeftRight_Controller",true);
                        break;
                    case ControlType.Keyboard:
                        sideMovement_input = inputScr.GetInputByName("Movement", "LeftRight");
                        break;

                }

                /*
                float sideMovement_input_noSnap = 0;
                switch (Constants.CONTROLLER)
                {
                    case ControlType.Controller:
                        sideMovement_input_noSnap = inputScr.GetInputByName("Movement", "LeftRight_Controller_NoSnap", true);
                        break;
                    case ControlType.Keyboard:
                        sideMovement_input_noSnap = inputScr.GetInputByName("Movement", "LeftRight_NoSnap");
                        break;

                }
                */


                Vector3 forwardMovement = trajScr.PlayerCam_Scr.FlatForward * forwardMovement_input* movementSpeed*Time.fixedDeltaTime;

                Vector3 sideMovement = trajScr.PlayerCam_Scr.FlatRight * sideMovement_input * movementSpeed*Time.fixedDeltaTime;


                if (playerController == null)
                {
                    playerController = gameObject.GetComponent<CharacterController>();
                }

                if (playerController != null)
                {
                    MoveCalc =  forwardMovement + sideMovement;

                    playerController.Move(MoveCalc);

                    if (camScr.LockOnTarget == null)
                    {
                        if (MoveCalc != Vector3.zero)
                        {
                            gameObject.transform.forward = new Vector3(MoveCalc.x, 0, MoveCalc.z).normalized;

                        }
                    }
                    else
                    {
                        gameObject.transform.forward = (new Vector3(camScr.LockOnTarget.transform.position.x, gameObject.transform.position.y, camScr.LockOnTarget.transform.position.z) - gameObject.transform.position).normalized;
                    }


                }

            }
        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();

        }

        if (trajScr == null)
        {
            trajScr = gameObject.GetComponent<PlayerCameraAndMovementTrajectoryCalculation>();
        }

        if (camScr == null)
        {
            camScr = GameObject.FindObjectOfType<PlayerCamera>();
        }


        if (trajScr!=null && inputScr != null&& camScr!=null)
        {
            ret = true;
        }

        return ret;
    }



}
