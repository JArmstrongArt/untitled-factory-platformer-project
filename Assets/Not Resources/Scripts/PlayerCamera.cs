﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum CameraOperationMode
{
    Explore,
    LockOn
}
public class PlayerCamera : MonoBehaviour
{

    private PlayerCameraAndMovementTrajectoryCalculation trajScr;
    public Vector3 LookTarget { get; set; }

    private float camDistance;
    public float CamDistance
    {

        set { camDistance = value; }
    }
    public float CamDistance_Orig { get; private set; }
    private CameraOperationMode camOpMode = CameraOperationMode.Explore;
    public Vector3 FlatForward { get; private set; }
    public Vector3 FlatRight { get; private set; }

    private Vector3 finalPos;
    //[SerializeField] float camLerpRate;




    [SerializeField] float camTurnExtent;
    [SerializeField] float camLeanExtent;

    public float targetYPos { private get; set; }
    public float CamTurnExtent
    {
        get { return camTurnExtent; }

    }

    public float CamLeanExtent
    {
        get { return camLeanExtent; }

    }

    private GameObject rawLookAt_inst;

    private PlayerMovement moveScr;

    private bool lateAwake = false;

    public bool MoveScrSetLookTarget { private get; set; }


    private InputManager inputScr;
    [SerializeField] float camTurnRate;
    private float curTurnAngle;

    private Vector3 pivot;

    private Vector3 postTurnPos;

    private GameObject lockOnTarget_passive;
    public GameObject LockOnTarget { get; private set; }
    private bool lockOn_axisUsed;
    [SerializeField] float lockOnRange;

    private bool lockOnMode=false;

    private GameObject sE_lockOnActivate_prefab;
    private GameObject sE_lockOnDeactivate_prefab;


    // Start is called before the first frame update


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(pivot, 0.3f);
    }
    private void OnDisable()
    {
        if (FindDependencies() && FindResources())
        {
            lateAwake = false;
        }
    }

    void SetLOVRDir()
    {
        if (lockOnTarget_passive != null)
        {
            trajScr.LockOnVisibilityRay.rayLength = Vector3.Distance(lockOnTarget_passive.transform.position, moveScr.transform.position);
            trajScr.LockOnVisibilityRay.customDir_vec = (lockOnTarget_passive.transform.position - moveScr.transform.position).normalized;
        }
    }



    private void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (lockOnTarget_passive != null)
            {
                SetLOVRDir();
            }
            else
            {
                trajScr.LockOnVisibilityRay.rayLength = 0;
                trajScr.LockOnVisibilityRay.customDir_vec = Vector3.zero;
            }


            Enemy[] allEnemies = GameObject.FindObjectsOfType<Enemy>();

            List<float> enemyDist = new List<float>();


            foreach (Enemy en in allEnemies)
            {
                if (en != null)
                {
                    enemyDist.Add(Vector3.Distance(moveScr.transform.position, en.transform.position));
                }
            }

            float shortestDist = float.MaxValue;



            foreach (Enemy en in allEnemies)
            {
                if (en != null)
                {
                    if (Vector3.Distance(moveScr.transform.position, en.transform.position) <= shortestDist)
                    {

                        shortestDist = Vector3.Distance(moveScr.transform.position, en.transform.position);
                        lockOnTarget_passive = en.gameObject;

                    }
                }

            }


            
            if (lockOnTarget_passive != null)
            {
                if (Vector3.Distance(lockOnTarget_passive.transform.position, moveScr.transform.position) > lockOnRange)
                {
                    lockOnTarget_passive = null;

                }
            }
            
            if (lockOnTarget_passive == null)
            {
                lockOnMode = false;
            }






            if (inputScr.GetInputByName("Camera", "LockOn") > 0)
            {
                if (lockOn_axisUsed == false)
                {
                    if (lockOnMode == true)
                    {
                        lockOnMode = false;
                    }
                    else
                    {
                        if (LockOnTarget == null)
                        {
                            if (lockOnTarget_passive != null)
                            {
                                trajScr.LockOnVisibilityRay.RayRoutine();

                                if (trajScr.LockOnVisibilityRay.ray_surface == null)
                                {
                                    lockOnMode = true;
                                }
                            }
                        }
                    }




                    lockOn_axisUsed = true;
                }
            }
            else
            {
                lockOn_axisUsed = false;
            }

            if (lockOnMode)
            {
                if (LockOnTarget == null)
                {
                    if (lockOnTarget_passive != null)
                    {
                        SetLOVRDir();
                        trajScr.LockOnVisibilityRay.RayRoutine();

                        if (trajScr.LockOnVisibilityRay.ray_surface == null)
                        {
                            LockOnTarget = lockOnTarget_passive;

                        }
                        else
                        {
                            lockOnMode = false;
                        }
                    }
                    else
                    {
                        LockOnTarget = null;
                    }


                }
                else
                {
                    if (LockOnTarget.GetComponent<EnemyAsReactionary>() != null)
                    {
                        if (LockOnTarget.GetComponent<Enemy>() == null)
                        {
                            LockOnTarget = null;
                            lockOnMode = false;
                        }
                    }
                }

            }
            else
            {
                LockOnTarget = null;
            }



        }

    }
    private void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(rawLookAt_inst);
    }

    
    
    // Update is called once per frame
    void FixedUpdate()
    {

        if (FindDependencies() && FindResources())
        {
            if (MoveScrSetLookTarget == true)
            {
                pivot = new Vector3(LookTarget.x, gameObject.transform.position.y, LookTarget.z);
                if (lateAwake == false)
                {
                    finalPos = gameObject.transform.position;
                    targetYPos = finalPos.y;

                    camDistance = Vector3.Distance(pivot, gameObject.transform.position);
                    CamDistance_Orig = camDistance;
                    lateAwake = true;

                }

                if (lateAwake == true)
                {
                    camOpMode = CamModeDecider();




                    switch (camOpMode)
                    {
                        case CameraOperationMode.Explore:
                            if (rawLookAt_inst == null)
                            {
                                rawLookAt_inst = new GameObject();

                            }

                            if (rawLookAt_inst != null)
                            {
                                rawLookAt_inst.name = "rawLookAt";
                                rawLookAt_inst.transform.position = gameObject.transform.position;
                                rawLookAt_inst.transform.LookAt(LookTarget);


                                gameObject.transform.forward = new Vector3(rawLookAt_inst.transform.forward.x, Mathf.Lerp(gameObject.transform.forward.y, rawLookAt_inst.transform.forward.y, 3 * Time.fixedDeltaTime), rawLookAt_inst.transform.forward.z);


                                finalPos = Vector3.Lerp(gameObject.transform.position, new Vector3(gameObject.transform.position.x, targetYPos, gameObject.transform.position.z), 5 * Time.fixedDeltaTime);











                                if (Vector3.Distance(pivot, gameObject.transform.position) != camDistance)
                                {
                                    finalPos += FlatForward * (Vector3.Distance(pivot, gameObject.transform.position) - camDistance);
                                }


                                //cam left/right rotation (WIP, only kind of works)
                                /*
                                float camTurn_input = 0;
                                switch (Constants.CONTROLLER)
                                {
                                    case ControlType.Controller:
                                        camTurn_input = inputScr.GetInputByName("Camera", "CamTurn_Controller");
                                        break;
                                    case ControlType.Keyboard:
                                        camTurn_input = inputScr.GetInputByName("Camera", "CamTurn");
                                        break;

                                }

                                curTurnAngle += camTurnRate * camTurn_input * Time.fixedDeltaTime;

                                Vector3 postTurnPos_old = postTurnPos;

                                postTurnPos = new Vector3(Mathf.Sin(curTurnAngle), 0, Mathf.Cos(curTurnAngle)) * CamDistance_Orig;

                                Vector3 postTurnPos_result = postTurnPos - postTurnPos_old;

                                finalPos += postTurnPos_result;
                                */


                            }



                            break;

                        case CameraOperationMode.LockOn:
                            if (LockOnTarget != null)
                            {
                                finalPos = moveScr.transform.position + (gameObject.transform.forward * trajScr.LockOnPos.z) + (gameObject.transform.right * trajScr.LockOnPos.x) + (gameObject.transform.up * trajScr.LockOnPos.y);
                                gameObject.transform.forward = Vector3.Lerp(gameObject.transform.forward, (LockOnTarget.transform.position - gameObject.transform.position).normalized, 5 * Time.deltaTime);


                            }



                            break;

                    }
                    FlatForward = new Vector3(gameObject.transform.forward.x, 0, gameObject.transform.forward.z).normalized;
                    FlatRight = new Vector3(gameObject.transform.right.x, 0, gameObject.transform.right.z).normalized;

                    gameObject.transform.position = finalPos;
                }
            }



        }

    }
    




    CameraOperationMode CamModeDecider()
    {
        CameraOperationMode prevOp = camOpMode;

        CameraOperationMode ret = CameraOperationMode.Explore;

        if (FindDependencies() && FindResources())
        {
            if (LockOnTarget != null )
            {
                ret = CameraOperationMode.LockOn;
            }

            if (prevOp != CameraOperationMode.Explore && ret == CameraOperationMode.Explore)
            {



                gameObject.transform.position = gameObject.transform.position + (gameObject.transform.forward * trajScr.RelativeCamSpawn.z) + (gameObject.transform.right * trajScr.RelativeCamSpawn.x) + (gameObject.transform.up * trajScr.RelativeCamSpawn.y);
                finalPos = gameObject.transform.position;
            }


            if(ret==CameraOperationMode.LockOn && prevOp!= CameraOperationMode.LockOn)
            {
                Instantiate(sE_lockOnActivate_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            }

            if (ret!= CameraOperationMode.LockOn && prevOp == CameraOperationMode.LockOn)
            {
                Instantiate(sE_lockOnDeactivate_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

            }

        }

        return ret;
    }
    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = GameObject.FindObjectOfType<InputManager>();
        }


        if (moveScr == null)
        {
            moveScr = GameObject.FindObjectOfType<PlayerMovement>();
        }

        if(trajScr == null)
        {
            trajScr = GameObject.FindObjectOfType<PlayerCameraAndMovementTrajectoryCalculation>();
        }


        if ( trajScr != null && moveScr != null && inputScr!=null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_lockOnDeactivate_prefab == null)
        {
            sE_lockOnDeactivate_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/se_lockOnDeactivate");
        }

        if (sE_lockOnActivate_prefab == null)
        {
            sE_lockOnActivate_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/se_lockOnActivate");
        }

        if(sE_lockOnDeactivate_prefab!=null && sE_lockOnActivate_prefab != null)
        {
            ret = true;
        }


        return ret;
    }
    
}
