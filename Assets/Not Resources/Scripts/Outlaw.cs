﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outlaw : Enemy
{
    [SerializeField] float snipeTimer;
    private float snipeTimer_current;
    [SerializeField] float snipePauseTimer;
    private float snipePauseTimer_current;
    private GameObject snipeLine_prefab;
    private GameObject snipeLine_inst;
    private GenericRay snipeRay;

    private EnemyModelAnimator_Outlaw outAnimScr;

    private bool lateAwake = false;

    private AudioSource outlawAimSrc;

    private GameObject sE_outlawLaugh_prefab;
    [SerializeField] Vector2 laughTimer_range;
    private float laughTimer_current;

    private bool alertSndPlayed = false;
    private GameObject sE_outlawAlert_prefab;
    [SerializeField] Vector2 sniperPitch_range;
    [SerializeField] float sniperFlash_secondsBeforeFire;
    [SerializeField] float sniperFlashTime;
    private float sniperFlashTime_current;
    private bool sniperFlash;
    private GameObject sE_outlawFlash_prefab;
    void ResetSniper()
    {
        if(FindDependencies() && FindResources())
        {


            ExtendedFunctionality.SafeDestroy(snipeLine_inst);

            snipeTimer_current = snipeTimer;
            snipePauseTimer_current = snipePauseTimer;
        }

    }

    protected override void Awake()
    {
        //keep this here because i want to run enemy.awake later than awake in this instance
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        ExtendedFunctionality.SafeDestroy(snipeLine_inst);
    }

    protected override void FixedUpdate()
    {
        if(FindDependencies() && FindResources())
        {
            if (lateAwake)
            {
                base.FixedUpdate();
            }
        }
    }


    private void LateUpdate()
    {
        if(FindDependencies() && FindResources())
        {
            if (lateAwake == false)
            {
                base.Awake();
                ResetSniper();

                lateAwake = true;
            }
            else
            {

                if(ChaseMode==true && AlertTime_Current > 0)
                {
                    if (alertSndPlayed == false)
                    {
                        Instantiate(sE_outlawAlert_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        alertSndPlayed = true;
                    }
                }
                else
                {
                    alertSndPlayed = false;

                }

                if (ChaseMode == false)
                {
                    if (laughTimer_current > 0)
                    {
                        laughTimer_current -= Time.deltaTime;
                    }
                    else
                    {
                        Instantiate(sE_outlawLaugh_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        laughTimer_current = Random.Range(laughTimer_range.x, laughTimer_range.y);


                    }
                }
                else
                {
                    laughTimer_current = Random.Range(laughTimer_range.x, laughTimer_range.y);

                    if (AlertTime_Current <= 0 && FrozenTimer_Current <= 0)
                    {


                        if (snipeLine_inst == null)
                        {
                            snipeLine_inst = Instantiate(snipeLine_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

                        }
                        else
                        {
                            Vector3 snipeVec = playerScr.transform.position - snipeRay.transform.position;
                            snipeRay.customDir_vec = snipeVec.normalized;
                            snipeRay.rayLength = snipeVec.magnitude;
                            snipeRay.RayRoutine();



                            LineRenderer snipeLine = snipeLine_inst.GetComponent<LineRenderer>();

                            if (snipeLine != null)
                            {
                                snipeLine.useWorldSpace = true;
                                if (snipeTimer_current > 0)
                                {
                                    snipeLine.positionCount = 2;
                                    if (snipeRay.ray_surface == null)
                                    {
                                        if (snipeTimer_current > sniperFlash_secondsBeforeFire)
                                        {
                                            snipeLine.SetPosition(0, snipeRay.transform.position);
                                            snipeLine.SetPosition(1, playerScr.transform.position);
                                        }
                                        else
                                        {
                                            if (sniperFlash)
                                            {
                                                snipeLine.positionCount = 1;

                                                snipeLine.SetPosition(0, Vector3.zero);

                                            }
                                            else{
                                                snipeLine.SetPosition(0, snipeRay.transform.position);
                                                snipeLine.SetPosition(1, playerScr.transform.position);
                                            }
                                            if (sniperFlashTime_current > 0)
                                            {
                                                sniperFlashTime_current -= Time.deltaTime;
                                            }
                                            else
                                            {
                                                sniperFlashTime_current = sniperFlashTime;

                                                sniperFlash = !sniperFlash;
                                                if (sniperFlash == true)
                                                {
                                                    Instantiate(sE_outlawFlash_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                                }
                                            }
                                        }


                                        outlawAimSrc.loop = true;
                                        outlawAimSrc.pitch = Mathf.Lerp(sniperPitch_range.x, sniperPitch_range.y, (float)(snipeTimer - snipeTimer_current) / (float)snipeTimer);
                                        if (!outlawAimSrc.isPlaying)
                                        {
                                            outlawAimSrc.Play();
                                        }
                                    }
                                    else
                                    {
                                        snipeLine.SetPosition(0, snipeRay.transform.position);
                                        snipeLine.SetPosition(1, snipeRay.ray_hitPoint);
                                        if (outlawAimSrc.isPlaying)
                                        {
                                            outlawAimSrc.Stop();
                                        }
                                        snipeTimer_current = snipeTimer;
                                    }


                                }
                                else
                                {
                                    if (outlawAimSrc.isPlaying)
                                    {
                                        outlawAimSrc.Stop();
                                    }
                                    snipeLine.positionCount = 1;
                                    snipeLine.SetPosition(0, Vector3.zero);
                                }
                            }
                            else
                            {
                                if (outlawAimSrc.isPlaying)
                                {
                                    outlawAimSrc.Stop();
                                }
                            }


                        }

                        if (snipeTimer_current > 0)
                        {
                            snipePauseTimer_current = 0;
                            snipeTimer_current -= Time.deltaTime;

                            if (snipeTimer_current <= 0)
                            {
                                if (snipeRay.ray_surface == null)
                                {
                                    outAnimScr.FireAnimDue = true;
                                    PlayerHurt hurtScr = playerScr.GetComponent<PlayerHurt>();
                                    if (hurtScr != null)
                                    {
                                        hurtScr.GetHurt();
                                    }
                                }

                                snipePauseTimer_current = snipePauseTimer;
                            }
                        }
                        else
                        {
                            if (snipePauseTimer_current > 0)
                            {
                                snipeTimer_current = 0;
                                snipePauseTimer_current -= Time.deltaTime;
                            }
                            else
                            {
                                snipeTimer_current = snipeTimer;
                            }

                        }
                    }
                    else
                    {
                        if (outlawAimSrc.isPlaying)
                        {
                            outlawAimSrc.Stop();
                        }
                    }
                }

                bool completelyInactive = !(ChaseMode && AlertTime_Current <= 0 && FrozenTimer_Current <= 0);

                if(completelyInactive)
                {



                    if (outlawAimSrc.isPlaying)
                    {
                        outlawAimSrc.Stop();

                    }
                    ResetSniper();
                }
            }





        }

    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (snipeLine_prefab == null)
        {
            snipeLine_prefab = Resources.Load<GameObject>("Prefabs/Enemies/snipeLine");
        }

        if (sE_outlawLaugh_prefab == null)
        {
            sE_outlawLaugh_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_outlawLaugh");
        }

        if (sE_outlawAlert_prefab == null)
        {
            sE_outlawAlert_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_outlawAlert");
        }

        if (sE_outlawFlash_prefab == null)
        {
            sE_outlawFlash_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_outlawFlash");
        }

        if (sE_outlawFlash_prefab!=null && sE_outlawAlert_prefab != null && sE_outlawLaugh_prefab != null && snipeLine_prefab != null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet= base.FindDependencies();

        if (snipeRay == null)
        {
            GameObject snipeRayObj = GameObject.FindGameObjectWithTag("outlaw_snipeRay");
            if (snipeRayObj != null)
            {
                if (snipeRayObj.transform.IsChildOf(gameObject.transform))
                {
                    snipeRay = snipeRayObj.GetComponent<GenericRay>();
                }
            }

            
        }

        if (outAnimScr == null)
        {
            outAnimScr = gameObject.GetComponent<EnemyModelAnimator_Outlaw>();
        }

        if (outlawAimSrc == null)
        {
            outlawAimSrc = gameObject.GetComponent<AudioSource>();
        }

        if (outlawAimSrc!=null && outAnimScr != null && snipeRay != null && parRet==true)
        {
            ret = true;
        }

        return ret;
    }
}
