﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum CurrentWeapon
{
    Dash,
    SprayerFire,
    SprayerIce,
    Rifle,
    Lob
}
public class PlayerComponentManager : MonoBehaviour
{
    private PlayerStats statScr;
    private PlayerMovement moveScr;
    private PlayerJump jumpScr;
    

    private PlayerGravity gravScr;

    private PlayerHurt hurtScr;

    public CurrentWeapon WeaponEnum { get; private set; }

    private PlayerDash dashScr;


    private InputManager inputScr;
    private bool weaponSwitch_axisUsed;

    private List<PlayerWeapon> allWeapons = new List<PlayerWeapon>();
    private bool unStunnedWeaponRefreshTime = true;

    private GameObject sE_weaponChange_prefab;
    // Start is called before the first frame update
    void Awake()
    {
        if (FindDependencies() && FindResources())
        {
            allWeapons = gameObject.GetComponents<PlayerWeapon>().ToList();

        }

    }

    void RefreshActiveWeapon(bool disableAll=false)
    {
        if (FindDependencies() && FindResources())
        {
            foreach (PlayerWeapon wpn in allWeapons)
            {
                if (wpn != null)
                {
                    if (disableAll == false)
                    {
                        if (wpn.WeaponEnum == WeaponEnum)
                        {
                            wpn.enabled = true;
                        }
                        else
                        {
                            wpn.enabled = false;
                        }
                    }
                    else
                    {
                        wpn.enabled = false;
                    }

                }
            }
        }

    }


    // Update is called once per frame
    void Update()
    {
        if (FindDependencies() && FindResources())
        {

            if(dashScr.DashDecideDuration_Current>0 || dashScr.DashDuration_Current > 0 || statScr.Dead)
            {
                gravScr.enabled = false;
            }
            else
            {
                gravScr.enabled = true;
            }

            if (hurtScr.StunType == StunnedType.NotStunned)
            {
                if (unStunnedWeaponRefreshTime == true)
                {
                    RefreshActiveWeapon();
                    unStunnedWeaponRefreshTime = false;
                }
                if (inputScr.allInputValues[inputScr.GetGroupIndexByName("WeaponSwitch")].HasActivity() && dashScr.DashDuration_Current <= 0)
                {
                    if (weaponSwitch_axisUsed == false)
                    {
                        if (inputScr.GetInputByName("WeaponSwitch", "ChangeWeapon") < 0)
                        {
                            WeaponEnum=(CurrentWeapon) ExtendedFunctionality.WrapValue((int)WeaponEnum, -1, System.Enum.GetValues(typeof(CurrentWeapon)).Cast<int>().Min(), System.Enum.GetValues(typeof(CurrentWeapon)).Cast<int>().Max());
                        }

                        if (inputScr.GetInputByName("WeaponSwitch", "ChangeWeapon") > 0)
                        {
                            WeaponEnum = (CurrentWeapon)ExtendedFunctionality.WrapValue((int)WeaponEnum, 1, System.Enum.GetValues(typeof(CurrentWeapon)).Cast<int>().Min(), System.Enum.GetValues(typeof(CurrentWeapon)).Cast<int>().Max());
                        }
                        Instantiate(sE_weaponChange_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        RefreshActiveWeapon();

                        weaponSwitch_axisUsed = true;
                    }
                }
                else
                {
                    weaponSwitch_axisUsed = false;
                }






                if (dashScr.DashDecideDuration_Current > 0)
                {
                    
                    jumpScr.enabled = false;
                    moveScr.enabled = false;
                }
                else
                {

                    if (dashScr.DashDuration_Current > 0)
                    {
                        jumpScr.enabled = false;
                        moveScr.enabled = false;
                    }
                    else
                    {
                        jumpScr.enabled = true;
                        moveScr.enabled = true;
                    }
                }

            }
            else
            {

                jumpScr.enabled = false;
                moveScr.enabled = false;
                RefreshActiveWeapon(true);
                unStunnedWeaponRefreshTime = true;
            }



        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (moveScr == null)
        {
            moveScr = gameObject.GetComponent<PlayerMovement>();
        }

        if (jumpScr == null)
        {
            jumpScr = gameObject.GetComponent<PlayerJump>();
        }

        if (dashScr == null)
        {
            dashScr = gameObject.GetComponent<PlayerDash>();
        }



        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<PlayerGravity>();
        }

        if (hurtScr == null)
        {
            hurtScr = gameObject.GetComponent<PlayerHurt>();
        }

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (statScr == null)
        {
            statScr = gameObject.GetComponent<PlayerStats>();
        }

        if (statScr!=null && inputScr != null && jumpScr != null && moveScr != null && dashScr!=null && gravScr!=null && hurtScr!=null)
        {
            ret = true;
        }
        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_weaponChange_prefab == null)
        {
            sE_weaponChange_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_weaponChange");
        }

        if (sE_weaponChange_prefab != null)
        {
            ret = true;
        }
        return ret;
    }
}
