﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleProjectile : Entity
{
    [SerializeField] GenericRay wallRay;
    [SerializeField] GenericRay obstructionRay;
    [SerializeField] GenericRay floorRay;
    [SerializeField] float awarenessRadius;
    [SerializeField] float enemyFindRate;
    private float enemyFindRate_current;
    [SerializeField] float lockOnRate;

    private Collider trig;

    public Vector3 moveDir { private get; set; }
    [SerializeField] float moveSpeed;

    private GameObject nearestEnemy;
    [SerializeField] float rateOfFireToDamageMultiple;
    private PlayerRifle rifleScr;
    [SerializeField] float errorCorrectionRate;
    [SerializeField] float fullLockRange;

    protected override void Awake()
    {
        if (FindDependencies() && base.FindResources())
        {
            base.Awake();
            enemyFindRate_current = enemyFindRate;

        }
    }

    GameObject FindNearestEnemy()
    {

        GameObject nearest = null;
        if (FindDependencies() && base.FindResources())
        {
            Enemy[] allEnemies = GameObject.FindObjectsOfType<Enemy>();

            foreach (Enemy en in allEnemies)
            {
                if (en != null)
                {
                    if (Vector3.Distance(en.transform.position, gameObject.transform.position) <= awarenessRadius)
                    {
                        nearest = en.gameObject;
                        break;
                    }
                }
            }
        }

        return nearest;
    }

    private void Update()
    {
        if (FindDependencies() && base.FindResources())
        {
            if (enemyFindRate_current > 0)
            {
                enemyFindRate_current -= Time.deltaTime;
            }
            else
            {
                nearestEnemy = FindNearestEnemy();
                enemyFindRate_current = enemyFindRate;
            }
        }

    }
    private void FixedUpdate()
    {
        if (FindDependencies() && base.FindResources())
        {
            if (trig.isTrigger == false)
            {
                trig.isTrigger = true;
            }

            wallRay.customDir_vec = moveDir.normalized;
            wallRay.rayLength = moveSpeed * Time.fixedDeltaTime;
            gameObject.transform.position += moveDir.normalized * moveSpeed * Time.fixedDeltaTime;

            if (wallRay.ray_surface != null)
            {
                Chest chestCheck = wallRay.ray_surface.GetComponent<Chest>();

                if(chestCheck != null)
                {
                    if (chestCheck.ChestTypeEnum == ChestType.Rifle || chestCheck.ChestTypeEnum == ChestType.Any)
                    {
                        Destroy(chestCheck.gameObject);
                        
                    }
                }
                Destroy(gameObject);
            }

            if (nearestEnemy != null)
            {
                Vector3 meToEnemy = (nearestEnemy.transform.position - gameObject.transform.position);
                obstructionRay.customDir_vec = meToEnemy.normalized;
                obstructionRay.rayLength = meToEnemy.magnitude;

                if (obstructionRay.ray_surface != null)
                {
                    if (nearestEnemy.transform.position.y > gameObject.transform.position.y)
                    {
                        moveDir = Vector3.Lerp(moveDir, Vector3.up, errorCorrectionRate * Time.fixedDeltaTime);
                    }
                    else
                    {
                        moveDir = Vector3.Lerp(moveDir, Vector3.down, errorCorrectionRate * Time.fixedDeltaTime);

                    }
                }
                else
                {
                    if (floorRay.ray_surface != null)
                    {
                        moveDir = Vector3.Lerp(moveDir, Vector3.up, errorCorrectionRate * Time.fixedDeltaTime);

                    }
                    else
                    {
                        if(meToEnemy.magnitude> fullLockRange)
                        {
                            moveDir = Vector3.Lerp(moveDir, meToEnemy.normalized, lockOnRate * ((float)awarenessRadius / (float)(Mathf.Abs(awarenessRadius - Vector3.Distance(gameObject.transform.position, nearestEnemy.transform.position)))) * Time.fixedDeltaTime);

                        }
                        else
                        {
                            moveDir = meToEnemy.normalized;

                        }

                    }
                }


            }








        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(FindDependencies() && base.FindResources())
        {
            Enemy enCheck = other.GetComponent<Enemy>();

            if (enCheck != null)
            {
                enCheck.ShiftHealth(Mathf.Abs(Mathf.RoundToInt( rifleScr.TimeBetweenShots*rateOfFireToDamageMultiple)) * -1, EnemyCauseOfDamage.Rifle);
                Destroy(gameObject);
            }


        }

        
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (trig == null)
        {
            trig = gameObject.GetComponent<Collider>();
        }
        if (rifleScr == null)
        {
            rifleScr = GameObject.FindObjectOfType<PlayerRifle>();
        }


        if (trig != null && rifleScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
