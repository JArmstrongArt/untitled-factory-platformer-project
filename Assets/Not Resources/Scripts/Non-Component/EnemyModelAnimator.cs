﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RendAndOrigMatPair
{
    public Renderer rend;
    public List<Material> origMats;

    public RendAndOrigMatPair(Renderer r)
    {
        rend = r;
        origMats = new List<Material>();
    }
}

[DefaultExecutionOrder(200)]
public abstract class EnemyModelAnimator : ModelAnimator
{
    private Material enemyFreezeProgressMat;
    private Material enemyFreezeMat;
    protected Material enemyDmgMat;
    private Material enemyWarnMat;
    protected Vector3 mdlOrigScale;
    protected Vector3 mdlEffectScale = Vector3.one;
    [SerializeField] float mdlScaleShrinkRate;



    List<RendAndOrigMatPair> origMatPairings = new List<RendAndOrigMatPair>();


    private bool hurtBlink = false;
    private bool warnBlink = false;

    protected Enemy enScr;

    private GameObject pS_flames_prefab;
    private GameObject pS_snowflakes_prefab;

    private GameObject pS_flames_inst;
    private GameObject pS_snowflakes_inst;


    [SerializeField] float pSTimer;
    private float pSTimer_current;
    [SerializeField] float pSScale;

    protected void SetMdlPath_OneTimeOnly(string newPath)
    {
        if (mdlPath == "" || mdlPath==null)
        {
            mdlPath = newPath;
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        ExtendedFunctionality.SafeDestroy(pS_flames_inst);
        ExtendedFunctionality.SafeDestroy(pS_snowflakes_inst);
    }
    void SetModelFrozen(bool frozen)
    {
        if (FindDependencies() && FindResources())
        {
            if (mdl_inst_anim != null)
            {
                if (frozen == false)
                {

                    foreach (RendAndOrigMatPair pair in origMatPairings)
                    {
                        pair.rend.materials = pair.origMats.ToArray();
                    }
                    mdl_inst_anim.speed = 1;
                }
                else
                {

                    foreach (RendAndOrigMatPair pair in origMatPairings)
                    {
                        Material[] rendMatCopy = pair.rend.materials;
                        //pair.rend.materials = new Material[] { enemyFreezeMat };
                        pair.rend.materials = new Material[pair.origMats.Count];
                        for(int i=0;i< rendMatCopy.Length;i++)
                        {
                            rendMatCopy[i] = enemyFreezeMat;
                        }
                        pair.rend.materials = rendMatCopy;
                    }
                    mdl_inst_anim.speed = Mathf.Lerp(mdl_inst_anim.speed, 0, 5 * Time.deltaTime);

                }
            }

        }

    }

    protected override void Awake()
    {


        

        if (FindResources() && FindDependencies())
        {

            SetMdlPath_OneTimeOnly("Prefabs/Enemies/enemyPlaceholderModel");
            base.Awake();

            mdlOrigScale = new Vector3(Mdl_Inst.transform.localScale.x, Mdl_Inst.transform.localScale.y, Mdl_Inst.transform.localScale.z);
            origMatPairings = ExtendedFunctionality.GeneratePairingsFromCurrentRends(Mdl_Inst);
            /*
            List<Renderer> allMdlRenderers = ExtendedFunctionality.GetAllRenderersInChildren(Mdl_Inst);
            foreach (Renderer rend in allMdlRenderers)
            {
                if (rend != null)
                {
                    RendAndOrigMatPair curPair = new RendAndOrigMatPair(rend);

                    foreach (Material mat in rend.materials)
                    {
                        curPair.origMats.Add(mat);
                    }

                    origMatPairings.Add(curPair);
                }
            }
            */
            enScr.GenerateHurtRegions();
        }
    }

    protected virtual void LateUpdate()
    {
        if(FindDependencies() && FindResources())
        {
            if (pS_flames_inst == null)
            {
                pS_flames_inst = Instantiate(pS_flames_prefab, gameObject.transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)), null);
                Vector3 scl = pS_flames_inst.transform.localScale;
                pS_flames_inst.transform.localScale = new Vector3(scl.x * pSScale, scl.y * pSScale, scl.z * pSScale);
            }
            else
            {
                pS_flames_inst.transform.position = gameObject.transform.position;
            }

            if (pS_snowflakes_inst == null)
            {
                pS_snowflakes_inst = Instantiate(pS_snowflakes_prefab, gameObject.transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)), null);
                Vector3 scl = pS_snowflakes_inst.transform.localScale;
                pS_snowflakes_inst.transform.localScale = new Vector3(scl.x * pSScale, scl.y * pSScale, scl.z * pSScale);
            }
            else
            {
                pS_snowflakes_inst.transform.position = gameObject.transform.position;
            }
        }

    }

    protected override void Update()
    {
        if (FindResources() && FindDependencies())
        {
            base.Update();
            if (mdl_inst_anim != null)
            {
                if (enScr.ChaseMode)
                {
                    if (enScr.AlertTime_Current > 0)
                    {
                        mdl_inst_anim.Play("attackWarn");

                    }
                }



                if (pSTimer_current > 0)
                {
                    pSTimer_current -= Time.deltaTime;

                    if (enScr.DmgTypeEnum == DamageType.Freeze)
                    {
                        if (pS_snowflakes_inst != null)
                        {
                            if (!pS_snowflakes_inst.GetComponent<ParticleSystem>().isPlaying)
                            {
                                pS_snowflakes_inst.GetComponent<ParticleSystem>().Play();

                            }


                        }

                        if (pS_flames_inst != null)
                        {
                            pS_flames_inst.GetComponent<ParticleSystem>().Stop();
                        }

                    }
                    if (enScr.DmgTypeEnum == DamageType.Fire)
                    {
                        if (pS_flames_inst != null)
                        {
                            if (!pS_flames_inst.GetComponent<ParticleSystem>().isPlaying)
                            {
                                pS_flames_inst.GetComponent<ParticleSystem>().Play();

                            }


                        }


                        if (pS_snowflakes_inst != null)
                        {
                            pS_snowflakes_inst.GetComponent<ParticleSystem>().Stop();
                        }
                    }
                }
                else
                {
                    pSTimer_current = 0;
                    if (pS_flames_inst != null)
                    {
                        pS_flames_inst.GetComponent<ParticleSystem>().Stop();
                    }

                    if (pS_snowflakes_inst != null)
                    {
                        pS_snowflakes_inst.GetComponent<ParticleSystem>().Stop();
                    }
                }


                bool modelBigEnoughToBlink = Mdl_Inst.transform.localScale.x - 0.01f > mdlOrigScale.x * mdlEffectScale.x || Mdl_Inst.transform.localScale.y - 0.01f > mdlOrigScale.y * mdlEffectScale.y || Mdl_Inst.transform.localScale.z - 0.01f > mdlOrigScale.z * mdlEffectScale.z;//0.01 added to account for floating point error
                bool alertBlinkAllowed = enScr.AlertTime_Current > 0 && enScr.ChaseMode == true;
                if (Mdl_Inst.transform.localScale.x > mdlOrigScale.x * mdlEffectScale.x)
                {
                    Mdl_Inst.transform.localScale -= new Vector3(mdlScaleShrinkRate * Time.deltaTime, 0, 0);
                }
                else
                {
                    Mdl_Inst.transform.localScale = new Vector3(mdlOrigScale.x * mdlEffectScale.x, Mdl_Inst.transform.localScale.y, Mdl_Inst.transform.localScale.z);
                }

                if (Mdl_Inst.transform.localScale.y > mdlOrigScale.y * mdlEffectScale.y)
                {
                    Mdl_Inst.transform.localScale -= new Vector3(0, mdlScaleShrinkRate * Time.deltaTime, 0);
                }
                else
                {
                    Mdl_Inst.transform.localScale = new Vector3(Mdl_Inst.transform.localScale.x, mdlOrigScale.y * mdlEffectScale.y, Mdl_Inst.transform.localScale.z);
                }


                if (Mdl_Inst.transform.localScale.z > mdlOrigScale.z * mdlEffectScale.z)
                {
                    Mdl_Inst.transform.localScale -= new Vector3(0, 0, mdlScaleShrinkRate * Time.deltaTime);
                }
                else
                {
                    Mdl_Inst.transform.localScale = new Vector3(Mdl_Inst.transform.localScale.x, Mdl_Inst.transform.localScale.y, mdlOrigScale.z * mdlEffectScale.z);
                }

                if (modelBigEnoughToBlink)
                {


                    if (hurtBlink == false)
                    {
                        foreach (RendAndOrigMatPair pair in origMatPairings)
                        {
                            if (enScr.FrozenTimer_Current <= 0)
                            {
                                pair.rend.materials = pair.origMats.ToArray();

                            }
                            else
                            {
                                SetModelFrozen(true);
                            }
                        }
                    }
                    else
                    {
                        foreach (RendAndOrigMatPair pair in origMatPairings)
                        {





                            Material[] rendMatCopy = pair.rend.materials;
                            //pair.rend.materials = new Material[] { enemyFreezeMat };
                            pair.rend.materials = new Material[pair.origMats.Count];
                            for (int i = 0; i < rendMatCopy.Length; i++)
                            {
                                if (enScr.DmgTypeEnum == DamageType.Freeze)
                                {
                                    rendMatCopy[i] = enemyFreezeProgressMat;

                                }
                                else
                                {
                                    rendMatCopy[i] = enemyDmgMat;

                                }
                            }
                            pair.rend.materials = rendMatCopy;




                        }
                    }
                    hurtBlink = !hurtBlink;
                }
                else
                {
                    if (alertBlinkAllowed)
                    {
                        if (warnBlink == false)
                        {
                            foreach (RendAndOrigMatPair pair in origMatPairings)
                            {
                                if (enScr.FrozenTimer_Current <= 0)
                                {
                                    pair.rend.materials = pair.origMats.ToArray();

                                }
                                else
                                {
                                    SetModelFrozen(true);
                                }
                            }
                        }
                        else
                        {
                            foreach (RendAndOrigMatPair pair in origMatPairings)
                            {
                                Material[] rendMatCopy = pair.rend.materials;
                                //pair.rend.materials = new Material[] { enemyFreezeMat };
                                pair.rend.materials = new Material[pair.origMats.Count];
                                for (int i = 0; i < rendMatCopy.Length; i++)
                                {
                                    rendMatCopy[i] = enemyWarnMat;
                                }
                                pair.rend.materials = rendMatCopy;
                            }
                        }
                        warnBlink = !warnBlink;
                    }
                    else
                    {
                        foreach (RendAndOrigMatPair pair in origMatPairings)
                        {
                            if (enScr.FrozenTimer_Current <= 0)
                            {
                                pair.rend.materials = pair.origMats.ToArray();

                            }
                            else
                            {
                                SetModelFrozen(true);
                            }
                        }
                    }
                }

                if (enScr.FrozenTimer_Current <= 0)
                {
                    if (!modelBigEnoughToBlink && !alertBlinkAllowed)
                    {
                        SetModelFrozen(false);

                    }



                    if (enScr.ChaseMode == false)
                    {
                        if (enScr.NoChaseMode_MoveDecideEnum == NoChaseMode_MoveDecide.NotMoving)
                        {
                            mdl_inst_anim.Play("idle");

                        }
                        else
                        {
                            mdl_inst_anim.Play("move");

                        }
                    }



                }
                else
                {
                    if (!modelBigEnoughToBlink && !alertBlinkAllowed)
                    {
                        SetModelFrozen(true);

                    }

                }

                enScr.HurtRegionToggle();


            }



        }
    }

    public virtual void HurtEffectTrigger(DamageType dmgEnum,EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {
        if (FindResources() && FindDependencies())
        {
            if (Mdl_Inst != null)
            {
                Mdl_Inst.transform.localScale = new Vector3(mdlOrigScale.x * mdlEffectScale.x * 1.3f, mdlOrigScale.y * mdlEffectScale.y * 1.3f, mdlOrigScale.z * mdlEffectScale.z * 1.3f);
                enScr.DmgTypeEnum = dmgEnum;
                if (dmgEnum != DamageType.Other)
                {
                    pSTimer_current = pSTimer;

                }
            }
        }

    }

    protected virtual bool  FindDependencies()
    {
        bool ret = false;

        if (enScr == null)
        {
            enScr = gameObject.GetComponent<Enemy>();
        }

        if (enScr != null)
        {
            ret = true;
        }

        return ret;

    }

    protected virtual bool FindResources()
    {
        bool ret = false;


        if (enemyDmgMat == null)
        {
            enemyDmgMat = Resources.Load<Material>("Materials/enemyDmgMat");
        }

        if (enemyWarnMat == null)
        {
            enemyWarnMat = Resources.Load<Material>("Materials/enemyWarnMat");
        }

        if (enemyFreezeMat == null)
        {
            enemyFreezeMat = Resources.Load<Material>("Materials/enemyFreezeMat");
        }

        if (enemyFreezeProgressMat == null)
        {
            enemyFreezeProgressMat = Resources.Load<Material>("Materials/enemyFreezeProgressMat");
        }

        if (pS_flames_prefab == null)
        {
            pS_flames_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_flames");
        }

        if (pS_snowflakes_prefab == null)
        {
            pS_snowflakes_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_snowflakes");
        }
        if (pS_flames_prefab!=null && pS_snowflakes_prefab != null && enemyFreezeProgressMat != null && enemyDmgMat != null && enemyWarnMat != null && enemyFreezeMat != null)
        {
            ret = true;
        }

        return ret;
    }


}
