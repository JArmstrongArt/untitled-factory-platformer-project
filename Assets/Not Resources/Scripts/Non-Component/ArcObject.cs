﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ArcObject : Entity
{
    protected Vector3 arcOrigin;
    protected Vector3 forwardDirection;
    protected float progressionRate;
    protected float arcAngle;
    [SerializeField] float gravRate;
    public float GravRate
    {
        set
        {
            gravRate = value;
        }
    }


    private bool arcBegun = false;

    [SerializeField] float totalArcTime;
    public float TotalArcTime
    {
        get
        {
            return totalArcTime;
        }
    }
    protected float totalArcTime_remaining;





    public Vector3 PredictArcPosition(float timeIntoArc, float power, Vector3 arcOr, Vector3 frontFacing, float angle)
    {

        if (base.FindResources())
        {
            float xPos = (power * (timeIntoArc) * Mathf.Cos(Mathf.Deg2Rad * angle));
            float yPos = ((power * (timeIntoArc) * Mathf.Sin(Mathf.Deg2Rad * angle)) - (0.5f * gravRate * (timeIntoArc) * (timeIntoArc)));

            Vector3 finalPos_proposed = arcOr + (frontFacing * xPos);
            return new Vector3(finalPos_proposed.x, arcOr.y + yPos, finalPos_proposed.z);
        }
        else
        {
            return arcOr;
        }
        

    }

    public void InitArc(float power, Vector3 lobPos, Vector3 frontFacing, float angle, float arcTime = -1)
    {

        if (base.FindResources())
        {
            arcOrigin = lobPos;
            forwardDirection = frontFacing;
            progressionRate = power;

            if (arcTime > 0)
            {
                totalArcTime = arcTime;
            }
            totalArcTime_remaining = TotalArcTime;
            arcAngle = angle;
            arcBegun = true;
        }
    }

    protected abstract void EndArcReact();

    protected virtual void FixedUpdate()
    {

        if (base.FindResources())
        {

            if (totalArcTime_remaining > 0)
            {
                if (arcBegun)
                {
                    totalArcTime_remaining -= Time.fixedDeltaTime;

                    gameObject.transform.position = PredictArcPosition(TotalArcTime - totalArcTime_remaining, progressionRate, arcOrigin, forwardDirection, arcAngle);
                }






            }
            else
            {

                if (arcBegun == true)
                {
                    EndArcReact();


                }
                totalArcTime_remaining = 0;
            }

        }


    }
}
