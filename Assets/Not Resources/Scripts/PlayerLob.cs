﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLob : PlayerWeapon,IEditorFunctionality
{
    [SerializeField] AudioSource lobAimSrc;
    [SerializeField] float minLobAngle;
    [SerializeField] float maxLobAngle;
    private float lobAngle_current;
    [SerializeField] float minLobPower;
    [SerializeField] float maxLobPower;
    private float lobPower_current;
    [SerializeField] float lobPower_riseRate;

    private GameObject lobUnit_prefab;
    private GameObject lobUnit_inst;
    private GameObject lobUnit_handPosition;
    private bool lobbed = false;

    public bool FirstLobbed { get; private set; }

    private PlayerModelAnimator mdlAnimScr;

    [SerializeField] float minimumReloadTime;//will extend if the lobbed object takes longer to destroy than the reload time.
    private float minimumReloadTime_current;

    private GameObject sE_lob_prefab;
    private GameObject sE_lobPrevent_prefab;

    private bool preventSnd_axisUsed;

    private GameObject lobLine_prefab;
    private GameObject lobLine_inst;

    [SerializeField] int lobLineLOD;
    [SerializeField] float percentOfLobLineToShow;

    private GameObject sE_lobReload_prefab;

    [SerializeField] Vector2 lobAim_pitchRange;
    public override void DisablePrepare()
    {
        ExtendedFunctionality.SafeDestroy(lobUnit_inst);
        lobPower_current =minLobPower;
        //lobbed = false;

        ExtendedFunctionality.SafeDestroy(lobLine_inst);
        if (lobAimSrc.isPlaying)
        {
            lobAimSrc.Stop();

        }
    }

    void LoadLobUnit()
    {
        if(base.FindDependencies() && FindResources())
        {
            if (lobUnit_inst == null)
            {
                lobUnit_inst = Instantiate(lobUnit_prefab, lobUnit_handPosition.transform.position, Quaternion.Euler(Vector3.zero), null);

            }

            MaintainOffset offScr = lobUnit_inst.GetComponent<MaintainOffset>();
            if (offScr == null)
            {
                offScr = lobUnit_inst.AddComponent<MaintainOffset>();
            }
            offScr.OffsetUpdate = UpdateType.Fixed;


            offScr.OffsetObj = lobUnit_handPosition;
            offScr.Init();
        }


    }

    public override void EnablePrepare()
    {
        if(base.FindDependencies() && FindResources())
        {
            //minimumReloadTime_current = minimumReloadTime;
            FirstLobbed = false;

            if (lobbed == false)
            {
                LoadLobUnit();


            }
            if (lobLine_inst == null)
            {
                lobLine_inst = Instantiate(lobLine_prefab, gameObject.transform.position, Quaternion.Euler(90, 0, 0), null);
            }

        }
    }



    private void Update()
    {
        if (base.FindDependencies() && FindResources())
        {
            LineRenderer lobLine = null;
            if (lobLine_inst != null)
            {
                lobLine = lobLine_inst.GetComponent <LineRenderer>() ;

            }
            if (lobbed == false)
            {
                minimumReloadTime_current = minimumReloadTime;
                if (inputScr.GetInputByName("Actions", "LobCharge") > 0)
                {
                    lobPower_current = Mathf.Clamp(lobPower_current + (lobPower_riseRate * Time.deltaTime), minLobPower, maxLobPower);

                    float lobPowerLerp = ((float)lobPower_current / (float)maxLobPower);
                    lobAngle_current = Mathf.Lerp(maxLobAngle, minLobAngle, lobPowerLerp);

                    lobAimSrc.pitch = Mathf.Lerp(lobAim_pitchRange.x, lobAim_pitchRange.y, lobPowerLerp);
                    if (!lobAimSrc.isPlaying)
                    {
                        lobAimSrc.Play();

                    }

                    if (lobUnit_inst != null)
                    {

                        LobUnit unitScr = lobUnit_inst.GetComponent<LobUnit>();

                        if (unitScr != null)
                        {
                            if (lobLine != null)
                            {
                                lobLineLOD = Mathf.Clamp(lobLineLOD, 1, int.MaxValue);
                                lobLine.useWorldSpace = true;

                                List<Vector3> lobLinePositions = new List<Vector3>();


                                for (int i = 0; i < lobLineLOD + 1; i++)
                                {
                                    lobLinePositions.Add(unitScr.PredictArcPosition(unitScr.TotalArcTime * ((float)i / (float)lobLineLOD), lobPower_current, lobUnit_inst.transform.position, gameObject.transform.forward, lobAngle_current));
                                }
                                //int oldCount = lobLinePositions.Count;

                                percentOfLobLineToShow = Mathf.Clamp(percentOfLobLineToShow, 0, 100);

                                int lobLinePositions_newCount = Mathf.RoundToInt(((float)lobLinePositions.Count / (float)100) * percentOfLobLineToShow);

                                while(lobLinePositions.Count> lobLinePositions_newCount)
                                {
                                    lobLinePositions.RemoveAt(lobLinePositions.Count - 1);
                                }

                                //print("orig position count: " + oldCount.ToString() + " | " + "new position count: " + lobLinePositions.Count.ToString() + " | " + "percent: " + percentOfLobLineToShow.ToString());


                                lobLine.positionCount = lobLinePositions.Count;




                                lobLine.SetPositions(lobLinePositions.ToArray());
                            }
                        }

                    }


                }
                else
                {
                    if (lobAimSrc.isPlaying)
                    {
                        lobAimSrc.Stop();

                    }
                    if (lobPower_current > minLobPower)
                    {

                        if (lobUnit_inst != null)
                        {

                            MaintainOffset offScr = lobUnit_inst.GetComponent<MaintainOffset>();
                            if (offScr != null)
                            {
                                Destroy(offScr);
                            }

                            LobUnit unitScr = lobUnit_inst.GetComponent<LobUnit>();
                            if (unitScr != null)
                            {
                                unitScr.InitArc(lobPower_current, lobUnit_inst.transform.position, gameObject.transform.forward, lobAngle_current);
                            }
                            else
                            {
                                ExtendedFunctionality.SafeDestroy(lobUnit_inst);
                            }

                            lobbed = true;

                            FirstLobbed = true;
                            mdlAnimScr.PlayLob();
                            Instantiate(sE_lob_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

                            lobPower_current = minLobPower;
                        }




                    }


                }
            }
            else
            {
                if (lobAimSrc.isPlaying)
                {
                    lobAimSrc.Stop();

                }
                if (lobLine != null)
                {
                    lobLine.positionCount = 1;
                    lobLine.SetPositions(new Vector3[1]);
                }
                if (inputScr.GetInputByName("Actions", "LobCharge") > 0)
                {
                    if (preventSnd_axisUsed == false)
                    {
                        Instantiate(sE_lobPrevent_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        preventSnd_axisUsed = true;
                    }

                }
                else
                {
                    preventSnd_axisUsed = false;
                }

                if (minimumReloadTime_current > 0)
                {
                    minimumReloadTime_current -= Time.deltaTime;
                }
                else
                {
                    if (lobUnit_inst == null)
                    {
                        Instantiate(sE_lobReload_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        LoadLobUnit();
                        lobbed = false;
                    }
                }

            }

        }
    }

    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();

        if (lobUnit_handPosition == null)
        {
            lobUnit_handPosition = GameObject.FindGameObjectWithTag("playerModel_lobUnit_handPosition");
        }

        if (mdlAnimScr == null)
        {
            mdlAnimScr = gameObject.GetComponent<PlayerModelAnimator>();
        }



        if (lobUnit_handPosition != null && mdlAnimScr!=null && parRet==true)
        {
            ret = true;
        }
        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (lobUnit_prefab == null)
        {
            lobUnit_prefab = Resources.Load<GameObject>("Prefabs/Entities/lobUnit");
        }

        if (sE_lob_prefab == null)
        {
            sE_lob_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_lob");
        }

        if (sE_lobPrevent_prefab == null)
        {
            sE_lobPrevent_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_lobPrevent");
        }

        if (lobLine_prefab == null)
        {
            lobLine_prefab = Resources.Load<GameObject>("Prefabs/Player/lobLine");
        }

        if (sE_lobReload_prefab == null)
        {
            sE_lobReload_prefab= Resources.Load<GameObject>("Prefabs/Sound Effects/sE_lobReload");
        }

        if (sE_lobReload_prefab!=null && lobLine_prefab != null && sE_lobPrevent_prefab != null && lobUnit_prefab != null && sE_lob_prefab!=null)
        {
            ret = true;
        }

        return ret;
    }



    public void EditorUpdate()
    {
        if (maxLobAngle < minLobAngle)
        {
            float temp = maxLobAngle;
            maxLobAngle = minLobAngle;
            minLobAngle = temp;
        }

        if (maxLobPower < minLobPower)
        {
            float temp = maxLobPower;
            maxLobPower = minLobPower;
            minLobPower = temp;
        }

        if(lobAim_pitchRange.y< lobAim_pitchRange.x)
        {
            float temp = lobAim_pitchRange.y;
            lobAim_pitchRange.y = lobAim_pitchRange.x;
            lobAim_pitchRange.x = temp;
        }

        percentOfLobLineToShow = Mathf.Clamp(percentOfLobLineToShow, 0, 100);
    }
}
