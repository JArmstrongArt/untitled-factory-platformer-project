﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum TriggerCall
{
    Enter,
    Stay,
    Exit
}
public class HurtEnemyInTrigger : MonoBehaviour
{
    [SerializeField] TriggerCall callEnum;
    [SerializeField] int damage;

    [SerializeField] bool timeStop;
    [SerializeField] float timeStopSpeed;
    [SerializeField] float timeStopLength;
    private float timeStopLength_current;

    private bool enemyHit;
    private bool healthDockTime;

    [SerializeField] bool playHitSound;

    private GameObject sE_dashHit_prefab;

    private Enemy curEnemy;

    public EnemyCauseOfDamage COD { private get; set; }

    [SerializeField] bool preventDoubleHits;
    private List<Enemy> enemiesHit = new List<Enemy>();
    void ResetTime()
    {
        Time.timeScale = 1.0f;
        timeStopLength_current = timeStopLength;
    }

    void OnDestroy()
    {
        ResetTime();
    }

    void Update()
    {
        if (FindResources())
        {
            if (enemyHit == true)
            {
                if (timeStopLength_current > 0)
                {
                    if (timeStop)
                    {
                        Time.timeScale = timeStopSpeed;
                        timeStopLength_current -= Time.unscaledDeltaTime;
                    }
                    else
                    {
                        timeStopLength_current = 0;
                    }

                }
                else
                {
                    
                    if (healthDockTime == true)
                    {
                        if (curEnemy != null)
                        {
                            if (curEnemy.FrozenTimer_Current <= 0)
                            {
                                curEnemy.ShiftHealth(Mathf.Abs(damage) * -1, COD);

                            }
                            else
                            {
                                curEnemy.DashFreezeKill(COD);

                            }
                            
                        }
                        healthDockTime = false;
                    }
                    enemyHit = false;
                    ResetTime();

                }
            }
            else
            {
                ResetTime();
            }
        }


    }

    void TrigResponse_Internal(Enemy enScr)
    {
        if ((preventDoubleHits && !enemiesHit.Contains(enScr)) || !preventDoubleHits) 
        {
            enemyHit = true;
            if (playHitSound == true)
            {
                Instantiate(sE_dashHit_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            }
            EnemyModelAnimator enAnim = enScr.GetComponent<EnemyModelAnimator>();
            if (enAnim != null)
            {
                enAnim.HurtEffectTrigger(DamageType.Other, COD);//start this effect early bc the shifthealth call for this wont occur til after the time slowdown has ended.
            }
            ResetTime();
            curEnemy = enScr;
            healthDockTime = true;
            if (preventDoubleHits)
            {
                enemiesHit.Add(enScr);
            }
        }


    }
    void TrigResponse(Collider col)
    {
        Enemy enCheck = col.GetComponent<Enemy>();
        if (enCheck != null)
        {
            ChikChik chikScr = enCheck.GetComponent<ChikChik>();
            if (chikScr == null)
            {
                TrigResponse_Internal(enCheck);
            }
            else
            {
                if (chikScr.ShieldStateEnum == ShieldState.Raised)
                {
                    if (COD != EnemyCauseOfDamage.Dash)
                    {
                        TrigResponse_Internal(enCheck);

                    }
                }
                else
                {
                    TrigResponse_Internal(enCheck);

                }
            }

            //removed from this call since i dont want the enemy to actually disappear until the time slowdown effect has ended
            //enCheck.ShiftHealth(Mathf.Abs(damage) * -1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (callEnum == TriggerCall.Enter)
        {
            TrigResponse(other);
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (callEnum == TriggerCall.Stay)
        {
            TrigResponse(other);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (callEnum == TriggerCall.Exit)
        {
            TrigResponse(other);
        }

    }




    bool FindResources()
    {
        bool ret = false;

        if (playHitSound == false)
        {
            ret = true;
        }
        else
        {
            if (sE_dashHit_prefab == null)
            {
                sE_dashHit_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_dashHit");
            }

            if (sE_dashHit_prefab != null)
            {
                ret = true;
            }
        }

        return ret;
    }
}
