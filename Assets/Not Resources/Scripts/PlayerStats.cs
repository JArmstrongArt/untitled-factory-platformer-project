﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerStats : Creature
{
    public bool Dead { get; private set; }
    private SceneSwitcherControl sceneSwitchScr;
    [SerializeField] float waitToReloadAfterDeathTime;
    private float waitToReloadAfterDeathTime_current;

    [HideInInspector]
    public List<int> collectedCoinIDs= new List<int>();
    [HideInInspector]

    public List<int> allCollectedIDs = new List<int>();

    private GameObject sE_playerHurt_prefab;
    private GameObject sE_playerDie_prefab;
    private GameObject sE_hurt_prefab;
    public override int ShiftHealth(int shiftAmount, EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {
        int ret = -1;
        if(FindResources() && FindDependencies())
        {
            ret = base.ShiftHealth(shiftAmount);

            int soundChance = Random.Range(0, 3);
            bool playSound = soundChance == 0 && shiftAmount<0;

            if (shiftAmount < 0)
            {
                Instantiate(sE_hurt_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);

            }



            if (CurrentHealth <= 0)
            {
                Dead = true;

                if (playSound)
                {

                    Instantiate(sE_playerDie_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }


            }
            else
            {
                Dead = false;

                if (playSound)
                {

                    Instantiate(sE_playerHurt_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }

            }
            inputScr.input_enabled = !Dead;

        }

        return ret;
    }


    private InputManager inputScr;
    private bool debug_axisUsed=false;





    protected override void Awake()
    {
        if (FindResources() && FindDependencies())
        {
            base.Awake();

            SaveLoadLive.LoadGame();
        }

    }

    void Update()
    {
        if(FindResources() && FindDependencies())
        {
            if (Dead)
            {
                if (waitToReloadAfterDeathTime_current > 0)
                {
                    waitToReloadAfterDeathTime_current -= Time.deltaTime;
                    if (waitToReloadAfterDeathTime_current <= 0)
                    {
                        sceneSwitchScr.UiAnim_SceneTransitionOut.Play();

                    }
                }

            }
            else
            {
                waitToReloadAfterDeathTime_current = waitToReloadAfterDeathTime;
            }

            if (inputScr.allInputValues[inputScr.GetGroupIndexByName("DEBUG")].HasActivity())
            {
                if (debug_axisUsed == false)
                {
                    if (inputScr.GetInputByName("DEBUG", "DBG_InstantSave") > 0)
                    {
                        SaveLoadLive.SaveGame();
                        print("saved" + Time.time.ToString());

                    }

                    if (inputScr.GetInputByName("DEBUG", "DBG_RestartScene") > 0)
                    {
                        sceneSwitchScr.UiAnim_SceneTransitionOut.Play();

                    }
                    debug_axisUsed = true;
                }
            }
            else
            {
                debug_axisUsed = false;

            }


        }
    }






    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (sceneSwitchScr == null)
        {
            sceneSwitchScr = GameObject.FindObjectOfType<SceneSwitcherControl>();
        }

        if (sceneSwitchScr!=null && inputScr != null)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (sE_playerHurt_prefab == null)
        {
            sE_playerHurt_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/Voice Lines/sE_playerHurt");
        }

        if (sE_playerDie_prefab == null)
        {
            sE_playerDie_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/Voice Lines/sE_playerDie");
        }

        if (sE_hurt_prefab == null)
        {
            sE_hurt_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_hurt");
        }


        if (sE_hurt_prefab != null && sE_playerDie_prefab != null && sE_playerHurt_prefab != null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
