﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDash : PlayerWeapon,IEditorFunctionality
{
    public GameObject NearestCollectable { get; set; }
    private PlayerCameraAndMovementTrajectoryCalculation trajScr;
    private CharacterController charScr;


    [SerializeField] float dashDuration;
    public float DashDuration_Current { get; private set; }

    [SerializeField] float dashSpeed;


    [SerializeField] float dashDecideDuration;
    public float DashDecideDuration_Current { get; private set; }



    private bool dash_axisUsed;

    private bool dashUsed;

    private PlayerGravity gravScr;

    private GameObject dashGuide_prefab;
    private GameObject dashGuide_inst;

    public float AscendingCoinPitchPercent { get; set; }
    public float AscendingCoinPitchPercent_Orig { get; private set; }

    private GameObject dashTrail_prefab;

    [SerializeField] float dashTrail_spawnRate;
    private float dashTrail_spawnRate_current;

    private GameObject sE_dash_prefab;

    private AudioSource dashDecideSrc;

    private bool dashSndSpawned;

    private bool airDashTaken;

    private GameObject dashHurtZone_prefab;
    private GameObject dashHurtZone_inst;

    public override void DisablePrepare()
    {
        if(FindResources() && FindDependencies())
        {
            ResetToNotUsing();

        }
    }

    public override void EnablePrepare()
    {
        if (FindResources() && FindDependencies())
        {
            ResetToNotUsing();
            
        }
    }

    void ResetDashDecindeSnd()
    {
        if(FindResources() && FindDependencies())
        {
            if (dashDecideSrc.isPlaying)
            {
                dashDecideSrc.Stop();
                dashDecideSrc.pitch = 1.0f;
            }
        }

    }

    void ResetToNotUsing()
    {
        if(FindResources() && FindDependencies())
        {
            ResetDashDecindeSnd();
            ExtendedFunctionality.SafeDestroy(dashHurtZone_inst);

            dashSndSpawned = false;
            dashTrail_spawnRate = 0;
            AscendingCoinPitchPercent = 100;
            AscendingCoinPitchPercent_Orig = AscendingCoinPitchPercent;
            gameObject.transform.forward = new Vector3(gameObject.transform.forward.x, 0, gameObject.transform.forward.z);
            NearestCollectable = null;
            dashUsed = false;
            dash_axisUsed = true;//so that you cant change into this weapon immediately deciding to dash.
            DashDuration_Current = 0;
            DashDecideDuration_Current = 0;
            ExtendedFunctionality.SafeDestroy(dashGuide_inst);

        }
    }

    private void Update()
    {
        if (FindResources() && FindDependencies())
        {

            if (gravScr.Grounded == true)
            {
                airDashTaken=false;
            }

            if (inputScr.GetInputByName("Actions", "Dash") > 0)
            {

                if (dash_axisUsed == false)
                {
                    if(gravScr.Grounded==true || (gravScr.Grounded==false && airDashTaken == false))
                    {
                        if (DashDuration_Current <= 0)
                        {
                            if (gravScr.Grounded == false)
                            {
                                airDashTaken = true;
                                gravScr.gravityCurrent = 0;//set this here because i dont want the dash to end and then a sudden shift up or down in gravity for the player if they began the dash falling or jumping
                            }
                            dashUsed = false;
                            DashDecideDuration_Current = dashDecideDuration;
                        }
                    }

                    dash_axisUsed = true;
                }


            }
            else
            {
                dash_axisUsed = false;
            }

            if( gravScr.Grounded==false && (DashDecideDuration_Current>0 || DashDuration_Current > 0))
            {
                gravScr.ConveyorMoveForcePrev = Vector3.zero;
            }

            if (DashDecideDuration_Current > 0)
            {
                
                DashDecideDuration_Current -= Time.deltaTime;


                float forPoint_input = 0;

                switch (Constants.CONTROLLER)
                {
                    case ControlType.Controller:
                        forPoint_input = inputScr.GetInputByName("Movement", "ForBack_Controller", true);

                        break;
                    case ControlType.Keyboard:
                        forPoint_input = inputScr.GetInputByName("Movement", "ForBack");
                        break;

                }


                float sidePoint_input = 0;
                switch (Constants.CONTROLLER)
                {
                    case ControlType.Controller:
                        sidePoint_input = inputScr.GetInputByName("Movement", "LeftRight_Controller", true);
                        break;
                    case ControlType.Keyboard:
                        sidePoint_input = inputScr.GetInputByName("Movement", "LeftRight");
                        break;

                }


                Vector3 dir = (trajScr.PlayerCam_Scr.FlatForward * forPoint_input) + (trajScr.PlayerCam_Scr.FlatRight * sidePoint_input);

                if (dir != Vector3.zero)
                {
                    gameObject.transform.forward = new Vector3(dir.x, 0, dir.z).normalized;


                }

                if (dashGuide_inst == null)
                {
                    dashGuide_inst = Instantiate(dashGuide_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }
                else
                {
                    dashGuide_inst.transform.position = gameObject.transform.position;
                    dashGuide_inst.transform.forward = -gameObject.transform.forward;

                }



                if (inputScr.GetInputByName("Actions", "Dash") <= 0)
                {
                    if (dashUsed == false)
                    {
                        CheckForMoreCollectables();
                        DashDecideDuration_Current = 0;
                        dashUsed = true;
                    }
                }

                if (!dashDecideSrc.isPlaying)
                {
                    dashDecideSrc.Play();
                }
                dashDecideSrc.pitch = Mathf.Lerp(1.0f, 1.4f,  Mathf.Clamp(  ((float)(dashDecideDuration - (DashDecideDuration_Current*0.5f))) / ((float)dashDecideDuration)   ,0,1) );

            }
            else
            {
                ExtendedFunctionality.SafeDestroy(dashGuide_inst);
                ResetDashDecindeSnd();

                dashUsed = false;
            }


        }

    }


    public void CheckForMoreCollectables()
    {
        if(FindResources() && FindDependencies())
        {
            DashDuration_Current = dashDuration;



            if (NearestCollectable != null)
            {

                gameObject.transform.forward = (NearestCollectable.transform.position - gameObject.transform.position).normalized;
            }
            else
            {

            }
        }



    }

    void LateUpdate()
    {
        if(FindResources() && FindDependencies())
        {
            if(DashDuration_Current > 0)
            {
                if (dashHurtZone_inst == null)
                {
                    dashHurtZone_inst = Instantiate(dashHurtZone_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    HurtEnemyInTrigger hrtScr = dashHurtZone_inst.GetComponent<HurtEnemyInTrigger>();
                    if (hrtScr != null)
                    {
                        hrtScr.COD = EnemyCauseOfDamage.Dash;
                    }
                }
                else
                {
                    dashHurtZone_inst.transform.position = gameObject.transform.position;

                }
            }
            else
            {
                ExtendedFunctionality.SafeDestroy(dashHurtZone_inst);
            }

        }
    }
    private void FixedUpdate()
    {
        if(FindResources() && FindDependencies())
        {
            if (DashDuration_Current > 0)
            {

                if (dashSndSpawned == false)
                {
                    Instantiate(sE_dash_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    dashSndSpawned = true;
                }
                charScr.Move(gameObject.transform.forward * dashSpeed * Time.fixedDeltaTime);
                DashDuration_Current -= Time.fixedDeltaTime;


                if (dashTrail_spawnRate_current > 0)
                {
                    dashTrail_spawnRate_current -= Time.deltaTime;
                }
                else
                {
                    Instantiate(dashTrail_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    dashTrail_spawnRate_current = dashTrail_spawnRate;
                }
                if (DashDuration_Current <= 0)
                {
                    ResetToNotUsing();
                }


            }
            else
            {
                dashTrail_spawnRate_current = dashTrail_spawnRate;
            }
        }

    }
    private void OnDestroy()
    {

        ExtendedFunctionality.SafeDestroy(dashGuide_inst);
        ExtendedFunctionality.SafeDestroy(dashHurtZone_inst);


    }


    bool FindResources()
    {
        bool ret = false;

        if (dashGuide_prefab == null)
        {
            dashGuide_prefab = Resources.Load<GameObject>("Prefabs/Player/dashGuide");
        }

        if (dashTrail_prefab == null)
        {

            dashTrail_prefab = Resources.Load<GameObject>("Prefabs/Entities/dashTrail");
        }

        if (sE_dash_prefab == null)
        {
            sE_dash_prefab= Resources.Load<GameObject>("Prefabs/Sound Effects/sE_dash");
        }

        if (dashHurtZone_prefab == null)
        {
            dashHurtZone_prefab = Resources.Load<GameObject>("Prefabs/Player/dashHurtZone");
        }
        if (dashGuide_prefab != null && dashTrail_prefab!=null && sE_dash_prefab!=null && dashHurtZone_prefab!=null)
        {
            ret = true;
        }

        return ret;
    }
    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();


        if (charScr == null)
        {
            charScr = gameObject.GetComponent<CharacterController>();
        }

        if (trajScr == null)
        {
            trajScr = gameObject.GetComponent<PlayerCameraAndMovementTrajectoryCalculation>();
        }

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<PlayerGravity>();
        }

        if (dashDecideSrc == null)
        {
            dashDecideSrc = gameObject.GetComponent<AudioSource>();
        }
        if (parRet==true && charScr!=null && trajScr!=null && gravScr!=null && dashDecideSrc!=null)
        {
            ret = true;
        }

        return ret;
    }

    public void EditorUpdate()
    {
        dashTrail_spawnRate = Mathf.Clamp(dashTrail_spawnRate, 0.01f, float.MaxValue);
    }
}
