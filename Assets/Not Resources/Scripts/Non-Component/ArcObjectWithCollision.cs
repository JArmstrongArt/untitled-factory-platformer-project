﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ArcObjectWithCollision : ArcObject
{
    [SerializeField] GenericRay collideRay;

    private Vector3 prevPos;

    protected abstract void CollideReact();

    protected override void FixedUpdate()
    {
        if (base.FindResources())
        {
            base.FixedUpdate();
            if(totalArcTime_remaining > 0)
            {
                if (prevPos != Vector3.zero)
                {
                    collideRay.customDir_vec = (gameObject.transform.position - prevPos).normalized;
                    collideRay.rayLength = (gameObject.transform.position - prevPos).magnitude * 1.5f;


                    if (collideRay.ray_surface != null)
                    {
                        CollideReact();
                    }


                }


                prevPos = gameObject.transform.position;
            }

        }

    }

    protected override void EndArcReact()
    {
        //just here bc Sure
        //i think it compiles bc this is an abstract class as a child of another abstract class but idk im just not comfortable leaving an abstract function unimplemented.
    }
}
