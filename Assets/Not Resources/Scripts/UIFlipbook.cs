﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
enum EndOfAnimAction
{
    Disappear,
    LoadScene

}

public class UIFlipbook : MonoBehaviour
{
    private Image animImg;
    [SerializeField] Sprite[] animFrames;
    private int animFrameIndex = -1;
    [SerializeField] float playbackRate;
    private float frameTimeRemaining;
    private float frameTimeRemaining_orig;
    [SerializeField] EndOfAnimAction animActionEnum;
    [SerializeField] string endOfAnimAction_loadScene_name;
    public void Play()
    {
        if (FindDependencies())
        {
            animFrameIndex = 0;
            frameTimeRemaining_orig = (float)1 / (float)playbackRate;
            frameTimeRemaining = frameTimeRemaining_orig;
        }

    }

    private void Update()
    {
        if (FindDependencies())
        {
            if (animFrameIndex >= 0 && animFrameIndex < animFrames.Length)
            {
                if (frameTimeRemaining > 0)
                {
                    frameTimeRemaining -= Time.deltaTime;
                }

                if (frameTimeRemaining <= 0)
                {
                    if (animFrameIndex + 1 < animFrames.Length)
                    {
                        animFrameIndex += 1;

                        animImg.sprite = animFrames[animFrameIndex];
                        animImg.enabled = true;

                    }
                    else
                    {
                        switch (animActionEnum)
                        {
                            case EndOfAnimAction.Disappear:
                                animFrameIndex = -1;
                                break;
                            case EndOfAnimAction.LoadScene:
                                SceneManager.LoadScene(endOfAnimAction_loadScene_name);
                                break;

                        }
                    }
                    frameTimeRemaining = frameTimeRemaining_orig;
                }



            }
            else
            {
                animImg.sprite = null;
                animImg.enabled = false;
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        if (animImg == null)
        {
            animImg = gameObject.GetComponent<Image>();
        }

        if (animImg != null)
        {
            ret = true;
        }

        return ret;
    }
}
