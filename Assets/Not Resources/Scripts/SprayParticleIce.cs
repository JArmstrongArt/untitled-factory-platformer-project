﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayParticleIce : SprayParticle
{
    [SerializeField] float freezePower;
    public override void OnParticleCollision(GameObject other)
    {
        Enemy enCheck = other.GetComponent<Enemy>();

        if (enCheck != null)
        {
            enCheck.ShiftFreezeProgress(freezePower);
        }
    }
}
