﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerWeapon : MonoBehaviour
{
    protected InputManager inputScr;
    [SerializeField] CurrentWeapon weaponEnum;
    public CurrentWeapon WeaponEnum
    {
        get
        {
            return weaponEnum;
        }
    }
    public abstract void DisablePrepare();

    public abstract void EnablePrepare();

    protected virtual void OnDisable()
    {
        if (FindDependencies())
        {
            DisablePrepare();

        }
    }

    protected virtual void OnEnable()
    {
        if (FindDependencies())
        {
            EnablePrepare();

        }

    }

    protected virtual void Awake()
    {
        if (FindDependencies())
        {
            EnablePrepare();

        }

    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();
        }

        if (inputScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
