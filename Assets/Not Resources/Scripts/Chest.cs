﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChestType
{
    Any,
    Dash,
    SprayerFire,
    Lob,
    Rifle
}

public class Chest : MonoBehaviour
{
    private GameObject sE_chestOpen_prefab;
    [SerializeField] float worldNoticeElevation;
    [SerializeField] float worldNoticeScale;
    private GameObject worldNotice_prefab;
    private GameObject worldNotice_inst;
    private Texture chestMainMat_diffTex;
    private Texture chestMainMat_diff_dashTex;
    private Texture chestMainMat_diff_rifleTex;
    private Texture chestMainMat_diff_sprayerFireTex;
    private Texture chestMainMat_diff_lobTex;

    private Texture chestType_dashTex;
    private Texture chestType_rifleTex;
    private Texture chestType_sprayerFireTex;
    private Texture chestType_lobTex;

    [SerializeField] Renderer chestRenderer;
    private int chestMainMatIndex=-1;
    private PlayerDash dashScr;
    private Collider myCol;
    [SerializeField] int coinsInside;
    [SerializeField] int startCoinID;
    private GameObject coin_prefab;

    [SerializeField] ChestType chestTypeEnum;
    public ChestType ChestTypeEnum
    {
        get
        {
            return chestTypeEnum;
        }
    }

    private void OnDestroy()
    {
        if(FindResources() && FindDependencies())
        {
            if (gameObject != null)
            {
                if (gameObject.scene.isLoaded)
                {
                    if (startCoinID >= 0 && coinsInside > 0)
                    {
                        GameObject facingInst = new GameObject();


                        if (facingInst != null)
                        {
                            facingInst.name = gameObject.name + "_facingInst";
                            facingInst.transform.forward = new Vector3(gameObject.transform.forward.x, 0, gameObject.transform.forward.z).normalized;

                            float facingInst_rotateInterval = (float)360 / (float)coinsInside;
                            for (int i = 0; i < coinsInside; i++)
                            {
                                GameObject coin_inst = Instantiate(coin_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                Collectable coinCol = coin_inst.GetComponent<Collectable>();
                                if (coinCol != null)
                                {

                                    coinCol.CollectableID = startCoinID + i;
                                    coinCol.CheckIfAlreadyObtained();

                                }
                                ArcCoin coinArc = coin_inst.GetComponent<ArcCoin>();
                                if (coinArc == null)
                                {
                                    coinArc = coin_inst.AddComponent<ArcCoin>();
                                }
                                coinArc.GravRate = 60;

                                coinArc.InitArc(25, gameObject.transform.position, facingInst.transform.forward, 81, 0.78f);
                                facingInst.transform.rotation = Quaternion.Euler(new Vector3(facingInst.transform.eulerAngles.x, facingInst.transform.eulerAngles.y + facingInst_rotateInterval, facingInst.transform.eulerAngles.z));
                            }
                            Destroy(facingInst);
                        }


                    }
                    Instantiate(sE_chestOpen_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }


                if (gameObject.transform.parent != null)
                {
                    Destroy(gameObject.transform.parent.gameObject);
                }
            }

        }
        ExtendedFunctionality.SafeDestroy(worldNotice_inst);

    }
    void SetChestTexture()
    {

        if(FindResources() && FindDependencies())
        {
            ExtendedFunctionality.SafeDestroy(worldNotice_inst);
            if (chestRenderer != null)
            {
                string detectName = "chestMainMat";
                for (int i = 0; i < chestRenderer.materials.Length; i++)
                {
                    if (chestRenderer.materials[i].name.Contains(detectName))//doing contains instead of a direct compare in case it appeends (instance) to the mat name like it loves to do
                    {
                        chestMainMatIndex = i;
                        break;
                    }
                }
                Texture destinedTex = null;

                if (chestMainMatIndex >= 0)
                {
                    if (chestRenderer.materials[chestMainMatIndex].HasProperty("_MainTex"))
                    {
                        if (chestTypeEnum != ChestType.Any)
                        {
                            worldNotice_inst = Instantiate(worldNotice_prefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y+ worldNoticeElevation, gameObject.transform.position.z), Quaternion.Euler(Vector3.zero), null);
                        }

                        Texture destinedTex_worldNotice = null;

                        switch (chestTypeEnum)
                        {
                            case ChestType.Any:
                                destinedTex = chestMainMat_diffTex;
                                break;
                            case ChestType.Dash:
                                destinedTex = chestMainMat_diff_dashTex;
                                destinedTex_worldNotice = chestType_dashTex;

                                break;
                            case ChestType.Lob:
                                destinedTex = chestMainMat_diff_lobTex;
                                destinedTex_worldNotice = chestType_lobTex;

                                break;
                            case ChestType.Rifle:
                                destinedTex = chestMainMat_diff_rifleTex;
                                destinedTex_worldNotice = chestType_rifleTex;

                                break;
                            case ChestType.SprayerFire:
                                destinedTex = chestMainMat_diff_sprayerFireTex;
                                destinedTex_worldNotice = chestType_sprayerFireTex;

                                break;
                            default:
                                destinedTex = chestMainMat_diffTex;

                                break;
                        }

                        if (worldNotice_inst != null)
                        {
                            worldNotice_inst.transform.localScale = new Vector3(worldNotice_inst.transform.localScale.x * worldNoticeScale, worldNotice_inst.transform.localScale.y * worldNoticeScale, worldNotice_inst.transform.localScale.z * worldNoticeScale);
                            LifeTimer lifeScr = worldNotice_inst.GetComponent<LifeTimer>();
                            if (lifeScr != null)
                            {
                                Destroy(lifeScr);
                            }
                            Renderer worldNotice_instRend = worldNotice_inst.GetComponent<Renderer>();
                            if (worldNotice_instRend != null)
                            {
                                Material worldNotice_instMat = worldNotice_instRend.material;
                                if (worldNotice_instMat != null)
                                {
                                    if (worldNotice_instMat.HasProperty("_MainTex"))
                                    {
                                        worldNotice_instRend.material.SetTexture("_MainTex", destinedTex_worldNotice);
                                    }
                                }
                            }
                        }



                        Texture currentTex = chestRenderer.materials[chestMainMatIndex].GetTexture("_MainTex");

                        Texture2D destinedTex2D = (Texture2D)destinedTex;
                        float rootCol_h, rootCol_s, rootCol_v;
                        Color rootCol = destinedTex2D.GetPixel(0, 0);
                        Color.RGBToHSV(rootCol, out rootCol_h, out rootCol_s, out rootCol_v);
                        rootCol_v *= 0.2f;
                        rootCol = Color.HSVToRGB(rootCol_h, rootCol_s, rootCol_v);

                        if (currentTex!= destinedTex)
                        {
                            chestRenderer.materials[chestMainMatIndex].SetTexture("_MainTex", destinedTex);

                        }

                        if (chestRenderer.materials[chestMainMatIndex].HasProperty("_Emission"))
                        {
                            chestRenderer.materials[chestMainMatIndex].SetColor("_Emission", rootCol);

                        }


                    }

                }
            }
        }
    }

    private void Awake()
    {
        if(FindDependencies() && FindResources())
        {
            SetChestTexture();
        }
    }
    private void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (chestTypeEnum == ChestType.Dash || chestTypeEnum == ChestType.Any)
            {
                myCol.isTrigger = dashScr.DashDuration_Current > 0;

            }
        }
    }
    /*
    public void BreakOpenReact()
    {

        if (gameObject.transform.parent != null)
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
        else
        {
            Destroy(gameObject);

        }

    }
    */
    private void OnTriggerStay(Collider other)
    {

        if (FindDependencies() && FindResources())
        {
            if (chestTypeEnum == ChestType.Dash|| chestTypeEnum == ChestType.Any)
            {


                PlayerDash playerCheck = other.GetComponent<PlayerDash>();
                if (playerCheck != null)
                {
                    Destroy(gameObject);


                }
            }

        }
    }

    bool FindDependencies()
    {
        bool ret = false;

        if (dashScr == null)
        {
            dashScr = GameObject.FindObjectOfType<PlayerDash>();
        }

        if (myCol == null)
        {
            myCol = gameObject.GetComponent<Collider>();
        }



        if (dashScr != null && myCol!=null)
        {
            ret= true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (sE_chestOpen_prefab == null)
        {
            sE_chestOpen_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_chestOpen");
        }
        if (worldNotice_prefab == null)
        {
            worldNotice_prefab = Resources.Load<GameObject>("Prefabs/worldNotice");

        }
        if (coin_prefab == null)
        {
            coin_prefab = Resources.Load<GameObject>("Prefabs/coin");
        }

        Texture texRet = null;
        Texture texRet_worldNotice = null;
        switch (chestTypeEnum)
        {
            case ChestType.Any:



                if (chestMainMat_diffTex == null)
                {
                    texRet = Resources.Load<Texture>("Textures/chest/chestMainMat_diff");
                    chestMainMat_diffTex = texRet;
                }
                else
                {
                    texRet = chestMainMat_diffTex;
                }



                break;
            case ChestType.Dash:
                if (chestMainMat_diff_dashTex == null)
                {
                    texRet = Resources.Load<Texture>("Textures/chest/chestMainMat_diff_dash");
                    chestMainMat_diff_dashTex = texRet;
                }
                else
                {
                    texRet = chestMainMat_diff_dashTex;
                }

                if (chestType_dashTex == null)
                {
                    texRet_worldNotice = Resources.Load<Texture>("Textures/chest/chestType_dash");
                    chestType_dashTex = texRet_worldNotice;
                }
                else
                {
                    texRet_worldNotice = chestType_dashTex;
                }
                break;
            case ChestType.Lob:
                if (chestMainMat_diff_lobTex == null)
                {
                    texRet = Resources.Load<Texture>("Textures/chest/chestMainMat_diff_lob");
                    chestMainMat_diff_lobTex = texRet;
                }
                else
                {
                    texRet = chestMainMat_diff_lobTex;
                }


                if (chestType_lobTex == null)
                {
                    texRet_worldNotice = Resources.Load<Texture>("Textures/chest/chestType_lob");
                    chestType_lobTex = texRet_worldNotice;
                }
                else
                {
                    texRet_worldNotice = chestType_lobTex;
                }

                break;
            case ChestType.Rifle:
                if (chestMainMat_diff_rifleTex == null)
                {
                    texRet = Resources.Load<Texture>("Textures/chest/chestMainMat_diff_rifle");
                    chestMainMat_diff_rifleTex = texRet;
                }
                else
                {
                    texRet = chestMainMat_diff_rifleTex;
                }


                if (chestType_rifleTex == null)
                {
                    texRet_worldNotice = Resources.Load<Texture>("Textures/chest/chestType_rifle");
                    chestType_rifleTex = texRet_worldNotice;
                }
                else
                {
                    texRet_worldNotice = chestType_rifleTex;
                }
                break;
            case ChestType.SprayerFire:
                if (chestMainMat_diff_sprayerFireTex == null)
                {
                    texRet = Resources.Load<Texture>("Textures/chest/chestMainMat_diff_sprayerFire");
                    chestMainMat_diff_sprayerFireTex = texRet;
                }
                else
                {
                    texRet = chestMainMat_diff_sprayerFireTex;
                }

                if (chestType_sprayerFireTex == null)
                {
                    texRet_worldNotice = Resources.Load<Texture>("Textures/chest/chestType_sprayerFire");
                    chestType_sprayerFireTex = texRet_worldNotice;
                }
                else
                {
                    texRet_worldNotice = chestType_sprayerFireTex;
                }
                break;
        }


        if (sE_chestOpen_prefab!=null && worldNotice_prefab != null && coin_prefab != null && texRet !=null && ((texRet_worldNotice!=null && chestTypeEnum!=ChestType.Any) || chestTypeEnum==ChestType.Any))
        {
            ret = true;
        }

        return ret;
    }


}
