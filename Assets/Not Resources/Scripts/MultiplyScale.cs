﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
enum DeleteMode
{
    WaitForAllToScale,
    DeleteOnFirstPassedScale
}


//the actions in this script are not scaled by time, but applying time.deltaTime how i think it should be applied results in totally different behaviour, so just leave it for now i guess.
public class MultiplyScale : MonoBehaviour
{
    [SerializeField] Vector3 scaleMultiples;
    [SerializeField] Vector3 scaleLimit;

    [SerializeField] Vector3 deleteOncePast;
    private Vector3 origScale;

    [SerializeField] DeleteMode delMode;
    // Start is called before the first frame update
    void Awake()
    {
        origScale = gameObject.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {



        
        float xVal = gameObject.transform.localScale.x * scaleMultiples.x;

        if((scaleMultiples.x<1 && xVal<origScale.x* scaleLimit.x && origScale.x * scaleLimit.x<origScale.x) || (scaleMultiples.x >=1  && xVal >= origScale.x * scaleLimit.x && origScale.x * scaleLimit.x >= origScale.x))
        {
            xVal = origScale.x * scaleLimit.x;
        }

        float yVal = gameObject.transform.localScale.y * scaleMultiples.y ;

        if ((scaleMultiples.y <1 && yVal < origScale.y * scaleLimit.y && origScale.y * scaleLimit.y < origScale.y) || (scaleMultiples.y >=1 && yVal >= origScale.y * scaleLimit.y && origScale.y * scaleLimit.y >= origScale.y))
        {
            yVal = origScale.y * scaleLimit.y;
        }


        float zVal = gameObject.transform.localScale.z * scaleMultiples.z ;

        if ((scaleMultiples.z < 1 && zVal < origScale.z * scaleLimit.z && origScale.z * scaleLimit.z < origScale.z) || (scaleMultiples.z >=1 && zVal >= origScale.z * scaleLimit.z && origScale.z * scaleLimit.z >= origScale.z))
        {
            zVal = origScale.z * scaleLimit.z;
        }
        

        /*
        float xVal = gameObject.transform.localScale.x * scaleMultiples.x;



        float yVal = gameObject.transform.localScale.y * scaleMultiples.y;




        float zVal = gameObject.transform.localScale.z * scaleMultiples.z;
        */


        //these are the conditions to say the x,y,or z value is WITHIN range, not out of it
        bool xCond = !((scaleMultiples.x < 1 && xVal < deleteOncePast.x * origScale.x) || (scaleMultiples.x >= 1 && xVal >= deleteOncePast.x * origScale.x));
        bool yCond = !((scaleMultiples.y < 1 && yVal < deleteOncePast.y * origScale.y) || (scaleMultiples.y >= 1 && yVal >= deleteOncePast.y * origScale.y));
        bool zCond = !((scaleMultiples.z < 1 && zVal < deleteOncePast.z * origScale.z) || (scaleMultiples.z >= 1 && zVal >= deleteOncePast.z * origScale.z));


        bool keepScalingCond = true;

        switch (delMode)
        {
            case DeleteMode.WaitForAllToScale:
                keepScalingCond = xCond || yCond || zCond;
                break;
            case DeleteMode.DeleteOnFirstPassedScale:
                keepScalingCond = xCond && yCond && zCond;
                break;
        }

        if (keepScalingCond)
        {
            gameObject.transform.localScale = new Vector3(xVal, yVal, zVal);

        }
        else
        {
            Destroy(gameObject);
        }

    }
}
