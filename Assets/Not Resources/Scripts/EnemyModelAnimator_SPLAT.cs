﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum SplatStage
{ 
    Idle,
    Beginning,
    Middle,
    End

}
public class EnemyModelAnimator_SPLAT : EnemyModelAnimator
{
    private PlayerStats statScr;
    private GameObject SPLAT_treadsModel_prefab;
    private GameObject SPLAT_treadsModel_inst;
    private GameObject SPLAT_armModel_prefab;
    private GameObject SPLAT_armModel_inst;

    private SplatStage splatStageEnum = SplatStage.Idle;
    private GameObject fistObj;
    [SerializeField] float splatRange;
    private bool lateAwake;
    [SerializeField] Vector2 splatEarlyExitTimeRange;
    private float splatEarlyExitTime_current;
    private PlayerGravity gravScr;
    protected override void Awake()
    {

        if (FindResources() && FindDependencies())
        {
            SetMdlPath_OneTimeOnly("Prefabs/Enemies/SPLAT/SPLATModel");

            base.Awake();


        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        ExtendedFunctionality.SafeDestroy(SPLAT_treadsModel_inst);
        ExtendedFunctionality.SafeDestroy(SPLAT_armModel_inst);
    }

    protected override void LateUpdate()
    {
        if(FindResources() && FindDependencies())
        {
            base.LateUpdate();
            if (Mdl_Inst != null && mdl_inst_anim!=null)
            {

                if (enScr.ChaseMode)
                {
                    if (enScr.AlertTime_Current > 0)
                    {
                        mdl_inst_anim.Play("alert");
                    }
                    else
                    {
                        mdl_inst_anim.Play("move");

                    }
                }

                if (SPLAT_treadsModel_inst == null)
                {
                    SPLAT_treadsModel_inst = Instantiate(SPLAT_treadsModel_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }
                else
                {
                    SPLAT_treadsModel_inst.transform.position = Mdl_Inst.transform.position;
                    SPLAT_treadsModel_inst.transform.forward = Vector3.Lerp(SPLAT_treadsModel_inst.transform.forward, Mdl_Inst.transform.forward, 3.0f * Time.deltaTime);
                }


                if (SPLAT_armModel_inst == null)
                {
                    SPLAT_armModel_inst = Instantiate(SPLAT_armModel_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }
                else
                {
                    if (lateAwake == false)
                    {

                        GameObject[] fistObj_proposed_all = GameObject.FindGameObjectsWithTag("SPLAT_fistObj");
                        if (fistObj_proposed_all != null)
                        {
                            for (int i = 0; i < fistObj_proposed_all.Length; i++)
                            {
                                if (fistObj_proposed_all[i] != null)
                                {
                                    if (fistObj_proposed_all[i].transform.IsChildOf(SPLAT_armModel_inst.transform))
                                    {
                                        fistObj = fistObj_proposed_all[i];
                                        break;
                                    }
                                }
                            }

                        }


                        if (fistObj != null)
                        {
                            lateAwake = true;

                        }
                    }
                    else
                    {
                        SPLAT_armModel_inst.transform.position = Mdl_Inst.transform.position;
                        SPLAT_armModel_inst.transform.right = Vector3.Lerp(SPLAT_armModel_inst.transform.right, -Mdl_Inst.transform.forward, 5.0f * Time.deltaTime);

                        Animator SPLAT_armModel_anim = SPLAT_armModel_inst.GetComponent<Animator>();

                        if (SPLAT_armModel_anim != null)
                        {
                            if (enScr.ChaseMode == false)
                            {
                                splatStageEnum = SplatStage.Idle;
                                splatEarlyExitTime_current = Random.Range(splatEarlyExitTimeRange.x, splatEarlyExitTimeRange.y);

                            }
                            else
                            {
                                if (enScr.AlertTime_Current <= 0)
                                {
                                    if (splatStageEnum == SplatStage.Idle)
                                    {
                                        splatStageEnum = SplatStage.Beginning;

                                    }

                                    if (splatStageEnum == SplatStage.Beginning)
                                    {
                                        if(splatEarlyExitTime_current > 0 && enScr.FrozenTimer_Current<=0)
                                        {
                                            splatEarlyExitTime_current -= Time.deltaTime;

                                        }
                                        float distToPlayer = Vector3.Distance(new Vector3(fistObj.transform.position.x, statScr.transform.position.y, fistObj.transform.position.z), statScr.transform.position);
                                        if ((distToPlayer <= splatRange || splatEarlyExitTime_current<=0) && SPLAT_armModel_anim.GetCurrentAnimatorStateInfo(0).IsName("splat_begin") && SPLAT_armModel_anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
                                        {
                                            splatStageEnum = SplatStage.Middle;
                                        }
                                    }
                                    else
                                    {
                                        splatEarlyExitTime_current = Random.Range(splatEarlyExitTimeRange.x, splatEarlyExitTimeRange.y);

                                    }

                                    if (splatStageEnum == SplatStage.Middle)
                                    {

                                        if(SPLAT_armModel_anim.GetCurrentAnimatorStateInfo(0).IsName("splat_middle") && SPLAT_armModel_anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
                                        {
                                            splatStageEnum = SplatStage.End;
                                        }
                                    }


                                    if (splatStageEnum == SplatStage.End)
                                    {
                                        if (SPLAT_armModel_anim.GetCurrentAnimatorStateInfo(0).IsName("splat_end") && SPLAT_armModel_anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
                                        {
                                            splatStageEnum = SplatStage.Idle;
                                        }
                                    }

                                }
                                else
                                {
                                    splatStageEnum = SplatStage.Idle;



                                }
                                SPLAT_armModel_anim.speed = mdl_inst_anim.speed;
                            }

                            HurtRegion fistHurt = fistObj.GetComponent<HurtRegion>();

                            if (fistHurt != null)
                            {
                                fistHurt.Damage = statScr.CurrentHealth;
                                if (splatStageEnum == SplatStage.Middle && enScr.FrozenTimer_Current<=0 && gravScr.Grounded)
                                {
                                    fistHurt.TriggerEnabled = true;
                                }
                                else
                                {
                                    fistHurt.TriggerEnabled = false;


                                }
                            }


                            switch (splatStageEnum)
                            {
                                case SplatStage.Idle:
                                    SPLAT_armModel_anim.Play("splat_idle");
                                    break;
                                case SplatStage.Beginning:
                                    SPLAT_armModel_anim.Play("splat_begin");
                                    break;
                                case SplatStage.Middle:
                                    SPLAT_armModel_anim.Play("splat_middle");
                                    break;

                                case SplatStage.End:
                                    SPLAT_armModel_anim.Play("splat_end");
                                    break;
                            }
                        }
                    }

                }
            }


        }
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (SPLAT_treadsModel_prefab == null)
        {
            SPLAT_treadsModel_prefab = Resources.Load<GameObject>("Prefabs/Enemies/SPLAT/SPLAT_treadsModel");
        }

        if (SPLAT_armModel_prefab == null)
        {
            SPLAT_armModel_prefab = Resources.Load<GameObject>("Prefabs/Enemies/SPLAT/SPLAT_armModel");
        }


        if ( SPLAT_armModel_prefab != null && SPLAT_treadsModel_prefab != null && parRet)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet= base.FindDependencies();


        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }

        if (gravScr == null)
        {
            gravScr = GameObject.FindObjectOfType<PlayerGravity>();
        }

        if (gravScr!=null && statScr != null && parRet)
        {
            ret = true;
        }
        return ret;
    }
}
