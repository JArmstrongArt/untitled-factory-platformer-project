﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModelAnimator_ChikChik : EnemyModelAnimator
{
    private GameObject chikChikHandsModel_prefab;
    private GameObject chikChikHandsModel_inst;
    private Animator chikChikHandsAnim;
    private PlayerStats statScr;
    private ChikChik chikScr;
    private GameObject sE_chikChikShieldHit_prefab;
    private GameObject sE_chikChikShieldBurn_prefab;
    private GameObject sE_chikChikShieldBreak_prefab;
    public bool BlockAnimDue { set; private get; }
    public bool BurnAnimDue { set; private get; }

    private GameObject pS_flames_shield_prefab;
    private GameObject pS_flames_shield_inst;

    [SerializeField] float minSoundWaitTime;
    private float minSoundWaitTime_current;

    [SerializeField] float afterburnTime;
    private float afterburnTime_current;

    private List<RendAndOrigMatPair> origHandsMats = new List<RendAndOrigMatPair>();



    private bool dmgMatEnabled;
    [SerializeField] float dmgFlashTime;
    private float dmgFlashTime_current;

    public bool FireAnimDue { private get; set; }

    private GameObject sE_chikChikShoot_prefab;


    public override void HurtEffectTrigger(DamageType dmgEnum, EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {
        if(FindResources() && FindDependencies())
        {
            if (chikScr.ShieldStateEnum != ShieldState.Raised)
            {
                
                base.HurtEffectTrigger(dmgEnum, cOD);
            }

        }

    }

    protected override void Awake()
    {

        if (FindResources() && FindDependencies())
        {
            SetMdlPath_OneTimeOnly("Prefabs/Enemies/chikChikModel");

            base.Awake();

            

        }
    }


    void DestroyHandsShit()
    {
        ExtendedFunctionality.SafeDestroy(chikChikHandsModel_inst);
        ExtendedFunctionality.SafeDestroy(pS_flames_shield_inst);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        DestroyHandsShit();

    }

    void SetHandsDamagedMat(bool state)
    {
        if(FindResources() && FindDependencies())
        {
            if (origHandsMats != null && chikChikHandsModel_inst != null)
            {
                if (state == false)
                {
                    foreach (RendAndOrigMatPair pair in origHandsMats)
                    {

                        pair.rend.materials = pair.origMats.ToArray();
                    }
                }
                else
                {
                    foreach (RendAndOrigMatPair pair in origHandsMats)
                    {
                        Material[] rendMatCopy = pair.rend.materials;

                        pair.rend.materials = new Material[pair.origMats.Count];
                        for (int i = 0; i < rendMatCopy.Length; i++)
                        {
                            rendMatCopy[i] = enemyDmgMat;
                        }
                        pair.rend.materials = rendMatCopy;
                    }
                }
            }
        }


    }

    protected override void LateUpdate()
    {
        if(FindResources() && FindDependencies())
        {
            base.LateUpdate();

            if (mdl_inst_anim != null)
            {
                if (enScr.ChaseMode == true && enScr.AlertTime_Current <= 0)
                {
                    if (FireAnimDue == true)
                    {
                        Instantiate(sE_chikChikShoot_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        mdl_inst_anim.Play("fire",-1,0);
                        FireAnimDue = false;

                    }
                    if(!mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("fire")||(mdl_inst_anim.GetCurrentAnimatorStateInfo(0).IsName("fire") && mdl_inst_anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1))
                    {
                        mdl_inst_anim.Play("move");

                    }


                }


                if (dmgFlashTime_current > 0)
                {
                    dmgFlashTime_current -= Time.deltaTime;
                    SetHandsDamagedMat(dmgMatEnabled);
                    dmgMatEnabled = !dmgMatEnabled;

                }
                else
                {
                    SetHandsDamagedMat(false);
                }
                if (minSoundWaitTime_current > 0)
                {
                    minSoundWaitTime_current -= Time.deltaTime;

                }
                if (chikScr.ShieldStateEnum == ShieldState.Raised)
                {
                    if (chikChikHandsModel_inst == null)
                    {
                        chikChikHandsModel_inst = Instantiate(chikChikHandsModel_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        origHandsMats = new List<RendAndOrigMatPair>();
                    }
                    else
                    {
                        if (origHandsMats == null || origHandsMats.Count <= 0)
                        {
                            origHandsMats = ExtendedFunctionality.GeneratePairingsFromCurrentRends(chikChikHandsModel_inst);

                        }

                        chikChikHandsAnim = chikChikHandsModel_inst.GetComponent<Animator>();
                        if (chikChikHandsAnim != null)
                        {
                            chikChikHandsModel_inst.transform.position = gameObject.transform.position;
                            chikChikHandsModel_inst.transform.forward = (statScr.transform.position - gameObject.transform.position).normalized;
                            if (BlockAnimDue)
                            {
                                if (minSoundWaitTime_current <= 0)
                                {
                                    Instantiate(sE_chikChikShieldHit_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                    minSoundWaitTime_current = minSoundWaitTime;
                                    chikChikHandsAnim.Play("block", -1, 0.0f);
                                }


                            }

                            if (BurnAnimDue)
                            {
                                if (chikScr.ShieldHealth <= 0)
                                {
                                    Instantiate(sE_chikChikShieldBreak_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                }
                                if (pS_flames_shield_inst == null)
                                {
                                    pS_flames_shield_inst = Instantiate(pS_flames_shield_prefab, gameObject.transform.position, Quaternion.Euler(-90, 0, 0), null);
                                }

                                if (minSoundWaitTime_current <= 0)
                                {
                                    Instantiate(sE_chikChikShieldBurn_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                                    minSoundWaitTime_current = minSoundWaitTime;

                                }
                                dmgFlashTime_current = dmgFlashTime;

                                afterburnTime_current = afterburnTime;

                            }


                            if (pS_flames_shield_inst != null)
                            {
                                pS_flames_shield_inst.transform.position = chikChikHandsModel_inst.transform.position;

                                ParticleSystem pS_flames = pS_flames_shield_inst.GetComponent<ParticleSystem>();
                                if (pS_flames != null)
                                {
                                    if (afterburnTime_current > 0)
                                    {
                                        if (!pS_flames.isPlaying)
                                        {
                                            pS_flames.Play();
                                        }
                                        afterburnTime_current -= Time.deltaTime;
                                    }
                                    else
                                    {
                                        if (pS_flames.isPlaying)
                                        {
                                            pS_flames.Stop();
                                        }
                                    }
                                }

                            }

                            BurnAnimDue = false;
                            BlockAnimDue = false;
                        }


                    }
                }
                else
                {
                    DestroyHandsShit();

                }
            }

            

        }
    }

    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet= base.FindDependencies();

        if (statScr==null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }

        if (chikScr == null)
        {
            if (enScr != null)
            {
                chikScr = enScr.GetComponent<ChikChik>();
            }
            else
            {
                chikScr = gameObject.GetComponent<ChikChik>();
            }
        }

        if(chikScr!=null && statScr != null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (chikChikHandsModel_prefab == null)
        {
            chikChikHandsModel_prefab = Resources.Load<GameObject>("Prefabs/Enemies/chikChikHandsModel");
        }

        if (sE_chikChikShieldHit_prefab == null)
        {
            sE_chikChikShieldHit_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_chikChikShieldHit");
        }

        if (sE_chikChikShieldBurn_prefab == null)
        {
            sE_chikChikShieldBurn_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_chikChikShieldBurn");
        }

        if (pS_flames_shield_prefab == null)
        {
            pS_flames_shield_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_flames");
        }

        if (sE_chikChikShieldBreak_prefab == null)
        {
            sE_chikChikShieldBreak_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_chikChikShieldBreak");
        }
        if (sE_chikChikShoot_prefab == null)
        {
            sE_chikChikShoot_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_chikChikShoot");
        }

        if (enemyDmgMat == null)
        {
            enemyDmgMat = Resources.Load<Material>("Materials/enemyDmgMat");
        }

        if (sE_chikChikShoot_prefab!=null && enemyDmgMat != null && sE_chikChikShieldBreak_prefab != null && sE_chikChikShieldBurn_prefab != null && pS_flames_shield_prefab != null && sE_chikChikShieldHit_prefab != null && chikChikHandsModel_prefab != null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }
}
