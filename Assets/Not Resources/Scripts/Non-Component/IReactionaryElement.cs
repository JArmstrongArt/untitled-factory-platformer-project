﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//slap this bad boy of an interface on any script you want to have an element which turns 'ConditionFulfilled' to true and can be compared in a Reaction script to make an effect happen as a result of one or more ConditionFulfilled-s.



public interface IReactionaryElement 
{
    bool ConditionFulfilled { get; set; }
}
