﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    private GameObject dropShadow_prefab;
    private GameObject dropShadow_inst;
    [SerializeField] bool dropShadow_enabled;

    [SerializeField] float dropShadow_radius;

    protected virtual void Awake()
    {

        if (FindResources())
        {
            if (dropShadow_enabled)
            {
                dropShadow_inst = Instantiate(dropShadow_prefab, gameObject.transform.position, Quaternion.Euler(90, 0, 0), null);

                MaintainOffset dropShadow_offset = dropShadow_inst.GetComponent<MaintainOffset>();
                if (dropShadow_offset == null)
                {
                    dropShadow_offset = dropShadow_inst.AddComponent<MaintainOffset>();
                }
                dropShadow_offset.OffsetUpdate = UpdateType.Late;
                dropShadow_offset.OffsetObj = gameObject;
                dropShadow_offset.Init();

                Projector dropShadow_proj = dropShadow_inst.GetComponent<Projector>();

                if (dropShadow_proj != null)
                {
                    dropShadow_proj.orthographic = true;
                    dropShadow_proj.orthographicSize = dropShadow_radius;
                    
                }
            }

        }
    }

    protected virtual void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(dropShadow_inst);

    }

    protected virtual bool FindResources()
    {
        bool ret = false;

        if (dropShadow_prefab == null)
        {
            dropShadow_prefab = Resources.Load<GameObject>("Prefabs/Entities/dropShadow");
        }

        if (dropShadow_prefab != null)
        {
            ret = true;
        }
        return ret;
    }
}
