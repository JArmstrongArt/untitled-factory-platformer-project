﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcExplosive : ArcObjectWithCollision
{
    private GameObject explosion_prefab;
    protected GameObject explosion_inst;
    protected Explosion explodeScr;

    private GameObject pS_fuse_prefab;
    private GameObject pS_fuse_inst;
    private ParticleSystem pS_fuse;
    private GameObject sE_arcExplosiveFuse_prefab;
    private GameObject sE_arcExplosiveFuse_inst;

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (pS_fuse_inst != null)
        {
            LifeTimer timer = pS_fuse_inst.GetComponent<LifeTimer>();
            if (timer == null)
            {
                timer = pS_fuse_inst.AddComponent<LifeTimer>();

            }
            timer.LifeTime = 3;
        }
        ExtendedFunctionality.SafeDestroy(sE_arcExplosiveFuse_inst);

    }
    protected override void CollideReact()
    {
        if (FindResources())
        {
            ExplodeAndDelete();
        }
    }
    protected override void FixedUpdate()
    {
        if (FindResources())
        {
            base.FixedUpdate();
            if (pS_fuse_inst == null)
            {
                pS_fuse_inst = Instantiate(pS_fuse_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                pS_fuse = pS_fuse_inst.GetComponent<ParticleSystem>();

            }
            else
            {
                pS_fuse_inst.transform.position = gameObject.transform.position;
                if (pS_fuse != null)
                {
                    if (totalArcTime_remaining > 0)
                    {
                        if (sE_arcExplosiveFuse_inst == null)
                        {
                            sE_arcExplosiveFuse_inst = Instantiate(sE_arcExplosiveFuse_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        }
                        else
                        {
                            sE_arcExplosiveFuse_inst.transform.position = gameObject.transform.position;
                        }
                        if (pS_fuse.isPlaying == false)
                        {
                            pS_fuse.Play();
                        }
                    }
                    else
                    {
                        ExtendedFunctionality.SafeDestroy(sE_arcExplosiveFuse_inst);

                        if (pS_fuse.isPlaying == true)
                        {
                            pS_fuse.Stop();
                        }
                    }
                }
                else
                {
                    ExtendedFunctionality.SafeDestroy(sE_arcExplosiveFuse_inst);

                    Destroy(pS_fuse_inst);
                }
            }




        }
    }

    protected override void EndArcReact()
    {
        if (FindResources())
        {
            base.EndArcReact();
            ExplodeAndDelete();

        }
    }
    protected virtual void ExplodeAndDelete()
    {
        if (FindResources())
        {
            explosion_inst = Instantiate(explosion_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            explodeScr = explosion_inst.GetComponent<Explosion>();
            if (explodeScr != null)
            {
                HurtEnemyInTrigger hrtScr = explosion_inst.GetComponent<HurtEnemyInTrigger>();
                if (hrtScr != null)
                {
                    hrtScr.COD = EnemyCauseOfDamage.Lob;

                }
            }
            else
            {
                Destroy(explosion_inst);
            }

            Destroy(gameObject);
        }

    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();

        if (explosion_prefab == null)
        {
            explosion_prefab = Resources.Load<GameObject>("Prefabs/Player/explosion");
        }

        if (pS_fuse_prefab == null)
        {
            pS_fuse_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_fuse");
        }

        if (sE_arcExplosiveFuse_prefab == null)
        {
            sE_arcExplosiveFuse_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_arcExplosiveFuse");
        }
        if (sE_arcExplosiveFuse_prefab!=null && pS_fuse_prefab != null && explosion_prefab != null && parRet)
        {
            ret = true;
        }

        return ret;
    }
}
