﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardRender : MonoBehaviour
{
    public GameObject CamInst { private get; set; }
    [SerializeField] bool tiltAllowed;

    private Mesh billboardMesh;
    private MeshFilter filt;

    private void LateUpdate()
    {
        if (FindResources() && FindDependencies())
        {
            if (filt.mesh != billboardMesh)
            {
                filt.mesh = billboardMesh;

            }
            if (CamInst != null)
            {
                if (tiltAllowed == true)
                {
                    gameObject.transform.LookAt(CamInst.transform.position);

                }
                else
                {
                    gameObject.transform.LookAt(new Vector3(CamInst.transform.position.x, gameObject.transform.position.y, CamInst.transform.position.z));

                }
            }
            else
            {
                CamInst = GameObject.FindGameObjectWithTag("MainCamera");
            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;
        if (filt == null)
        {
            filt = gameObject.GetComponent<MeshFilter>();
        }

        if (filt != null)
        {
            ret = true;
        }

        return ret;
    }
    bool FindResources()
    {
        bool ret = false;

        if (billboardMesh == null)
        {
            GameObject billboardObj = Resources.Load<GameObject>("Models/billboard/billboard");

            if (billboardObj != null)
            {
                MeshFilter billboardFilt = billboardObj.GetComponent<MeshFilter>();

                if (billboardFilt != null)
                {
                    billboardMesh = billboardFilt.sharedMesh;
                }
            }
        }

        if (billboardMesh != null)
        {
            ret = true;
        }

        return ret;
    }
}
