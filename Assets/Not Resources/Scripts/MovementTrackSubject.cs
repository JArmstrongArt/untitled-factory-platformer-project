﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
enum TrackLoopMode
{
    NoLoop,
    Loop
}
[DefaultExecutionOrder(-100)]//this is so that, when used as a moving platform. it is calculated before other scripts that use movement
public class MovementTrackSubject : MonoBehaviour
{
    [SerializeField] UpdateType moveUpdateEnum;

    [SerializeField] MovementTrack myTrack;
    [SerializeField] float moveSpeed;
    private float moveSpeed_current;
    [SerializeField] TrackLoopMode loopEnum;

    [SerializeField] int trackIndex_start;
    private int trackIndex_current;
    private float moveProgress;

    public Vector3 DiffPos { get; private set; }
    private Vector3 prevPos;

    private void Awake()
    {
        Init();
    }

    private void OnEnable()
    {
        Init();
    }

    void NullifiedState()
    {


        trackIndex_current = -1;
        moveSpeed_current = 0;
        moveProgress = 0;
    }
    private void Init()
    {
        if (myTrack != null)
        {

            trackIndex_current = Mathf.Clamp(trackIndex_start, 0, myTrack.AllTrackCorners.Count - 1);
            moveSpeed_current = moveSpeed;
            moveProgress = 0;
        }
        else
        {
            NullifiedState();
        }

    }

    void mainFunc(float timeUnit)
    {
        if (myTrack != null)
        {
            if (trackIndex_current >= 0 && trackIndex_current < myTrack.AllTrackCorners.Count)
            {
                Vector3 positionA = Vector3.zero;
                Vector3 positionB = Vector3.zero;
                if (trackIndex_current + 1 < myTrack.AllTrackCorners.Count)
                {
                    positionA = myTrack.AllTrackCorners[trackIndex_current].position;
                    positionB = myTrack.AllTrackCorners[trackIndex_current + 1].position;

                }
                else
                {
                    positionA = myTrack.AllTrackCorners[trackIndex_current].position;
                    positionB = myTrack.AllTrackCorners[0].position;
                }
                gameObject.transform.position = Vector3.Lerp(positionA, positionB, moveProgress);

                moveProgress += moveSpeed_current * timeUnit;
                moveProgress = Mathf.Clamp(moveProgress, 0, float.MaxValue);
                if (moveProgress >= 1)
                {

                    switch (loopEnum)
                    {
                        case TrackLoopMode.Loop:
                            if (trackIndex_current + 1 <= myTrack.AllTrackCorners.Count - 1)
                            {

                                trackIndex_current += 1;

                            }
                            else
                            {

                                trackIndex_current = 0;
                            }

                            moveProgress = 0;

                            break;

                        case TrackLoopMode.NoLoop:
                            if (trackIndex_current + 1 <= myTrack.AllTrackCorners.Count - 2)
                            {

                                trackIndex_current += 1;
                                moveProgress = 0;

                            }
                            else
                            {
                                moveProgress = 1;
                            }

                            break;
                    }


                }
            }
        }
        else
        {
            NullifiedState();

        }

        DiffPos = gameObject.transform.position - prevPos;
        prevPos = gameObject.transform.position;
    }

    private void FixedUpdate()
    {

        if (moveUpdateEnum == UpdateType.Fixed)
        {
            mainFunc(Time.fixedDeltaTime);
        }
    }

    private void Update()
    {

        if (moveUpdateEnum == UpdateType.Regular)
        {
            mainFunc(Time.deltaTime);
        }
    }

    private void LateUpdate()
    {

        if (moveUpdateEnum == UpdateType.Late)
        {
            mainFunc(Time.deltaTime);
        }
    }
}
