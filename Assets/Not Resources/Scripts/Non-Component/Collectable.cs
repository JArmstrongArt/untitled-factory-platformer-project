﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[DefaultExecutionOrder(-100)]
public abstract class Collectable : Entity
{
    protected bool secretlyDisabled = false;
    public bool SecretlyDisabled
    {
        set
        {
            secretlyDisabled = value;
        }
    }

    private bool endInteraction=false;

    protected int collectableID;
    public int CollectableID
    {
        set
        {
            collectableID = value;
        }

    }


    protected abstract void CollectionReact(PlayerDash dashScr =null);
    protected PlayerStats statScr;
    protected override void Awake()
    {
        if (FindResources())
        {
            
            base.Awake();
            collectableID = gameObject.transform.GetSiblingIndex();

            CheckIfAlreadyObtained();
        }

    }

    public bool CheckIfAlreadyObtained()
    {
        bool ret = false;
        statScr = GameObject.FindObjectOfType<PlayerStats>();

        if (statScr != null)
        {
            if (statScr.allCollectedIDs.Contains(collectableID))
            {
                ret = true;

            }
        }

        List<Renderer> colRenderers = ExtendedFunctionality.GetAllRenderersInChildren(gameObject);

        foreach(Renderer rend in colRenderers)
        {
            foreach(Material mat in rend.materials)
            {
                
                if (mat.HasProperty("_Color"))
                {
                    Color origCol = mat.GetColor("_Color");

                    if (ret == true)
                    {
                        mat.SetColor("_Color", new Color(origCol.r, origCol.g, origCol.b, 0.25f));

                    }
                    else
                    {
                        mat.SetColor("_Color", new Color(origCol.r, origCol.g, origCol.b, 1.0f));

                    }
                }
            }
        }


        return ret;
    }

    GameObject FindNearestCollectable(float radius = -1)
    {
        GameObject ret = null;

        Collectable[] allCollectables = GameObject.FindObjectsOfType<Collectable>();
        List<float> distList = new List<float>();
        foreach(Collectable col in allCollectables)
        {
            if (col != this)
            {
                if (col.endInteraction == false)
                {
                    if (radius < 0)
                    {
                        distList.Add(Vector3.Distance(gameObject.transform.position, col.gameObject.transform.position));

                    }
                    else
                    {
                        if (Vector3.Distance(gameObject.transform.position, col.gameObject.transform.position) <= radius)
                        {
                            distList.Add(Vector3.Distance(gameObject.transform.position, col.gameObject.transform.position));

                        }

                    }
                }

            }
        }

        if (distList.Count > 0)
        {
            distList.Sort();

            foreach(Collectable col in allCollectables)
            {
                if (Vector3.Distance(gameObject.transform.position, col.gameObject.transform.position) == distList[0])
                {
                    ret = col.gameObject;
                    break;
                }
            }
        }

        return ret;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (FindResources())
        {
            PlayerStats statFound = other.GetComponent<PlayerStats>();
            if (statFound != null && statFound == statScr&&secretlyDisabled==false)
            {
                endInteraction = true;
                if (!statScr.allCollectedIDs.Contains(collectableID))
                {
                    statScr.allCollectedIDs.Add(collectableID);
                }



                PlayerDash dashScr = other.GetComponent<PlayerDash>();
                if (dashScr != null)
                {
                    if (dashScr.DashDuration_Current > 0)
                    {
                        dashScr.NearestCollectable = FindNearestCollectable(3.5f);
                        dashScr.CheckForMoreCollectables();
                    }
                }

                CollectionReact(dashScr);

            }
        }

    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();


        if (parRet == true)
        {
            ret = true;
        }

        return ret;
    }




    //unusued for now, i had a custom gui button to activate it but it turns out custom inspectors in unity are broken and shit : )
    public void FindID()
    {
        Collectable[] allCol = GameObject.FindObjectsOfType<Collectable>();
        int finalID = 0;
        bool validID = false;
        bool earlyBreak=false;
        while (validID == false)
        {
            earlyBreak = false;
            foreach (Collectable col in allCol)
            {
                if (col != this)
                {
                    if (col.collectableID == finalID)
                    {
                        finalID += 1;
                        earlyBreak = true;
                        break;
                    }
                }

                
            }
            if (earlyBreak == false)
            {
                validID = true;
            }
        }

        collectableID = finalID;

    }
}
