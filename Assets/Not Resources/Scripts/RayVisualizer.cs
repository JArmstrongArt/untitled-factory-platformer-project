﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayVisualizer : MonoBehaviour
{
    [SerializeField] Vector3 rayDir;
    [SerializeField] float rayLen;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(gameObject.transform.position, gameObject.transform.position + (rayDir.normalized * rayLen));
    }
}
