﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-200)]
public class PlayerModelAnimator : ModelAnimator
{

    //private GameObject playerModel_prefab;
    //public GameObject PlayerModel_Inst { get; private set; }
    //private float playerModelHeightOffset = 1.0f;

    //private Animator playerModel_inst_anim;

    //private Renderer rend;

    private PlayerMovement moveScr;
    private PlayerGravity gravScr;

    
    private bool jumpAirtime;

    private PlayerHurt hurtScr;

    //public List<Renderer> allPlayerModelMeshRenderers = new List<Renderer>();


    private PlayerDash dashScr;
    private PlayerRifle rifleScr;
    private PlayerSprayerIce sprayerIceScr;
    private PlayerSprayerFire sprayerFireScr;
    private PlayerLob lobScr;

    private PlayerStats statScr;

    private GameObject pS_playerDie_prefab;
    private bool pS_playerDie_played = false;
    protected override void Awake()
    {
        mdlPath = "Prefabs/Player/PlayerModel_prefab";//this HAS to be outside of the usual findResources/findDependencies nest, as findResources will return false without a valid mdlPath for the class ModelAnimator
        if (FindDependencies() && FindResources())
        {
            base.Awake();

        }
    }

    public void SetModelAlpha(float alpha)
    {
        if (FindDependencies() && FindResources())
        {
            foreach (Renderer rend in mdl_allRenderers)
            {

                if (rend != null)
                {
                    foreach (Material mat in rend.materials)
                    {
                        if (!mat.name.Contains("hideMat"))
                        {
                            if (mat.HasProperty("_Color"))
                            {
                                Color origCol = mat.GetColor("_Color");

                                mat.SetColor("_Color", new Color(origCol.r, origCol.g, origCol.b, alpha));
                            }
                        }

                    }
                }
            }
        }


    }


    public void PlayLob()
    {
        if (FindDependencies() && FindResources())
        {
            if (mdl_inst_anim != null)
            {
                if (lobScr.FirstLobbed == true)
                {
                    mdl_inst_anim.Play("lob_attack",-1,0.0f);

                }
            }
        }
    }


    // Update is called once per frame
    protected override void Update()
    {
        if ( FindDependencies() && FindResources())
        {
            base.Update();

            if (mdl_inst_anim != null)
            {
                if (statScr.Dead)
                {
                    SetModelAlpha(0.0f);
                    if (pS_playerDie_played == false)
                    {
                        Instantiate(pS_playerDie_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                        pS_playerDie_played = true;
                    }
                }
                else
                {
                    pS_playerDie_played = false;
                    //i removed the run speed ralistic variation because it makes the animation less satisfying
                    //playerModel_inst_anim.speed = 1;


                    mdl_inst_anim.Play("sprayer");
                    switch (rifleScr.rifleStateEnum)
                    {
                        case ModelAnimator_RifleState.Idle:
                            mdl_inst_anim.Play("rifle_idle");
                            break;
                        case ModelAnimator_RifleState.ShotFired:
                            mdl_inst_anim.Play("rifle_fire", -1, 0.0f);
                            rifleScr.rifleStateEnum = ModelAnimator_RifleState.ShotFiredAfter;
                            break;
                    }

                    if (lobScr.FirstLobbed == false)
                    {
                        mdl_inst_anim.Play("lob_idle");

                    }


                    if (hurtScr.StunType == StunnedType.NotStunned)
                    {
                        if (rifleScr.enabled == true)
                        {
                            mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("rifle"), 1);

                        }
                        else
                        {
                            mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("rifle"), 0);

                        }

                        if (sprayerFireScr.enabled == true || sprayerIceScr.enabled == true)
                        {
                            mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("sprayer"), 1);

                        }
                        else
                        {
                            mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("sprayer"), 0);

                        }

                        if (lobScr.enabled == true)
                        {
                            mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("lob"), 1);

                        }
                        else
                        {
                            mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("lob"), 0);

                        }

                        if (dashScr.DashDecideDuration_Current > 0 || dashScr.DashDuration_Current > 0)
                        {
                            mdl_inst_anim.Play("dash");

                        }
                        else
                        {
                            if (gravScr.Grounded == true)
                            {
                                if (moveScr.MoveCalc.magnitude >= 0.01)
                                {
                                    mdl_inst_anim.Play("run");
                                    //playerModel_inst_anim.speed = moveScr.MoveCalc.magnitude / 0.265f;
                                }
                                else
                                {
                                    mdl_inst_anim.Play("stand");

                                }
                                jumpAirtime = false;
                            }
                            else
                            {
                                if (gravScr.gravityCurrent > 0)
                                {
                                    jumpAirtime = true;
                                    mdl_inst_anim.Play("jump");

                                }
                                else
                                {
                                    if (jumpAirtime == true)
                                    {
                                        mdl_inst_anim.Play("jump_fall");

                                    }
                                    else
                                    {
                                        mdl_inst_anim.Play("fall");

                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("rifle"), 0);
                        mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("sprayer"), 0);
                        mdl_inst_anim.SetLayerWeight(mdl_inst_anim.GetLayerIndex("lob"), 0);

                        if (hurtScr.StunType == StunnedType.StunnedAndInvincible)
                        {
                            mdl_inst_anim.Play("hurt");

                        }
                        else
                        {
                            mdl_inst_anim.Play("shock");

                        }

                    }
                }







            }

        }
    }



    bool FindResources()
    {
        bool ret = false;

        if (pS_playerDie_prefab == null)
        {
            pS_playerDie_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_playerDie");
        }

        if (pS_playerDie_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;


        if (moveScr == null)
        {
            moveScr = gameObject.GetComponent<PlayerMovement>();
        }

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<PlayerGravity>();
        }

        if (dashScr == null)
        {
            dashScr = gameObject.GetComponent<PlayerDash>();
        }

        if (rifleScr == null)
        {
            rifleScr = gameObject.GetComponent<PlayerRifle>();
        }

        if (sprayerFireScr== null)
        {
            sprayerFireScr = gameObject.GetComponent<PlayerSprayerFire>();
        }

        if (sprayerIceScr == null)
        {
            sprayerIceScr = gameObject.GetComponent<PlayerSprayerIce>();
        }

        if (hurtScr == null)
        {
            hurtScr = gameObject.GetComponent<PlayerHurt>();
        }

        if (lobScr == null)
        {
            lobScr = gameObject.GetComponent<PlayerLob>();
        }

        if (statScr == null)
        {
            statScr = gameObject.GetComponent<PlayerStats>();
        }

        if (statScr != null && lobScr != null && moveScr != null && gravScr!=null && dashScr!=null && hurtScr!=null && rifleScr!=null && sprayerIceScr!=null && sprayerFireScr!=null)
        {
            ret = true;
        }

        return ret;
    }
}
