%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: protonPopInMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: body
    m_Weight: 0
  - m_Path: face
    m_Weight: 0
  - m_Path: Proton
    m_Weight: 0
  - m_Path: Proton/motion
    m_Weight: 1
  - m_Path: Proton/motion/main
    m_Weight: 0
  - m_Path: Proton/motion/main/main_end
    m_Weight: 0
  - m_Path: Proton/motion/ring1
    m_Weight: 0
  - m_Path: Proton/motion/ring1/ring1_ball 1
    m_Weight: 0
  - m_Path: Proton/motion/ring1/ring1_ball 1/ring1_ball_x
    m_Weight: 0
  - m_Path: Proton/motion/ring1/ring1_ball 1/ring1_ball_x/ring1_ball_x_end
    m_Weight: 0
  - m_Path: Proton/motion/ring1/ring1_ball 1/ring1_ball_y
    m_Weight: 0
  - m_Path: Proton/motion/ring1/ring1_ball 1/ring1_ball_y/ring1_ball_y_end
    m_Weight: 0
  - m_Path: Proton/motion/ring2
    m_Weight: 0
  - m_Path: Proton/motion/ring2/ring2_ball 1
    m_Weight: 0
  - m_Path: Proton/motion/ring2/ring2_ball 1/ring2_ball_x
    m_Weight: 0
  - m_Path: Proton/motion/ring2/ring2_ball 1/ring2_ball_x/ring2_ball_x_end
    m_Weight: 0
  - m_Path: Proton/motion/ring2/ring2_ball 1/ring2_ball_y
    m_Weight: 0
  - m_Path: Proton/motion/ring2/ring2_ball 1/ring2_ball_y/ring2_ball_y_end
    m_Weight: 0
  - m_Path: ring1_ball
    m_Weight: 0
  - m_Path: ring1_sideA
    m_Weight: 0
  - m_Path: ring1_sideB
    m_Weight: 0
  - m_Path: ring2_ball
    m_Weight: 0
  - m_Path: ring2_sideA
    m_Weight: 0
  - m_Path: ring2_sideB
    m_Weight: 0
