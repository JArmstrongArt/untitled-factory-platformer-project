﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamWallShaderSwitcher : MonoBehaviour
{
    private Renderer camStencilSphereRend;
    [SerializeField] GenericRay camObstructionRay;
    private PlayerStats statScr;




    private Shader stencilShader;

    private Shader origShader;

    private GameObject prevObj;

    private bool newObject;

    private Vector3 stencilSphereOrigScale;

    private void Awake()
    {
        if(FindDependencies() && FindResources())
        {
            stencilSphereOrigScale = camStencilSphereRend.transform.localScale;
        }
    }
    private void Update()
    {
        if (FindDependencies() && FindResources())
        {
            if (camObstructionRay.ray_surface == prevObj)
            {
                newObject = false;
            }
            else
            {
                newObject = true;
            }
            
            if (newObject)
            {
                if (prevObj != null)
                {
                    if (prevObj.GetComponent<Renderer>() != null)
                    {
                        if (prevObj.GetComponent<Renderer>().material != null)
                        {
                            prevObj.GetComponent<Renderer>().material.shader = origShader;

                        }
                    }
                }

                if (camObstructionRay.ray_surface != null)
                {
                    if (camObstructionRay.ray_surface.GetComponent<Renderer>() != null)
                    {
                        if (camObstructionRay.ray_surface.GetComponent<Renderer>().material != null)
                        {
                            origShader = camObstructionRay.ray_surface.GetComponent<Renderer>().material.shader;
                            camObstructionRay.ray_surface.GetComponent<Renderer>().material.shader = stencilShader;
                        }
                        else
                        {
                            origShader = null;
                        }
                    }
                    else
                    {
                        origShader = null;

                    }

                }


            }


            Vector3 meToPlayer = statScr.transform.position - gameObject.transform.position;
            camObstructionRay.customDir_vec = meToPlayer.normalized;
            camObstructionRay.rayLength = meToPlayer.magnitude;

            bool obstructed = camObstructionRay.ray_surface != null;

            if (obstructed)
            {
                camStencilSphereRend.transform.localScale = Vector3.Lerp(camStencilSphereRend.transform.localScale,stencilSphereOrigScale, 8 * Time.deltaTime);
                camStencilSphereRend.enabled = true;

            }
            else
            {
                camStencilSphereRend.enabled = false;
                camStencilSphereRend.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

            }

            prevObj = camObstructionRay.ray_surface;
        }

    }

    bool FindDependencies()
    {
        bool ret = false;




        if (statScr == null)
        {
            statScr = GameObject.FindObjectOfType<PlayerStats>();
        }



        if (camStencilSphereRend == null)
        {
            GameObject camStencilSphereObj = GameObject.FindGameObjectWithTag("camStencilSphere");
            if (camStencilSphereObj != null)
            {
                camStencilSphereRend = camStencilSphereObj.GetComponent<Renderer>();
            }
        }

        if (camStencilSphereRend != null && statScr != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindResources()
    {
        bool ret = false;

        if (stencilShader == null)
        {
            stencilShader = Resources.Load<Shader>("Shaders/CamStencilWall");
        }

        if (stencilShader != null)
        {
            ret = true;
        }

        return ret;
    }
}
