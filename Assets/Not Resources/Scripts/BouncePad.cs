﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncePad : MonoBehaviour
{
    private GenericRay bouncePadRay;
    private PlayerGravity gravScr;
    [SerializeField] float bouncePower;
    public float BouncePower
    {
        get
        {
            return bouncePower;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (FindDependencies())
        {
            if (bouncePadRay.ray_surface == gameObject)
            {
                if (gravScr.gravityCurrent < 0)
                {
                    gravScr.UpwardForceQueued = true;
                    gravScr.UFT = UpwardForceType.BouncePad;
                    gravScr.BouncePadSrc = this;
                }


            }
        }
    }

    bool FindDependencies()
    {
        bool ret = false;
        if (bouncePadRay == null)
        {
            //player_bouncePadRay
            GameObject bouncePadRayObj = GameObject.FindGameObjectWithTag("player_bouncePadRay");
            if (bouncePadRayObj != null)
            {
                bouncePadRay = bouncePadRayObj.GetComponent<GenericRay>();
            }
        }

        if (gravScr == null)
        {
            gravScr = GameObject.FindObjectOfType<PlayerGravity>();
        }

        if (bouncePadRay != null && gravScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
