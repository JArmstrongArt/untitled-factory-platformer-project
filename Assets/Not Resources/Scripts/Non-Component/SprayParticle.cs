﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class SprayParticle : MonoBehaviour
{

    public abstract void OnParticleCollision(GameObject other);
}
