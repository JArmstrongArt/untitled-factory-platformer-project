﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-200)]
public class PlayerCameraAndMovementTrajectoryCalculation : MonoBehaviour
{
    [SerializeField] Vector3 relativeCamLookPoint;

    private InputManager inputScr;

    [SerializeField] Vector3 relativeCamSpawn;

    private GameObject playerCam_prefab;
    private GameObject playerCam_inst;


    public PlayerCamera PlayerCam_Scr { get; private set; }
    public Vector3 RelativeCamSpawn
    {
        get { return relativeCamSpawn; }

    }

    [SerializeField] Vector3 lockOnPos;
    public Vector3 LockOnPos
    {
        get { return lockOnPos; }

    }

    [SerializeField] GenericRay lockOnVisibilityRay;
    public GenericRay LockOnVisibilityRay
    {
        get
        {
            return lockOnVisibilityRay;
        }
    }
    void Update()
    {
        if (FindResources() && FindDependencies())
        {
            float forwardMovement_input_noSnap = 0;
            switch (Constants.CONTROLLER)
            {
                case ControlType.Controller:
                    forwardMovement_input_noSnap = inputScr.GetInputByName("Movement", "ForBack_Controller_NoSnap", true);
                    break;
                case ControlType.Keyboard:
                    forwardMovement_input_noSnap = inputScr.GetInputByName("Movement", "ForBack_NoSnap");
                    break;

            }

            if (playerCam_inst == null)
            {
                playerCam_inst = Instantiate(playerCam_prefab, gameObject.transform.position + (gameObject.transform.forward * relativeCamSpawn.z) + (gameObject.transform.right * relativeCamSpawn.x) + (gameObject.transform.up * relativeCamSpawn.y), Quaternion.Euler(Vector3.zero), null);
            }

            if (playerCam_inst != null)
            {
                if (PlayerCam_Scr == null)
                {
                    PlayerCam_Scr = playerCam_inst.GetComponent<PlayerCamera>();

                }

                if (PlayerCam_Scr != null)
                {
                    float sideMovement_input_noSnap = 0;
                    switch (Constants.CONTROLLER)
                    {
                        case ControlType.Controller:
                            sideMovement_input_noSnap = inputScr.GetInputByName("Movement", "LeftRight_Controller_NoSnap", true);
                            break;
                        case ControlType.Keyboard:
                            sideMovement_input_noSnap = inputScr.GetInputByName("Movement", "LeftRight_NoSnap", true);
                            break;

                    }

                    PlayerCam_Scr.CamDistance = PlayerCam_Scr.CamDistance_Orig + (PlayerCam_Scr.CamLeanExtent * forwardMovement_input_noSnap);

                    PlayerCam_Scr.LookTarget = gameObject.transform.position + (gameObject.transform.forward * (relativeCamLookPoint.z + (PlayerCam_Scr.CamTurnExtent * Mathf.Abs(sideMovement_input_noSnap)))) + (gameObject.transform.right * relativeCamLookPoint.x) + (gameObject.transform.up * relativeCamLookPoint.y);
                    PlayerCam_Scr.MoveScrSetLookTarget = true;
                }
            }


        }

    }

    private void OnDestroy()
    {
        if (playerCam_inst != null)
        {
            Destroy(playerCam_inst);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(gameObject.transform.position + (gameObject.transform.forward * relativeCamSpawn.z) + (gameObject.transform.right * relativeCamSpawn.x) + (gameObject.transform.up * relativeCamSpawn.y), ExtendedFunctionality.AverageOfVector3Values(gameObject.transform.localScale) * 0.3f);

        if (FindDependencies())
        {
            float sideMovement_input_noSnap = 0;
            switch (Constants.CONTROLLER)
            {
                case ControlType.Controller:
                    sideMovement_input_noSnap = inputScr.GetInputByName("Movement", "LeftRight_Controller_NoSnap", true);
                    break;
                case ControlType.Keyboard:
                    sideMovement_input_noSnap = inputScr.GetInputByName("Movement", "LeftRight_NoSnap", true);
                    break;

            }
            Gizmos.color = Color.blue;

            if (PlayerCam_Scr != null)
            {

                Gizmos.DrawSphere(gameObject.transform.position + (gameObject.transform.forward * (relativeCamLookPoint.z + (PlayerCam_Scr.CamTurnExtent * Mathf.Abs(sideMovement_input_noSnap)))) + (gameObject.transform.right * relativeCamLookPoint.x) + (gameObject.transform.up * relativeCamLookPoint.y), ExtendedFunctionality.AverageOfVector3Values(gameObject.transform.localScale) * 0.3f);
            }
            else
            {
                Gizmos.DrawSphere(gameObject.transform.position + (gameObject.transform.forward * relativeCamLookPoint.z) + (gameObject.transform.right * relativeCamLookPoint.x) + (gameObject.transform.up * relativeCamLookPoint.y), ExtendedFunctionality.AverageOfVector3Values(gameObject.transform.localScale) * 0.3f);

            }

        }


        Gizmos.color = Color.green;
        Gizmos.DrawSphere(gameObject.transform.position + (gameObject.transform.forward * 2), ExtendedFunctionality.AverageOfVector3Values(gameObject.transform.localScale) * 0.3f);

        Gizmos.color = Color.black;
        Gizmos.DrawSphere(gameObject.transform.position + (gameObject.transform.forward * lockOnPos.z) + (gameObject.transform.right * lockOnPos.x) + (gameObject.transform.up * lockOnPos.y), ExtendedFunctionality.AverageOfVector3Values(gameObject.transform.localScale) * 0.3f);


    }

    bool FindDependencies()
    {
        bool ret = false;

        if (inputScr == null)
        {
            inputScr = gameObject.GetComponent<InputManager>();

        }



        if ( inputScr != null)
        {
            ret = true;
        }

        return ret;
    }
    bool FindResources()
    {
        bool ret = false;

        if (playerCam_prefab == null)
        {
            playerCam_prefab = Resources.Load<GameObject>("Prefabs/Player/playerCam");
        }

        if (playerCam_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
