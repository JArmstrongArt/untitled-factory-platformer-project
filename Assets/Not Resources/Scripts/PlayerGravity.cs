﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum UpwardForceType
{
    None,
    Jump,
    BouncePad
}
public class PlayerGravity : Gravity, IEditorFunctionality
{
    public BouncePad BouncePadSrc { set; private get; }
    private UpwardForceType uFT = UpwardForceType.None;
    public UpwardForceType UFT
    {

        get
        {
            return uFT;
        }

        set
        {
            uFT = value;
        }
    }
    public bool UpwardForceQueued { get; set; }
    private PlayerJump jumpScr;

    private PlayerCameraAndMovementTrajectoryCalculation trajScr;

    private bool updateCamTargetY =false;

    private PlayerMovement moveScr;




    [SerializeField] float coyoteTime;
    public float CoyoteTime_Current { get; private set; }





    [SerializeField] GenericRay ceilingRay;



    protected override void FixedUpdate()
    {
        if (FindDependencies())
        {
            base.FixedUpdate();



            GenericRay curGroundRay = FindActiveGroundRay();
            bool sineMoveCond = curGroundRay != null && curGroundRay.ray_surface != null && curGroundRay.ray_surface.GetComponent<SineMove>() != null && curGroundRay.ray_surface.GetComponent<SineMove>().SineStrength.y != 0;
            bool moveTrackCond = curGroundRay != null && curGroundRay.ray_surface != null && curGroundRay.ray_surface.GetComponent<MovementTrackSubject>() != null;
            if (curGroundRay==null||sineMoveCond|| moveTrackCond)
            {
                updateCamTargetY = true;
            }

            if (curGroundRay == null)
            {

                if (gravityCurrent < 0)
                {
                    UFT = UpwardForceType.None;
                }

                if (ceilingRay.ray_surface != null)
                {
                    if (gravityCurrent > 0)
                    {
                        gravityCurrent = 0;
                    }
                }
                if (CoyoteTime_Current > 0)
                {
                    CoyoteTime_Current -= Time.fixedDeltaTime;
                }
            }
            else
            {
                

                if (gravityCurrent <= 0)
                {
                    CoyoteTime_Current = coyoteTime;

                    if (curGroundRay.Ray_HitAll != null)
                    {
                        if (updateCamTargetY == true || curGroundRay.Ray_HitAll[0].normal.y != 1)
                        {
                            if (trajScr.PlayerCam_Scr != null)
                            {
                                trajScr.PlayerCam_Scr.targetYPos = moveScr.gameObject.transform.position.y + trajScr.RelativeCamSpawn.y;

                            }
                            updateCamTargetY = false;
                        }
                    }

                }



            }

            if(curGroundRay!=null || CoyoteTime_Current > 0)
            {
                if (UpwardForceQueued == true)
                {

                    CoyoteTime_Current = 0;
                    switch (UFT)
                    {
                        case UpwardForceType.Jump:
                            gravityCurrent = Mathf.Abs(jumpScr.JumpStrength);
                            break;
                        case UpwardForceType.BouncePad:
                            if (BouncePadSrc != null)
                            {
                                gravityCurrent = BouncePadSrc.BouncePower;
                            }
                            else
                            {
                                gravityCurrent = 1.0f;

                            }
                            break;
                    }
                    UpwardForceQueued = false;
                }
            }






        }


    }



    protected override bool FindDependencies()
    {
        bool ret = false;
        bool parRet = base.FindDependencies();

        


        if (jumpScr == null)
        {
            jumpScr = gameObject.GetComponent<PlayerJump>();
        }

        if (trajScr == null)
        {
            trajScr = GameObject.FindObjectOfType<PlayerCameraAndMovementTrajectoryCalculation>();
        }

        if (moveScr == null)
        {
            moveScr = gameObject.GetComponent<PlayerMovement>();
        }
        if (moveScr!=null && trajScr != null && jumpScr != null &&parRet==true)
        {
            ret = true;
        }

        return ret;
    }
}
