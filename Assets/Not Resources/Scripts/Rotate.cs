﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] Vector3 spinVals;
    public Vector3 SpinVals
    {
        get
        {
            return spinVals;
        }

        set
        {
            spinVals = value;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x + (spinVals.x * Time.deltaTime), gameObject.transform.eulerAngles.y + (spinVals.y * Time.deltaTime), gameObject.transform.eulerAngles.z + (spinVals.z * Time.deltaTime));
    }
}
