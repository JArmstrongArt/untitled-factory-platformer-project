﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MaintainOffset : MonoBehaviour
{
    [SerializeField] UpdateType offsetUpdate;
    public UpdateType OffsetUpdate
    {
        set
        {
            offsetUpdate = value;
        }
    }
    public GameObject OffsetObj { private get; set; }

    private Vector3 targetPos;
    private void Awake()
    {
        Init();
    }
    public void Init()
    {
        if (OffsetObj != null)
        {
            targetPos = gameObject.transform.position - OffsetObj.transform.position;

        }
    }

    // Update is called once per frame
    void LateUpdate()
    {

        if (offsetUpdate == UpdateType.Late)
        {
            MainFunc();
        }

    }

    void FixedUpdate()
    {

        if (offsetUpdate == UpdateType.Fixed)
        {
            MainFunc();
        }

    }

    void Update()
    {

        if (offsetUpdate == UpdateType.Regular)
        {
            MainFunc();
        }

    }

    void MainFunc()
    {
        if (OffsetObj != null)
        {
            gameObject.transform.position = targetPos + OffsetObj.transform.position;

        }
    }
}
