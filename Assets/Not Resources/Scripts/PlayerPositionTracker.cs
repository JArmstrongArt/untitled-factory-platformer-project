﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionTracker : MonoBehaviour
{
    [SerializeField] float trackRecordRate;
    private float trackRecordRate_current = 0;
    private PlayerGravity gravScr;
    public Vector3 MostRecentPosition { get; private set; }

    private void Awake()
    {

    }
    private void Update()
    {
        if (FindDependencies())
        {
            if (trackRecordRate_current > 0)
            {
                trackRecordRate_current -= Time.deltaTime;
            }
            else
            {

                if (gravScr.AllGroundRaysActive() == true)
                {
                    GenericRay activeRay = gravScr.FindActiveGroundRay();
                    if (activeRay != null)
                    {
                        SineMove movingPlatformScr = activeRay.ray_surface.GetComponent<SineMove>();
                        Conveyor conveyScr = activeRay.ray_surface.GetComponent<Conveyor>();
                        MovementTrackSubject trackScr = activeRay.ray_surface.GetComponent<MovementTrackSubject>();
                        if (trackScr==null && movingPlatformScr == null && (conveyScr==null||(conveyScr!=null && conveyScr.Running==true)))
                        {

                            MostRecentPosition = gameObject.transform.position;
                            trackRecordRate_current = trackRecordRate;
                        }
                    }


                }

            }
        }

    }

    bool FindDependencies()
    {
        bool ret = false;

        if (gravScr == null)
        {
            gravScr = gameObject.GetComponent<PlayerGravity>();
        }

        if (gravScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
