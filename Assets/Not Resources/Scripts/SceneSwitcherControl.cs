﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSwitcherControl : MonoBehaviour
{
    [SerializeField] UIFlipbook uiAnim_sceneTransitionIn;
    [SerializeField] UIFlipbook uiAnim_sceneTransitionOut;
    public UIFlipbook UiAnim_SceneTransitionOut
    {
        get
        {
            return uiAnim_sceneTransitionOut;
        }
    }
    private void Awake()
    {
        uiAnim_sceneTransitionIn.Play();
    }
}
