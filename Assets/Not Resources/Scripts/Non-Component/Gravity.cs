﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour, IEditorFunctionality
{
    private CharacterController charScr;

    [SerializeField] float gravityStrength;
    [SerializeField] float gravityMaxUp;
    [SerializeField] float gravityMaxDown;

    [HideInInspector]
    public float gravityCurrent;
    public bool Grounded { get; private set; }



    [SerializeField] GameObject groundRayParent;

    private List<GenericRay> allGroundRays = new List<GenericRay>();

    //private Vector3 movingPlatMoveForcePrev;
    public Vector3 ConveyorMoveForcePrev { private get; set; }

    public void EditorUpdate()
    {
        gravityMaxDown = Mathf.Abs(gravityMaxDown);
        gravityMaxUp = Mathf.Abs(gravityMaxUp);

    }

    void GenerateAllGroundRays(bool fullRefresh = false)
    {
        if (fullRefresh)
        {
            allGroundRays = new List<GenericRay>();
        }
        if (allGroundRays.Count <= 0)
        {
            for (int i = 0; i < groundRayParent.transform.childCount; i++)
            {
                Transform child = groundRayParent.transform.GetChild(i);
                if (child != null)
                {
                    GameObject childObj = child.gameObject;
                    if (childObj != null)
                    {
                        GenericRay curRay = childObj.GetComponent<GenericRay>();

                        if (curRay != null)
                        {
                            allGroundRays.Add(curRay);
                        }
                    }

                }

            }
        }
    }

    void Update()
    {
        GenerateAllGroundRays();
    }



    public GenericRay FindActiveGroundRay()
    {
        GenericRay ret = null;

        foreach (GenericRay ray in allGroundRays)
        {
            if (ray != null)
            {
                if (ray.ray_surface != null)
                {
                    Collider rayCol = ray.ray_surface.GetComponent<Collider>();
                    if (rayCol != null)
                    {
                        if (rayCol.isTrigger == false)
                        {
                            ret = ray;
                            break;
                        }
                    }

                }
            }
        }
        return ret;
    }



    public bool AllGroundRaysActive()
    {
        bool active = true;

        foreach (GenericRay ray in allGroundRays)
        {
            if (ray != null)
            {
                if (ray.ray_surface == null || (ray.ray_surface != null && ray.ray_surface.GetComponent<Collider>() != null && ray.ray_surface.GetComponent<Collider>().isTrigger == true))
                {
                    active = false;
                    break;
                }
            }

        }
        return active;
    }

    bool AnyGroundRayActive()
    {
        bool active = false;

        foreach (GenericRay ray in allGroundRays)
        {
            if (ray != null)
            {
                if (ray.ray_surface != null)
                {
                    Collider rayCol = ray.ray_surface.GetComponent<Collider>();
                    if (rayCol != null)
                    {
                        if (rayCol.isTrigger == false)
                        {
                            active = true;
                            break;
                        }
                    }

                }
            }

        }
        return active;
    }
    protected virtual void FixedUpdate()
    {
        if (FindDependencies())
        {




            GenericRay curGroundRay = FindActiveGroundRay();
            if (curGroundRay == null)
            {
                if (gravityCurrent >= -Mathf.Abs(gravityMaxDown) && gravityCurrent <= Mathf.Abs(gravityMaxUp))
                {
                    gravityCurrent -= gravityStrength * Time.fixedDeltaTime;
                }

                if (gravityCurrent > Mathf.Abs(gravityMaxUp))
                {
                    gravityCurrent = Mathf.Abs(gravityMaxUp);
                }
                
                if(gravityCurrent < -Mathf.Abs(gravityMaxDown))
                {
                    gravityCurrent = -Mathf.Abs(gravityMaxDown);

                }

            }
            else
            {

                if (gravityCurrent <= 0)
                {
                    charScr.Move(Vector3.down * 0.3f);//correction for when there's a bit of space between the floor and the player's feet
                    gravityCurrent = 0;

                }



            }



            if (curGroundRay != null)
            {
                SineMove movingPlatformScr = curGroundRay.ray_surface.GetComponent<SineMove>();

                if (movingPlatformScr != null)
                {

                    //movingPlatMoveForcePrev = movingPlatformScr.DiffPos;
                    //charScr.Move(movingPlatMoveForcePrev);
                    charScr.Move(movingPlatformScr.DiffPos);
                }
                else
                {
                    //movingPlatMoveForcePrev = Vector3.zero;
                }

                Conveyor convScr = curGroundRay.ray_surface.GetComponent<Conveyor>();
                if (convScr != null && convScr.Running==true)
                {
                    ConveyorMoveForcePrev = ExtendedFunctionality.EulerAnglesToDirection(convScr.EulerConveyorDir).normalized * convScr.ConveyorSpeed * Time.fixedDeltaTime;
                    charScr.Move(ConveyorMoveForcePrev);
                }
                else
                {
                    ConveyorMoveForcePrev = Vector3.zero;
                }

                MovementTrackSubject trackScr = curGroundRay.ray_surface.GetComponent<MovementTrackSubject>();
                if (trackScr != null)
                {
                    charScr.Move(trackScr.DiffPos);
                }

            }
            else
            {
                //charScr.Move(movingPlatMoveForcePrev);
                charScr.Move(ConveyorMoveForcePrev);

            }





            Grounded = AnyGroundRayActive();

            charScr.Move(Vector3.up * gravityCurrent);
        }


    }



    protected virtual bool FindDependencies()
    {
        bool ret = false;

        if (charScr == null)
        {
            charScr = gameObject.GetComponent<CharacterController>();
        }


        if ( charScr != null)
        {
            ret = true;
        }

        return ret;
    }
}
