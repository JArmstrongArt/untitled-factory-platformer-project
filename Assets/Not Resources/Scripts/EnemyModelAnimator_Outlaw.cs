﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModelAnimator_Outlaw : EnemyModelAnimator
{
    public bool FireAnimDue { private get; set; }
    private bool notFiredYet=true;

    private GameObject sE_outlawShoot_prefab;

    protected override void Awake()
    {

        if (FindResources() && FindDependencies())
        {
            SetMdlPath_OneTimeOnly("Prefabs/Enemies/outlawModel");

            base.Awake();


        }
    }
    protected override void Update()
    {
        if (FindResources() && FindDependencies())
        {
            base.Update();



            if (mdl_inst_anim != null)
            {



                if (enScr.ChaseMode == true)
                {
                    if (enScr.AlertTime_Current <= 0)
                    {
                        if (FireAnimDue)
                        {
                            Instantiate(sE_outlawShoot_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                            notFiredYet = false;
                            mdl_inst_anim.Play("fire",-1,0);
                            FireAnimDue = false;
                        }
                        else
                        {
                            if (notFiredYet)
                            {
                                mdl_inst_anim.Play("aim");

                            }
                        }


                    }
                }
                else
                {
                    notFiredYet = true;
                }





            }
        }
    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet= base.FindResources();

        if (sE_outlawShoot_prefab == null)
        {
            sE_outlawShoot_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_outlawShoot");
        }

        if(sE_outlawShoot_prefab!=null && parRet == true)
        {
            ret = true;
        }
        return ret;
    }
}
