﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twister : Enemy
{


    private GameObject sE_twisterWarn_prefab;
    private bool twisterSndPlayed = false;


    private void Update()
    {
        if(FindResources() && base.FindDependencies())
        {
            if (AlertTime_Current >= 0&& ChaseMode==true)
            {
                if (twisterSndPlayed == false)
                {
                    Instantiate(sE_twisterWarn_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    twisterSndPlayed = true;
                }
            }
            else
            {
                twisterSndPlayed = false;
            }
        }

    }

    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();

        if (sE_twisterWarn_prefab == null)
        {
            sE_twisterWarn_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_twisterWarn");
        }

        if(sE_twisterWarn_prefab!=null && parRet == true)
        {
            ret = true;
        }

        return ret;

    }

}
