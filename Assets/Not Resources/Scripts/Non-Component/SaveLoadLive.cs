﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
struct saveData
{
    public List<int> allCollectedIDs;
    public List<int> collectedCoinIDs;


}


public abstract class SaveLoadLive : MonoBehaviour
{
    public static void SaveGame()
    {
        BinaryFormatter bF = new BinaryFormatter();
        FileStream fS = File.Create(Application.dataPath + "/saveGame.dat");
        saveData sD;

        PlayerStats statScr = GameObject.FindObjectOfType<PlayerStats>();

        if (statScr != null)
        {
            sD.allCollectedIDs = statScr.allCollectedIDs;
            sD.collectedCoinIDs = statScr.collectedCoinIDs;


            bF.Serialize(fS, sD);
        }


        fS.Close();
    }

    public static void LoadGame()
    {
        if (File.Exists(Application.dataPath + "/saveGame.dat"))
        {
            BinaryFormatter bF = new BinaryFormatter();
            FileStream fS = File.Open(Application.dataPath + "/saveGame.dat", FileMode.Open);
            saveData sD = (saveData)bF.Deserialize(fS);
            fS.Close();

            PlayerStats statScr = GameObject.FindObjectOfType<PlayerStats>();

            if (statScr != null)
            {
                statScr.allCollectedIDs = sD.allCollectedIDs;
                statScr.collectedCoinIDs = sD.collectedCoinIDs;
            }

            Collectable[] allCollectables = GameObject.FindObjectsOfType<Collectable>();

            foreach(Collectable col in allCollectables)
            {
                col.CheckIfAlreadyObtained();
            }
        }
    }
}
