﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlane : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        PlayerPositionTracker posTrack = other.GetComponent<PlayerPositionTracker>();
        if (other.GetComponent<PlayerPositionTracker>()!=null)
        {

            PlayerStats statsScr = other.GetComponent<PlayerStats>();

            if (statsScr != null)
            {
                statsScr.ShiftHealth(-1);

                if (statsScr.CurrentHealth > 0)
                {
                    CharacterController contr = other.GetComponent<CharacterController>();
                    if (contr != null)
                    {
                        contr.enabled = false;
                    }
                    other.gameObject.transform.position = posTrack.MostRecentPosition;
                    if (contr != null)
                    {
                        contr.enabled = true;
                    }
                }


            }
        }


        Enemy enCheck = other.GetComponent<Enemy>();
        if (enCheck != null)
        {
            enCheck.ShiftHealth(Mathf.Abs(enCheck.CurrentHealth) * -1);
        }
    }
}
