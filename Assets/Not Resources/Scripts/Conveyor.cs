﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    [SerializeField] Vector3 eulerConveyorDir;
    public Vector3 EulerConveyorDir
    {
        get
        {
            return eulerConveyorDir;
        }
    }
    [SerializeField] float conveyorSpeed;
    public float ConveyorSpeed
    {
        get
        {
            return conveyorSpeed;
        }
    }
    private bool running = true;
    public bool Running
    {
        get
        {
            return running;
        }

        set
        {
            running = value;
        }
    }


    private void OnDrawGizmos()
    {
        float lineLen = 4;
        Vector3 lineOrigin = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2, gameObject.transform.position.z);
        Gizmos.color = Color.yellow;

        Gizmos.DrawSphere(lineOrigin, 1);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(lineOrigin, lineOrigin + (ExtendedFunctionality.EulerAnglesToDirection(eulerConveyorDir) * lineLen));
    }
}
