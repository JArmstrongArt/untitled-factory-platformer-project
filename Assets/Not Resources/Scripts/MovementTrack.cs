﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-200)]//this is because subjects of this track are on a -100 delay and rely on the init of this class to function

public class MovementTrack : MonoBehaviour, IEditorFunctionality
{
    
    private List<Transform> allTrackCorners = new List<Transform>();
    public List<Transform> AllTrackCorners
    {
        get
        {
            return allTrackCorners;
        }
    }

    void RefreshTrackCorners()
    {
        allTrackCorners = new List<Transform>();

        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            ExtendedFunctionality.GutObject(gameObject.transform.GetChild(i).gameObject);
            allTrackCorners.Add(gameObject.transform.GetChild(i).gameObject.transform);
        }
    }
    void Awake()
    {
        RefreshTrackCorners();
    }
    public void EditorUpdate()
    {
        RefreshTrackCorners();


    }

    void OnDrawGizmos()
    {
        int LOD = 8;
        float ballSize = 1.0f;
        LOD = Mathf.Clamp(LOD, 1, int.MaxValue);
        for(int i=0;i<allTrackCorners.Count;i++)
        {
            if (allTrackCorners[i] != null)
            {
                Gizmos.color = Color.Lerp(Color.blue, Color.red, ((float)1 / (float)(allTrackCorners.Count - 1)) * i);
                Gizmos.DrawSphere(allTrackCorners[i].position, ballSize);


                if (i < allTrackCorners.Count - 1)
                {
                    if (allTrackCorners[i + 1] != null)
                    {
                        Gizmos.color = Color.green;
                        Vector3 meToNext = allTrackCorners[i + 1].position - allTrackCorners[i].position;

                        for (int j = 1; j < LOD; j++)
                        {
                            Gizmos.DrawSphere(allTrackCorners[i].position + (meToNext.normalized * (((float)meToNext.magnitude / (float)(LOD - 1)) * j)), ballSize * 0.5f);
                        }
                    }


                }
            }


        }
    }
}
