﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public abstract class ExtendedFunctionality:MonoBehaviour
{
    public static void GutObject(GameObject sourceObj, Component specialExclude = null)
    {
        while (sourceObj.transform.childCount > 0)
        {
            ExtendedFunctionality.SafeDestroy(sourceObj.transform.GetChild(0).gameObject);
        }

        RemoveAllComponents(sourceObj, specialExclude);
    }
    public static void RemoveAllComponents(GameObject sourceObj, Component specialExclude = null)
    {
        List<Component> allScrOnObj = sourceObj.GetComponents<Component>().ToList();


        List<Transform> transformComponents = new List<Transform>();
        for (int i = 0; i < allScrOnObj.Count; i++)
        {
            Transform tranMode = allScrOnObj[i] as Transform;
            if (tranMode != null)
            {
                transformComponents.Add(tranMode);
            }

        }
        int whileBump = 1;
        if (specialExclude == null)
        {
            whileBump = 0;
        }
        while (allScrOnObj.Count > whileBump + transformComponents.Count)
        {
            for (int i = 0; i < allScrOnObj.Count; i++)
            {
                if (i >= 0 && i < allScrOnObj.Count)
                {
                    if (allScrOnObj[i] != specialExclude && allScrOnObj[i] as Transform == null)
                    {

                        ExtendedFunctionality.SafeDestroy(allScrOnObj[i]);
                        allScrOnObj.RemoveAt(i);
                    }
                }

            }
        }
    }
    public static Vector3 EulerAnglesToDirection(Vector3 euler)
    {
        return (Quaternion.Euler(euler) * Vector3.forward).normalized;
    }
    public static List<RendAndOrigMatPair> GeneratePairingsFromCurrentRends(GameObject parent)
    {
        List<RendAndOrigMatPair> ret = new List<RendAndOrigMatPair>();
        List<Renderer> allMdlRenderers = ExtendedFunctionality.GetAllRenderersInChildren(parent);
        foreach (Renderer rend in allMdlRenderers)
        {
            if (rend != null)
            {
                RendAndOrigMatPair curPair = new RendAndOrigMatPair(rend);

                foreach (Material mat in rend.materials)
                {
                    curPair.origMats.Add(mat);
                }

                ret.Add(curPair);
            }
        }
        return ret;
    }


    public static List<Renderer> GetAllRenderersInChildren(GameObject parent)
    {

        Renderer [] chilRends = parent.GetComponentsInChildren<Renderer>();
        List<Renderer> ret = new List<Renderer>();
        foreach (Renderer chil in chilRends)
        {
            if (chil != null)
            {
                ret.Add(chil);

            }
        }

        return ret;
    }
    public static void SafeDestroy(GameObject obj)
    {

        if (obj != null)
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                DestroyImmediate(obj);
            }
            else
            {
                Destroy(obj);

            }
        }
    }

    public static void SafeDestroy(Component comp)
    {
        if (comp != null)
        {

            if (Application.isEditor && !Application.isPlaying)
            {
                DestroyImmediate(comp);
            }
            else
            {
                Destroy(comp);

            }
        }
    }

    public static float WrapValue(float val, float addition, float lowerBound, float upperBound)
    {
        if (lowerBound == upperBound)
        {
            return lowerBound;
        }
        else
        {
            if (upperBound < lowerBound)
            {
                float temp = upperBound;
                upperBound = lowerBound;
                lowerBound = temp;
            }


            float unwrappedValue = val + addition;


            float resultantValue = 0;

            if (unwrappedValue > upperBound)
            {
                resultantValue = lowerBound + (unwrappedValue - upperBound - 1);
            }

            if (unwrappedValue < lowerBound)
            {
                resultantValue = upperBound - (lowerBound - unwrappedValue - 1);
            }

            if (unwrappedValue >= lowerBound && unwrappedValue <= upperBound)
            {
                resultantValue = unwrappedValue;
            }

            return resultantValue;
        }

    }
    public static float AverageOfVector3Values(Vector3 vec)
    {
        return ((float)(vec.x + vec.y + vec.z) / (float)3);
    }

    public static Vector3 MultiplyVector3s(List<Vector3> factors)
    {
        Vector3 ret = Vector3.zero;

        if (ListTrueEmpty(factors) == false)
        {
            ret = factors[0];

            if (factors.Count > 1)
            {
                for (int i = 1; i < factors.Count; i++)
                {
                    ret = new Vector3(ret.x * factors[i].x, ret.y * factors[i].y, ret.z * factors[i].z);
                }
            }

        }
        return ret;
    }

    public static bool ListTrueEmpty<T>(List<T> check)
    {
        bool ret = false;

        if (check == null)
        {
            ret = true;
        }
        else
        {
            if (check.Count <= 0)
            {
                ret = true;
            }
        }
        return ret;
    }
}
