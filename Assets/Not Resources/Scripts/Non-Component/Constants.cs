﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControlType
{
    Keyboard,
    Controller
}
public class Constants
{
    public static bool DEBUG_MODE = true;
    public static ControlType CONTROLLER = ControlType.Controller;
    public static Vector3 NULLVEC3 = new Vector3(-10000, -10000, -10000);
}
