﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//i want to put this under Enemy.cs BUT then i cant override the player ShiftHealth function
public enum EnemyCauseOfDamage
{
    Other,
    Dash,
    SprayerFire,
    Rifle,
    Lob

}
public class Creature : Entity,IEditorFunctionality
{


    [SerializeField] int maxHealth;




    public int MaxHealth
    {
        get { return maxHealth; }

    }

    public int CurrentHealth { get; private set; }

    protected override void Awake()
    {
        if (base.FindResources() )
        {
            base.Awake();
            CurrentHealth = maxHealth;

        }

    }

    public virtual int ShiftHealth(int shiftAmount, EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {

        int ret = CurrentHealth;
        if (base.FindResources() )
        {
            int health_proposed = CurrentHealth + shiftAmount;
            health_proposed = Mathf.Clamp(health_proposed, 0, maxHealth);
            CurrentHealth = health_proposed;





            ret = CurrentHealth;
        }

        return ret;
    }

    public virtual void EditorUpdate()
    {
        CharacterController charScr = gameObject.GetComponent<CharacterController>();
        if (charScr == null)
        {
            charScr = gameObject.AddComponent<CharacterController>();
        }

        if (charScr != null)
        {
            charScr.slopeLimit = 45;
            charScr.stepOffset = 0;
            charScr.skinWidth = 0.08f;
            charScr.minMoveDistance = 0.001f;

        }


        if (maxHealth < 1)
        {
            maxHealth = 1;
        }
    }


}
