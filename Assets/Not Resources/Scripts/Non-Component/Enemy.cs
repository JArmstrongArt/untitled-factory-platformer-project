﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum NoChaseMode_TurnDecide
{
    Forward,
    Right,
    Left
}

public enum NoChaseMode_MoveDecide
{
    Moving,
    NotMoving
}

public enum DamageType
{
    Freeze,
    Fire,
    Other
}

public abstract class Enemy : Creature,IEditorFunctionality
{


    private DamageType dmgTypeEnum = DamageType.Other;
    public DamageType DmgTypeEnum
    {
        set
        {
            dmgTypeEnum = value;
        }

        get
        {
            return dmgTypeEnum;
        }
    }
    private NoChaseMode_TurnDecide noChaseMode_turnDecideEnum = NoChaseMode_TurnDecide.Forward;
    [HideInInspector]
    public NoChaseMode_TurnDecide NoChaseMode_TurnDecideEnum
    {
        get
        {
            return noChaseMode_turnDecideEnum;
        }
    }
    private NoChaseMode_MoveDecide noChaseMode_moveDecideEnum = NoChaseMode_MoveDecide.Moving;
    [HideInInspector]
    public NoChaseMode_MoveDecide NoChaseMode_MoveDecideEnum
    {
        get
        {
            return noChaseMode_moveDecideEnum;
        }
    }

    [SerializeField] Vector2 noChaseMode_turnDecide_timerRange;
    [SerializeField] Vector2 noChaseMode_moveDecide_timerRange;
    private float noChaseMode_turnDecide_timer_current;
    private float noChaseMode_moveDecide_timer_current;

    private GameObject sE_enemyDmg_prefab;
    private EnemyModelAnimator mdlAnimScr;

    private GameObject pS_firework_prefab;
    private GameObject sE_firework_prefab;

    [SerializeField] float awarenessRadius;
    private Vector3 origPos;


    public bool ChaseMode { get; private set; }
    protected Vector3 moveDir;

    private CharacterController charScr;

    [SerializeField] float moveSpeed;
    private float moveSpeed_orig;
    [SerializeField] float homeRadius;

    private GameObject rightTurnerObj;
    private GameObject leftTurnerObj;
    [SerializeField] float alertTime;
    public float AlertTime_Current { get; private set; }

    protected PlayerStats playerScr;//i dont need playerstats specifically, but it is a child of the creature class and thats the closest i have to a representation of a 'player' on my player object
    [SerializeField] float chaseModeSpeedMultiplier;
    public float ChaseModeSpeedMultiplier
    {
        get
        {
            return chaseModeSpeedMultiplier;
        }
    }

    [SerializeField] float freezeLimit;

    private GameObject sE_enemyFreezeDie_prefab;
    private GameObject sE_enemyFreezeThaw_prefab;
    public float FreezeProgress {get; private set; }




    public float FrozenTimer_Current { get; set; }
    [SerializeField] float freezeLength;

    private List<HurtRegion> allHurtRegionsUnderSelf = new List<HurtRegion>();

    private GameObject sE_enemyFreezeProgress_prefab;
    private GameObject sE_enemyFreeze_prefab;

    [SerializeField] float aggroTimer;//how long the enemy will be chasing you even out of range if you attack it
    private float aggroTimer_current;
    [SerializeField] GenericRay sightRay;
    [SerializeField] float playerPermeanceTimer;//how long the enemy will remember the player exists after all of the other chasemode conditions have been exhausted
    private float playerPermeanceTimer_current;
    [SerializeField] GenericRay walkableRay;
    [SerializeField] float chaseModePlayerSurroundRadius;
    [SerializeField] float chaseMode_turnToPlayerRate;

    [SerializeField] float freezeSnd_intervalTime;
    private float freezeSnd_intervalTime_current;

    [SerializeField] float dmgSnd_intervalTime;
    private float dmgSnd_intervalTime_current;
    float ShiftFreezeProgress_Internal(float shiftAmount)
    {

        float freezeProgress_proposed = FreezeProgress + shiftAmount;
        freezeProgress_proposed = Mathf.Clamp(freezeProgress_proposed, 0, freezeLimit);
        FreezeProgress = freezeProgress_proposed;



        if (FrozenTimer_Current <= 0)
        {
            if (shiftAmount > 0)
            {
                mdlAnimScr.HurtEffectTrigger(DamageType.Freeze);

                if (freezeSnd_intervalTime_current <= 0)
                {
                    Instantiate(sE_enemyFreezeProgress_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    freezeSnd_intervalTime_current = freezeSnd_intervalTime;
                }
            }

            if (FreezeProgress >= freezeLimit)
            {
                FrozenTimer_Current = freezeLength;
                Instantiate(sE_enemyFreeze_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            }
        }

        return FreezeProgress;
    }

    public float ShiftFreezeProgress(float shiftAmount, EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {

        float ret = FreezeProgress;
        if (FindResources() && FindDependencies())
        {
            ChikChik chikScr = gameObject.GetComponent<ChikChik>();
            if (chikScr != null)
            {
                if (chikScr.ShieldStateEnum != ShieldState.Raised)
                {
                    ret = ShiftFreezeProgress_Internal(shiftAmount);

                }
                else
                {
                    if (chikScr.ChikAnimScr != null)
                    {
                        if (cOD != EnemyCauseOfDamage.SprayerFire)
                        {


                            chikScr.ChikAnimScr.BlockAnimDue = true;
                        }

                    }
                    else
                    {

                        ret = ShiftFreezeProgress_Internal(shiftAmount);


                    }
                }
            }
            else
            {
                ret = ShiftFreezeProgress_Internal(shiftAmount);

            }




        }

        return ret;
    }

    public void GenerateHurtRegions()
    {
        allHurtRegionsUnderSelf = gameObject.GetComponentsInChildren<HurtRegion>().ToList();

    }

    public void HurtRegionToggle(bool state=true)
    {
        foreach(HurtRegion hR in allHurtRegionsUnderSelf)
        {
            if (hR != null)
            {
                if (FrozenTimer_Current <= 0)
                {
                    hR.TriggerEnabled = state;

                }
                else
                {
                    hR.TriggerEnabled = false;

                }

            }
        }
    }



    public void DashFreezeKill(EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {
        if(FindDependencies() && FindResources())
        {
            Instantiate(sE_enemyFreezeDie_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
            ShiftHealth(-300, cOD);//set to 300 because every enemy EXCEPT FOR splat has less than or equal to 300 health
            FrozenTimer_Current = 0;
        }

    }


    public override void EditorUpdate()
    {
        base.EditorUpdate();
        origPos = gameObject.transform.position;
        noChaseMode_moveDecide_timerRange = new Vector2(Mathf.Abs( noChaseMode_moveDecide_timerRange.x),Mathf.Abs( noChaseMode_moveDecide_timerRange.y));
        noChaseMode_turnDecide_timerRange = new Vector2(Mathf.Abs(noChaseMode_turnDecide_timerRange.x),Mathf.Abs(noChaseMode_turnDecide_timerRange.y));
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(gameObject.transform.position, awarenessRadius);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(origPos, homeRadius);
    }

    protected override void Awake()
    {


        if(FindDependencies() && FindResources())
        {
            base.Awake();

            FrozenTimer_Current = 0;
            AlertTime_Current = alertTime;
            moveDir = gameObject.transform.forward;
            moveSpeed_orig = moveSpeed;
            origPos = gameObject.transform.position;
            noChaseMode_moveDecide_timer_current = Random.Range(noChaseMode_moveDecide_timerRange.x, noChaseMode_moveDecide_timerRange.y);
            noChaseMode_turnDecide_timer_current = Random.Range(noChaseMode_turnDecide_timerRange.x, noChaseMode_turnDecide_timerRange.y);
            GenerateHurtRegions();

        }
    }

    protected virtual void FixedUpdate()
    {
        if (FindDependencies() && FindResources())
        {
            if (freezeSnd_intervalTime_current > 0)
            {
                freezeSnd_intervalTime_current -= Time.fixedDeltaTime;
            }

            if (dmgSnd_intervalTime_current > 0)
            {
                dmgSnd_intervalTime_current -= Time.fixedDeltaTime;
            }

            bool distancePermitted = Vector3.Distance(gameObject.transform.position, new Vector3(playerScr.transform.position.x, gameObject.transform.position.y, playerScr.transform.position.z)) <= awarenessRadius;
            if (sightRay != null)
            {
                Vector3 sightVec = playerScr.transform.position - gameObject.transform.position;
                sightRay.customDir_vec = sightVec.normalized;
                sightRay.rayLength = sightVec.magnitude;
                sightRay.RayRoutine();

                distancePermitted = distancePermitted && sightRay.ray_surface == null;
            }

            if (distancePermitted)
            {
                playerPermeanceTimer_current = playerPermeanceTimer;

            }

            if (aggroTimer_current > 0 || distancePermitted || playerPermeanceTimer_current > 0)
            {
                if (ChaseMode == false)
                {
                    ChaseMode = true;

                }
            }
            else
            {
                ChaseMode = false;
                AlertTime_Current = alertTime;

            }

            if (aggroTimer_current > 0)
            {
                aggroTimer_current -= Time.fixedDeltaTime;
            }

            if (playerPermeanceTimer_current > 0)
            {
                playerPermeanceTimer_current -= Time.fixedDeltaTime;
            }

            if (FrozenTimer_Current > 0)
            {
                FreezeProgress = 0;
                FrozenTimer_Current -= Time.deltaTime;

                if (FrozenTimer_Current <= 0)
                {
                    Instantiate(sE_enemyFreezeThaw_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                }


            }
            else
            {
                FrozenTimer_Current = 0;




                if (ChaseMode == false)
                {
                    bool withinWalkableLand = Vector3.Distance(gameObject.transform.position, new Vector3(origPos.x, gameObject.transform.position.y, origPos.z)) <= homeRadius;
                    if (walkableRay != null)
                    {
                        withinWalkableLand = withinWalkableLand && walkableRay.ray_surface != null;
                    }
                    if (withinWalkableLand)
                    {
                        if (rightTurnerObj == null)
                        {
                            rightTurnerObj = new GameObject();
                            rightTurnerObj.name = gameObject.name + "_rightTurner";
                        }

                        if (leftTurnerObj == null)
                        {
                            leftTurnerObj = new GameObject();
                            leftTurnerObj.name = gameObject.name + "_leftTurner";
                        }

                        if (rightTurnerObj != null)
                        {
                            rightTurnerObj.transform.position = gameObject.transform.position + (new Vector3(gameObject.transform.right.x, 0, gameObject.transform.right.z) * 5);
                        }
                        if (leftTurnerObj != null)
                        {
                            leftTurnerObj.transform.position = gameObject.transform.position + (new Vector3(-gameObject.transform.right.x, 0, -gameObject.transform.right.z) * 5);
                        }

                        if (rightTurnerObj != null && leftTurnerObj != null)
                        {
                            if (noChaseMode_moveDecide_timer_current > 0)
                            {
                                noChaseMode_moveDecide_timer_current -= Time.fixedDeltaTime;
                            }

                            if (noChaseMode_turnDecide_timer_current > 0)
                            {
                                noChaseMode_turnDecide_timer_current -= Time.fixedDeltaTime;
                            }

                            if (noChaseMode_moveDecide_timer_current <= 0)
                            {
                                noChaseMode_moveDecideEnum = (NoChaseMode_MoveDecide)Random.Range(System.Enum.GetValues(typeof(NoChaseMode_MoveDecide)).Cast<int>().Min(), System.Enum.GetValues(typeof(NoChaseMode_MoveDecide)).Cast<int>().Max() + 1);
                                noChaseMode_moveDecide_timer_current = Random.Range(noChaseMode_moveDecide_timerRange.x, noChaseMode_moveDecide_timerRange.y);

                            }

                            if (noChaseMode_turnDecide_timer_current <= 0)
                            {
                                noChaseMode_turnDecideEnum = (NoChaseMode_TurnDecide)Random.Range(System.Enum.GetValues(typeof(NoChaseMode_TurnDecide)).Cast<int>().Min(), System.Enum.GetValues(typeof(NoChaseMode_TurnDecide)).Cast<int>().Max() + 1);
                                noChaseMode_turnDecide_timer_current = Random.Range(noChaseMode_turnDecide_timerRange.x, noChaseMode_turnDecide_timerRange.y);

                            }

                            switch (noChaseMode_turnDecideEnum)
                            {
                                case NoChaseMode_TurnDecide.Forward:
                                    moveDir = gameObject.transform.forward;
                                    break;
                                case NoChaseMode_TurnDecide.Left:
                                    if (noChaseMode_moveDecideEnum == NoChaseMode_MoveDecide.Moving)
                                    {
                                        moveDir = Vector3.Lerp(moveDir, (new Vector3(leftTurnerObj.transform.position.x, gameObject.transform.position.y, leftTurnerObj.transform.position.z) - gameObject.transform.position).normalized, 0.3f * Time.fixedDeltaTime);

                                    }
                                    else
                                    {
                                        moveDir = gameObject.transform.forward;

                                    }
                                    break;

                                case NoChaseMode_TurnDecide.Right:
                                    if (noChaseMode_moveDecideEnum == NoChaseMode_MoveDecide.Moving)
                                    {
                                        moveDir = Vector3.Lerp(moveDir, (new Vector3(rightTurnerObj.transform.position.x, gameObject.transform.position.y, rightTurnerObj.transform.position.z) - gameObject.transform.position).normalized, 0.3f * Time.fixedDeltaTime);

                                    }
                                    else
                                    {
                                        moveDir = gameObject.transform.forward;

                                    }
                                    break;
                            }

                            switch (noChaseMode_moveDecideEnum)
                            {
                                case NoChaseMode_MoveDecide.Moving:
                                    moveSpeed = moveSpeed_orig;
                                    break;
                                case NoChaseMode_MoveDecide.NotMoving:
                                    moveSpeed = 0;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        noChaseMode_turnDecideEnum = NoChaseMode_TurnDecide.Forward;
                        //moveDir = Vector3.Lerp(moveDir, (new Vector3(origPos.x, gameObject.transform.position.y, origPos.z) - gameObject.transform.position).normalized, 3 * Time.fixedDeltaTime);
                        moveDir = (new Vector3(origPos.x, gameObject.transform.position.y, origPos.z) - gameObject.transform.position).normalized;
                    }

                }
                else
                {
                    Vector3 meToPlayer = new Vector3(playerScr.transform.position.x, gameObject.transform.position.y, playerScr.transform.position.z) - gameObject.transform.position;
                    moveDir = Vector3.Lerp(moveDir, meToPlayer.normalized, chaseMode_turnToPlayerRate * Time.fixedDeltaTime);

                    if (AlertTime_Current > 0)
                    {
                        moveSpeed = 0;
                        AlertTime_Current -= Time.fixedDeltaTime;
                    }
                    else
                    {
                        bool stopCondition = meToPlayer.magnitude <= chaseModePlayerSurroundRadius;
                        bool stopCondition_walkable = false;
                        if (walkableRay != null)
                        {
                            stopCondition_walkable = walkableRay.ray_surface == null;
                        }
                        if (!stopCondition && stopCondition_walkable == false)
                        {
                            moveSpeed = Mathf.Lerp(moveSpeed, moveSpeed_orig * chaseModeSpeedMultiplier, 12 * Time.fixedDeltaTime);

                        }
                        else
                        {
                            if (stopCondition_walkable == false)
                            {
                                moveSpeed = Mathf.Lerp(moveSpeed, 0, 12 * Time.fixedDeltaTime);

                            }
                            else
                            {
                                moveSpeed = 0;

                            }

                        }
                    }
                }

                if (moveDir != Vector3.zero)
                {
                    gameObject.transform.forward = moveDir.normalized;

                }

                charScr.Move(moveDir * moveSpeed * Time.fixedDeltaTime);
            }





        }
    }

    public int ShiftHealth_FlameParticle(int shiftAmount)
    {
        int ret = ShiftHealth(Mathf.Abs( shiftAmount)*-1, EnemyCauseOfDamage.SprayerFire);
        mdlAnimScr.HurtEffectTrigger(DamageType.Fire);
        FrozenTimer_Current = 0;
        ShiftFreezeProgress(Mathf.Abs(FreezeProgress) * -1,EnemyCauseOfDamage.SprayerFire);
        return ret;
    }

    public override int ShiftHealth(int shiftAmount, EnemyCauseOfDamage cOD = EnemyCauseOfDamage.Other)
    {
        int retVal = -1;
        if (FindDependencies() && FindResources())
        {

            if (shiftAmount < 0)
            {
                aggroTimer_current = aggroTimer;
                if (dmgSnd_intervalTime_current <= 0)
                {
                    Instantiate(sE_enemyDmg_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    dmgSnd_intervalTime_current = dmgSnd_intervalTime;
                }
                mdlAnimScr.HurtEffectTrigger(DamageType.Other);


                if (CurrentHealth+shiftAmount<=0)
                {
                    
                    GameObject fireworkInst= Instantiate(pS_firework_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
                    LifeTimer fireLife = fireworkInst.GetComponent<LifeTimer>();

                    if (fireLife == null)
                    {
                        fireLife = fireworkInst.AddComponent<LifeTimer>();
                    }

                    fireLife.LifeTime = 10.0f;

                    Instantiate(sE_firework_prefab, fireworkInst.transform.position, fireworkInst.transform.rotation, fireworkInst.transform.parent);
                }
            }

            

            retVal = base.ShiftHealth(shiftAmount);

            
            if (CurrentHealth <= 0)
            {
                SpecialReactionaryCheckDestroy();

            }
            
        }

        return retVal;
    }


    void SpecialReactionaryCheckDestroy()
    {
        EnemyAsReactionary reactScr = gameObject.GetComponent<EnemyAsReactionary>();

        if (reactScr == null)
        {
            Destroy(gameObject);
        }
        else
        {
            reactScr.ConditionFulfilled = true;
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                Destroy(gameObject.transform.GetChild(i).gameObject);
            }

            ExtendedFunctionality.RemoveAllComponents(gameObject, reactScr as Component);
        }

        ExtendedFunctionality.SafeDestroy(rightTurnerObj);
        ExtendedFunctionality.SafeDestroy(leftTurnerObj);
    }

    protected virtual bool FindDependencies()
    {
        bool ret = false;

        if (mdlAnimScr == null)
        {
            mdlAnimScr = gameObject.GetComponent<EnemyModelAnimator>();
        }

        if (charScr == null)
        {
            charScr = gameObject.GetComponent<CharacterController>();
        }

        if (playerScr == null)
        {
            playerScr = GameObject.FindObjectOfType<PlayerStats>();
        }


        if (mdlAnimScr != null && charScr!=null && playerScr!=null)
        {
            ret = true;
        }


        return ret;
    }



    protected override bool FindResources()
    {
        bool ret = false;
        bool parRet = base.FindResources();

        if (sE_enemyDmg_prefab == null)
        {
            sE_enemyDmg_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_enemyDmg");
        }

        if (pS_firework_prefab == null)
        {
            pS_firework_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_firework");
        }

        if (sE_firework_prefab == null)
        {
            sE_firework_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_firework");

        }

        if (sE_enemyFreezeDie_prefab == null)
        {
            sE_enemyFreezeDie_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_enemyFreezeDie");
        }

        if (sE_enemyFreezeProgress_prefab == null)
        {
            sE_enemyFreezeProgress_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_enemyFreezeProgress");

        }

        if (sE_enemyFreeze_prefab == null)
        {
            sE_enemyFreeze_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_enemyFreeze");

        }

        if (sE_enemyFreezeThaw_prefab == null)
        {
            sE_enemyFreezeThaw_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_enemyFreezeThaw");
        }

        if (sE_enemyFreezeThaw_prefab!=null && sE_enemyFreeze_prefab != null && sE_enemyFreezeProgress_prefab != null && sE_enemyFreezeDie_prefab != null && sE_enemyDmg_prefab != null && pS_firework_prefab!=null && sE_firework_prefab!=null && parRet == true)
        {
            ret = true;
        }

        return ret;
    }
}
