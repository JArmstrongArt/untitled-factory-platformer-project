﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionScript_SineMove : ReactionScript
{
    [SerializeField] SineMove[] sinScr;
    protected override void ReactActive()
    {
        if (sinScr != null && sinScr.Length > 0)
        {
            foreach (SineMove sin in sinScr)
            {
                sin.enabled = true;
            }
        }
    }

    protected override void ReactInactive()
    {
        if(sinScr!=null && sinScr.Length > 0)
        {
            foreach(SineMove sin in sinScr)
            {
                sin.enabled = false;
            }
        }
    }
}
