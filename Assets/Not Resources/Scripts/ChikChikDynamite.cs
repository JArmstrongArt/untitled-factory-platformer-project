﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChikChikDynamite : ArcExplosive
{
    [SerializeField] float explosionScale;
    protected override void ExplodeAndDelete()
    {
        if (base.FindResources())
        {
            base.ExplodeAndDelete();
            if (explosion_inst != null)
            {
                explosion_inst.transform.localScale *= explosionScale;
                if (explodeScr != null)
                {
                    foreach(GameObject child in explodeScr.myChildren)
                    {
                        if (child != null)
                        {
                            child.transform.localScale *= explosionScale;

                        }
                    }
                }
                
                HurtEnemyInTrigger hurtEnScr = explosion_inst.GetComponent<HurtEnemyInTrigger>();
                if (hurtEnScr != null)
                {
                    Destroy(hurtEnScr);
                }
            }
        }
    }

    protected override void CollideReact()
    {
        if (base.FindResources())
        {
            base.CollideReact();
        }
    }

    protected override void EndArcReact()
    {
        if (base.FindResources())
        {
            base.EndArcReact();
        }
    }


}
