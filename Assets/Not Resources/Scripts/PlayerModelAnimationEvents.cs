﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModelAnimationEvents : MonoBehaviour
{
    private GameObject sE_footstep_prefab;

    // Start is called before the first frame update
    public void SpawnFootstepSound()
    {
        if (FindResources())
        {
            Instantiate(sE_footstep_prefab, gameObject.transform.position, Quaternion.Euler(Vector3.zero), null);
        }

    }

    bool FindResources()
    {
        bool ret = false;
        if (sE_footstep_prefab == null)
        {
            sE_footstep_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_footstep");
        }

        if ( sE_footstep_prefab != null)
        {
            ret = true;
        }

        return ret;
    }
}
