%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: chikChikHandsMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: ChikChikHands
    m_Weight: 0
  - m_Path: ChikChikHands/scale
    m_Weight: 1
  - m_Path: ChikChikHands/scale/l_hand 1
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger1
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger1/l_finger11
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger1/l_finger11/l_finger12
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger1/l_finger11/l_finger12/l_finger12_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger2
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger2/l_finger21
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger2/l_finger21/l_finger22
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger2/l_finger21/l_finger22/l_finger22_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger3
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger3/l_finger31
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger3/l_finger31/l_finger32
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_finger3/l_finger31/l_finger32/l_finger32_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_thumb0
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_thumb0/l_thumb1
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_thumb0/l_thumb1/l_thumb2
    m_Weight: 0
  - m_Path: ChikChikHands/scale/l_hand 1/l_thumb0/l_thumb1/l_thumb2/l_thumb2_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger1
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger1/r_finger11
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger1/r_finger11/r_finger12
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger1/r_finger11/r_finger12/r_finger12_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger2
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger2/r_finger21
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger2/r_finger21/r_finger22
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger2/r_finger21/r_finger22/r_finger22_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger3
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger3/r_finger31
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger3/r_finger31/r_finger32
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_finger3/r_finger31/r_finger32/r_finger32_end
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_thumb0
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_thumb0/r_thumb1
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_thumb0/r_thumb1/r_thumb2
    m_Weight: 0
  - m_Path: ChikChikHands/scale/r_hand 1/r_thumb0/r_thumb1/r_thumb2/r_thumb2_end
    m_Weight: 0
  - m_Path: l_hand
    m_Weight: 0
  - m_Path: metalPlate
    m_Weight: 0
  - m_Path: r_hand
    m_Weight: 0
