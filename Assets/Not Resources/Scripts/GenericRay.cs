﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//this is a script i pass around on a lot of my projects that allows you to make a raycast on any gameobject with highly customizable settings, it's far easier than setting up raycast code time and time again.


//the possible directions the ray can go, custom is a special mode that grabs the direction from the customDir_vec variable.
[Serializable]
public enum rayDirections
{
    V3Up,
    V3Down,
    V3Left,
    V3Right,
    V3Forward,
    V3Backward,
    TransformUp,
    TransformDown,
    TransformLeft,
    TransformRight,
    TransformForward,
    TransformBackward,
    Custom


}


//the possible update cycles this raycast will operate under.

public enum UpdateType
{
    Regular,
    Fixed,
    Late
}



public class GenericRay : MonoBehaviour,IEditorFunctionality
{

    public UpdateType updateType_enum;

    private Vector3 targetDir;//the actual vector3 direction this ray will move in based on 'direction' enum instance.
    [SerializeField] rayDirections direction;
    public float rayLength;
    private Ray ray;
    private RaycastHit[] ray_hitAll;
    public RaycastHit[] Ray_HitAll
    {
        get
        {
            RaycastHit[] ret = null;
            if (ray_hitAll != null)
            {
                if (ray_hitAll.Length > 0)
                {
                    ret = ray_hitAll;
                }
            }
            return ret;
        }
    }
    [SerializeField] bool raycastAll;
    [SerializeField] bool detectTriggers;
    [SerializeField] LayerMask ray_layers;//the layers which the ray can collide with.

    public GameObject ray_surface;//shorthand for the gameobject that ray_hit has.
    public Vector3 ray_hitPoint;//shorthand for the exact world coordinates that ray_hit strikes.




    public Vector3 customDir_vec;//the direction that targetDir will be set to if the 'direction' enum instance is set to 'Custom'.
    [SerializeField] Color rayCol;//the colour the ray will be when it is represented by a gizmo.
    // Use this for initialization
    void Awake()
    {

        ray_hitPoint = Vector3.zero;
    }

    void FindOutTargetDir()
    {
        switch (direction)
        {
            case rayDirections.V3Up:
                targetDir = Vector3.up;
                break;
            case rayDirections.V3Down:
                targetDir = Vector3.down;
                break;
            case rayDirections.V3Left:
                targetDir = Vector3.left;
                break;
            case rayDirections.V3Right:
                targetDir = Vector3.right;
                break;
            case rayDirections.V3Forward:
                targetDir = Vector3.forward;
                break;
            case rayDirections.V3Backward:
                targetDir = Vector3.back;
                break;
            case rayDirections.TransformBackward:
                targetDir = -transform.forward;
                break;
            case rayDirections.TransformForward:
                targetDir = transform.forward;
                break;
            case rayDirections.TransformDown:
                targetDir = -transform.up;
                break;
            case rayDirections.TransformUp:
                targetDir = transform.up;
                break;
            case rayDirections.TransformRight:
                targetDir = transform.right;
                break;
            case rayDirections.TransformLeft:
                targetDir = -transform.right;
                break;
            case rayDirections.Custom:
                targetDir = customDir_vec;
                break;
        }
    }

    public void RayRoutine()
    {
        //set targetDir appropriate to the 'direction' enum instance.
        FindOutTargetDir();

        ray = new Ray(gameObject.transform.position, targetDir);



        if (raycastAll == false)
        {
            RaycastHit ray_hit;
            if (Physics.Raycast(ray, out ray_hit, rayLength, ray_layers))
            {

                if (ray_hit.collider.isTrigger == true)
                {
                    if (detectTriggers == true)
                    {
                        ray_hitAll = new RaycastHit[] { ray_hit };
                        ray_hitPoint = ray_hit.point;
                        ray_surface = ray_hit.collider.gameObject;
                    }
                    else
                    {
                        NoCollisionsSetting();
                    }
                }
                else
                {
                    ray_hitAll = new RaycastHit[] { ray_hit };
                    ray_hitPoint = ray_hit.point;
                    ray_surface = ray_hit.collider.gameObject;
                }





            }
            else
            {
                NoCollisionsSetting();


            }
        }
        else
        {
            ray_hitAll = Physics.RaycastAll(ray, rayLength, ray_layers);

            if (ray_hitAll != null)
            {
                if (ray_hitAll.Length > 0)
                {
                    if(ray_hitAll[0].collider.isTrigger == true)
                    {
                        if (detectTriggers == true)
                        {
                            ray_surface = ray_hitAll[0].collider.gameObject;
                            ray_hitPoint = ray_hitAll[0].point;
                        }
                        else
                        {
                            NoCollisionsSetting();
                        }
                    }
                    else
                    {
                        ray_surface = ray_hitAll[0].collider.gameObject;
                        ray_hitPoint = ray_hitAll[0].point;

                    }


                }
                else
                {
                    NoCollisionsSetting();

                }
            }
            else
            {
                NoCollisionsSetting();
            }

        }





    }

    void NoCollisionsSetting()
    {
        ray_hitAll = null;
        ray_surface = null;
        ray_hitPoint = Vector3.zero;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(updateType_enum == UpdateType.Late)
        {
            RayRoutine();
        }


    }

    void FixedUpdate()
    {
        if (updateType_enum == UpdateType.Fixed)
        {
            RayRoutine();
        }


    }
    void Update()
    {
        if (updateType_enum == UpdateType.Regular)
        {
            RayRoutine();
        }


    }


    private void OnDrawGizmos()
    {
        Gizmos.color = rayCol;
        Gizmos.DrawLine(gameObject.transform.position, gameObject.transform.position+( targetDir * rayLength));
    }
    public void EditorUpdate()
    {
        FindOutTargetDir();
    }

}
