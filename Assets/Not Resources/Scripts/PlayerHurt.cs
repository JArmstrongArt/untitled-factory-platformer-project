﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public enum StunnedType
{
    NotStunned,
    StunnedAndInvincible,
    StunnedAndVulnerable
}
public class PlayerHurt : MonoBehaviour
{

    [SerializeField] float stunTime;
    private float stunTime_current=0;

    private PlayerStats statScr;

    private StunnedType stunType = StunnedType.NotStunned;
    public StunnedType StunType
    {
        get
        {
            return stunType;
        }

        private set
        {
            stunType = value;
        }
    }
    //public bool Stunned { get; private set; }

    [SerializeField] float invincibleTime;
    private float invincibleTime_current = 0;

    private PlayerModelAnimator modelAnimScr;

    

    public void GetHurt(int damage = 1,bool shockerType=false)
    {
        if (FindDependencies() )
        {
            //bool hurtCond = stunTime_current <= 0 && invincibleTime_current <= 0;
            bool hurtCond = StunType != StunnedType.StunnedAndInvincible && invincibleTime_current <= 0 && statScr.Dead==false;
            if (hurtCond)
            {
                stunTime_current = stunTime;
                if (shockerType == false)
                {
                    StunType = StunnedType.StunnedAndInvincible;
                }
                else
                {
                    StunType = StunnedType.StunnedAndVulnerable;

                }

                List<PlayerWeapon> allActions = FindObjectsOfType<PlayerWeapon>().ToList();

                foreach(PlayerWeapon action in allActions)
                {
                    action.DisablePrepare();
                }
                if (StunType==StunnedType.StunnedAndInvincible)
                {
                    invincibleTime_current = invincibleTime;

                    statScr.ShiftHealth(Mathf.Abs(damage) * -1);
                }

            }

        }
    }




    // Update is called once per frame
    void Update()
    {
        if (FindDependencies() )
        {
            if (stunTime_current > 0)
            {
                //invincibleTime_current = invincibleTime;
                //Stunned = true;
                stunTime_current -= Time.deltaTime;
            }
            else
            {
                StunType = StunnedType.NotStunned;
                if (invincibleTime_current > 0)
                {



                    invincibleTime_current -= Time.deltaTime;
                }
                //Stunned = false;
            }


            if (statScr.Dead == false)
            {
                if (invincibleTime_current <= 0 || stunTime_current > 0)
                {
                    modelAnimScr.SetModelAlpha(1.0f);
                }
                else
                {
                    modelAnimScr.SetModelAlpha(0.5f);

                }
            }

        }

    }


    bool FindDependencies()
    {
        bool ret = false;
        if (statScr == null)
        {
            statScr = gameObject.GetComponent<PlayerStats>();

        }

        if (modelAnimScr == null)
        {
            modelAnimScr = gameObject.GetComponent<PlayerModelAnimator>();
        }

        if (statScr != null && modelAnimScr!=null)
        {
            ret = true;
        }

        return ret;
    }


}
