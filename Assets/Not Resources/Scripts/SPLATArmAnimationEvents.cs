﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPLATArmAnimationEvents : MonoBehaviour
{
    [SerializeField] Transform slamPos;
    private GameObject SPLATShockwaveModel_prefab;
    private GameObject sE_SPLATBegin_prefab;
    private GameObject sE_SPLATBegin_inst;
    private GameObject sE_SPLATReadyToSplat_prefab;
    private GameObject sE_SPLATHitGround_prefab;
    private PlayerGravity gravScr;
    private PlayerHurt hurtScr;
    [SerializeField] float slamStunRange;
    public void SPLATArmSlam()
    {
        if (FindResources() && FindDependencies())
        {
            Instantiate(SPLATShockwaveModel_prefab, slamPos.position, Quaternion.Euler(-90, 0, 0), null);
            Instantiate(sE_SPLATHitGround_prefab, slamPos.position, Quaternion.Euler(Vector3.zero), null);
            if((gravScr.transform.position-slamPos.transform.position).magnitude<= slamStunRange)
            {
                if (gravScr.Grounded)
                {
                    hurtScr.GetHurt(0, true);
                }
            }


        }

    }

    public void SPLATBegin()
    {
        if (FindResources() && FindDependencies())
        {
            if (sE_SPLATBegin_inst == null)
            {
                sE_SPLATBegin_inst = Instantiate(sE_SPLATBegin_prefab, slamPos.position, Quaternion.Euler(Vector3.zero), null);
                MaintainOffset offScr = sE_SPLATBegin_inst.GetComponent<MaintainOffset>();
                if (offScr == null)
                {
                    offScr = sE_SPLATBegin_inst.AddComponent<MaintainOffset>();
                }
                offScr.OffsetObj = slamPos.gameObject;
                offScr.Init();
            }
        }

    }

    public void SPLATReadyToSplat()
    {
        ExtendedFunctionality.SafeDestroy(sE_SPLATBegin_inst);
        if (FindResources() && FindDependencies())
        {
            Instantiate(sE_SPLATReadyToSplat_prefab, slamPos.position, Quaternion.Euler(Vector3.zero), null);

        }
    }



    private void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(sE_SPLATBegin_inst);

    }


    bool FindResources()
    {
        bool ret = false;
        if (SPLATShockwaveModel_prefab == null)
        {
            SPLATShockwaveModel_prefab = Resources.Load<GameObject>("Prefabs/Enemies/SPLATShockwaveModel");
        }

        if (sE_SPLATBegin_prefab == null)
        {
            sE_SPLATBegin_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_SPLATBegin");
        }

        if (sE_SPLATReadyToSplat_prefab == null)
        {
            sE_SPLATReadyToSplat_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_SPLATReadyToSplat");
        }

        if (sE_SPLATHitGround_prefab == null)
        {
            sE_SPLATHitGround_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_SPLATHitGround");
        }


        if (sE_SPLATHitGround_prefab!=null && sE_SPLATReadyToSplat_prefab!=null && sE_SPLATBegin_prefab!=null && SPLATShockwaveModel_prefab != null)
        {
            ret = true;
        }

        return ret;
    }

    bool FindDependencies()
    {
        bool ret = false;


        if (gravScr == null)
        {
            gravScr = GameObject.FindObjectOfType<PlayerGravity>();
        }

        if (hurtScr == null)
        {
            hurtScr = GameObject.FindObjectOfType<PlayerHurt>();
        }

        if(gravScr!=null && hurtScr != null)
        {
            ret = true;
        }

        return ret;
    }

    public void OnDrawGizmos()
    {
        if (slamPos != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(slamPos.position, slamStunRange);
        }
    }
}
