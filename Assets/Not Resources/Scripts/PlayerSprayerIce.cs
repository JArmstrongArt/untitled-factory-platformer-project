﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprayerIce : PlayerSprayer
{

    private GameObject pS_sprayerIce_prefab;
    private GameObject sE_sprayerIce_init_prefab;
    private GameObject sE_sprayerIce_loop_prefab;

    private GameObject iceLoop_inst;
    private GameObject iceInit_inst;

    private bool initSoundComplete = false;

    protected override void EnableDisableShared()
    {
        if (base.FindDependencies() && FindResources())
        {
            sprayerMdlTag = "playerModel_sprayer_ice";
            base.EnableDisableShared();
        }

    }

    public override void EnablePrepare()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.EnablePrepare();
            if (sprayer_firePoint != null)
            {
                sprayParticle_inst = Instantiate(pS_sprayerIce_prefab, sprayer_firePoint.transform.position, Quaternion.Euler(Vector3.zero), null);
                RefreshSprayParticle();
            }
        }

    }

    void EndSound()
    {
        initSoundComplete = false;
        ExtendedFunctionality.SafeDestroy(iceInit_inst);
        ExtendedFunctionality.SafeDestroy(iceLoop_inst);
    }

    public override void DisablePrepare()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.DisablePrepare();
            EndSound();
        }

    }

    protected override void Update()
    {
        if (base.FindDependencies() && FindResources())
        {
            base.Update();
            if (spraying == true)
            {
                if (initSoundComplete == false)
                {
                    if (iceInit_inst == null)
                    {
                        iceInit_inst = Instantiate(sE_sprayerIce_init_prefab, sprayParticle_inst.transform.position, Quaternion.Euler(Vector3.zero), null);

                    }
                    initSoundComplete = true;
                }


                if (iceLoop_inst == null)
                {
                    iceLoop_inst = Instantiate(sE_sprayerIce_loop_prefab, sprayParticle_inst.transform.position, Quaternion.Euler(Vector3.zero), sprayParticle_inst.transform);
                }

            }
            else
            {
                EndSound();

            }
        }
    }


    bool FindResources()
    {
        bool ret = false;

        if (pS_sprayerIce_prefab == null)
        {
            pS_sprayerIce_prefab = Resources.Load<GameObject>("Prefabs/Particles/pS_sprayerIce");
        }

        if (sE_sprayerIce_init_prefab == null)
        {
            sE_sprayerIce_init_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_sprayerIce_init");
        }

        if (sE_sprayerIce_loop_prefab == null)
        {
            sE_sprayerIce_loop_prefab = Resources.Load<GameObject>("Prefabs/Sound Effects/sE_sprayerIce_loop");
        }

        if (pS_sprayerIce_prefab != null && sE_sprayerIce_init_prefab!=null && sE_sprayerIce_loop_prefab!=null)
        {
            ret = true;
        }

        return ret;
    }

}
