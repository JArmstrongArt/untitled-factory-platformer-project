﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelAnimator : MonoBehaviour
{

    protected string mdlPath;
    private GameObject mdl_prefab;
    public GameObject Mdl_Inst { get; private set; }

    protected Animator mdl_inst_anim;

    [SerializeField] float mdl_heightOffset;

    private Renderer preMdlRend;

    [HideInInspector]
    public List<Renderer> mdl_allRenderers = new List<Renderer>();




    void GenerateMdlInfo()
    {
        if (preMdlRend == null)
        {
            preMdlRend = gameObject.GetComponent<Renderer>();
        }

        if (Mdl_Inst == null)
        {

            Mdl_Inst = Instantiate(mdl_prefab, gameObject.transform.position + new Vector3(0, Mathf.Abs(mdl_heightOffset) * -1, 0), Quaternion.Euler(gameObject.transform.eulerAngles), gameObject.transform);
        }



        if (preMdlRend != null)
        {
            if (Mdl_Inst != null)
            {

                preMdlRend.enabled = false;

                if (Mdl_Inst != null)
                {
                    if (mdl_allRenderers == null || (mdl_allRenderers != null && mdl_allRenderers.Count <= 0))
                    {
                        mdl_allRenderers = ExtendedFunctionality.GetAllRenderersInChildren(gameObject);

                    }



                    if (mdl_inst_anim == null)
                    {
                        mdl_inst_anim = Mdl_Inst.GetComponent<Animator>();
                    }
                }




            }
            else
            {
                preMdlRend.enabled = true;

            }
        }



    }


    protected virtual void Awake()
    {
        LoadModel();
        GenerateMdlInfo();

    }

    protected virtual void OnDestroy()
    {
        ExtendedFunctionality.SafeDestroy(Mdl_Inst);
    }

    protected virtual void Update()
    {
        GenerateMdlInfo();




    }

    protected bool LoadModel()
    {
        bool ret = false;
        if (mdl_prefab == null)
        {

            mdl_prefab = Resources.Load<GameObject>(mdlPath);
        }

        if (mdl_prefab != null)
        {
            ret = true;
        }


        return ret;
    }


}
